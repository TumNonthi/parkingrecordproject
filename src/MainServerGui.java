import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.jdesktop.swingx.JXDatePicker;




public class MainServerGui extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9211389498294185521L;

	private static MainServerGui mainGui;
	
	private MainServer mainServer;
	
	private static final char[] defaultPassword = {'1','3','5','7','9'};
	//private static char[] correctPassword = {'1','2','3','4','5'};
	private static final String filename_password = "appdata/password.dpk";
	
	private static final char[] defaultEmployeePassword = {'1','2','3','4','5'};
	private static final String filename_employee_password = "appdata/password_employee.txt";
	
	private final int LAYER_LEVEL_BG = 10;
	private final int LAYER_LEVEL_CONTENT = 20;
	
	private Color bgColor = new Color(0,0,0,160);
	
	private JFrame parentFrame;
	
	private JLayeredPane layeredPane;
	
	private JLabel bgLabel;
	
	private MyPanel portPanel;
	
	private JLabel portTitleLabel;
	private JTextField portTextField;
	private JButton startButton;
	
	private JLabel passwordTitleLabel;
	private JPasswordField passwordTextField;
	
	private JLabel newPasswordTitleLabel;
	private JPasswordField newPasswordTextField;
	
	private JButton submitNewPasswordButton;
	
	private MyPanel dataPanel;
	
	private JLabel titleLabel;
	
	private JLabel barcodeTitleLabel;
	private JTextField barcodeTextField;
	private JButton barcodeSearchButton;
	
	private JLabel plateNumTitleLabel;
	private JTextField plateNumTextField;
	private JButton plateNumSearchButton;
	
	private JLabel nameTitleLabel;
	private JTextField nameTextField;
	
	private JLabel brandTitleLabel;
	private JTextField brandTextField;
	
	private JLabel addressTitleLabel;
	private JTextField addressTextField;
	
	private JLabel telTitleLabel;
	private JTextField telTextField;
	
	private JLabel typeLabel;
	private JRadioButton monthlyDayRadioButton;
	private JRadioButton monthlyNightRadioButton;
	private JRadioButton monthlyAllRadioButton;
	private ButtonGroup monthlyTypeButtonGroup;
	
	private JRadioButton carRadioButton;
	private JRadioButton motorcycleRadioButton;
	private ButtonGroup carTypeButtonGroup;
	
	private JLabel issueTitleLabel;
	private JXDatePicker issueDatePicker;
	
	private JLabel expTitleLabel;
	private JXDatePicker expDatePicker;
	
	private JButton saveChangesButton;
	private JButton removeRecordButton;
	private JButton removeBarcodeButton;
	private JButton addRecordButton;
	private JButton addBarcodeButton;
	private JButton cancelButton;
	
	private JLabel logLabel;
	
	private MyPanel reportPanel;
	
	private JLabel reportTitleLabel;
	private JButton infoParkingEntriesButton;
	private JButton infoMemberEntriesButton;
	private JButton infoBarcodesButton;
	
	private JLabel infoDailyDateLabel;
	private JTextField infoDailyDateTextField;
	private JLabel infoDailyMonthLabel;
	private JTextField infoDailyMonthTextField;
	private JButton infoDailyButton;
	
	private JLabel infoMonthlyMonthLabel;
	private JTextField infoMonthlyMonthTextField;
	private JButton infoMonthlyButton;
	private JButton reportProcessedMonthlyButton;
	
	
	////////////////////////////////////
	// Transaction Editor
	////////////////////////////////////
	
	private JButton transactionEditorButton;
	
	private TransactionEditorDialog teDialog;
	
	public MainServerGui(JFrame aFrame) {
		
		parentFrame = aFrame;
		
		mainServer = new MainServer(ConfigValues.defaultPort);
		mainServer.setServerGUI(this);
		//mainServer.startServerLobby();
		
		
		setPreferredSize(new Dimension(1024,640));
		//setLayout(null);
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		layeredPane = new JLayeredPane();
        //layeredPane.setPreferredSize(new Dimension(1024, 768));
        
        bgLabel = new JLabel();
        Icon bgIcon = new ImageIcon("UI/bg/bg01.jpg");
        //bgLabel.setBounds(0,0,1366,768);
        bgLabel.setBounds(0,0,1024,640);
        bgLabel.setIcon(bgIcon);
        layeredPane.add(bgLabel, new Integer(LAYER_LEVEL_BG));
        
        portPanel = new MyPanel();
        portPanel.setBounds(50,30,360,160);
        portPanel.setBackground(bgColor);
        portPanel.setOpaque(false);
        portPanel.setLayout(null);
        layeredPane.add(portPanel, new Integer(LAYER_LEVEL_CONTENT));
        
        portTitleLabel = new JLabel("Port");
        portTitleLabel.setBounds(20,20,80,25);
        portPanel.add (portTitleLabel);
        
        portTextField = new JTextField("");
        portTextField.setBounds(110,20,150,25);
        portPanel.add (portTextField);
        
        startButton = new JButton("Start");
        startButton.setBounds(270,20,70,25);
        startButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		startButtonPressed();
        	}
        });
        portPanel.add(startButton);
        
        passwordTitleLabel = new JLabel("Password");
        passwordTitleLabel.setBounds(20,55,80,25);
        portPanel.add (passwordTitleLabel);
        
        passwordTextField = new JPasswordField("");
        passwordTextField.setBounds(110,55,150,25);
        portPanel.add (passwordTextField);
        
        newPasswordTitleLabel = new JLabel("New Password");
        newPasswordTitleLabel.setBounds(20,90,90,25);
        portPanel.add (newPasswordTitleLabel);
        
        newPasswordTextField = new JPasswordField("");
        newPasswordTextField.setBounds(110,90,150,25);
        portPanel.add (newPasswordTextField);
        
        submitNewPasswordButton = new JButton("Submit");
        submitNewPasswordButton.setBounds(270,90,70,25);
        submitNewPasswordButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		submitNewPasswordButtonPressed();
        	}
        });
        portPanel.add(submitNewPasswordButton);
        
        CustomGuiUtilities.makeTransparent(portPanel.getComponents());
        
        
        dataPanel = new MyPanel();
        dataPanel.setBounds(440,30,534,580);
        dataPanel.setBackground(bgColor);
        dataPanel.setOpaque(false);
        dataPanel.setLayout(null);
        layeredPane.add(dataPanel, new Integer(LAYER_LEVEL_CONTENT));
        
        titleLabel = new JLabel("Monthly Car Entry");
        titleLabel.setBounds(205, 20, 124, 25);
        dataPanel.add(titleLabel);
        
        barcodeTitleLabel = new JLabel("Barcode");
        barcodeTitleLabel.setBounds(20,55,100,25);
        dataPanel.add (barcodeTitleLabel);
        
        barcodeTextField = new JTextField("");
        barcodeTextField.setBounds(120,55,150,25);
        dataPanel.add (barcodeTextField);
        
        barcodeSearchButton = new JButton("Search");
        barcodeSearchButton.setBounds(280,55,70,25);
        barcodeSearchButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		barcodeSearchPressed();
        	}
        });
        dataPanel.add(barcodeSearchButton);
        
        plateNumTitleLabel = new JLabel("ทบ. รถ");
        plateNumTitleLabel.setBounds(20,90,80,25);
        dataPanel.add (plateNumTitleLabel);
        
        plateNumTextField = new JTextField("");
        plateNumTextField.setBounds(120,90,150,25);
        dataPanel.add (plateNumTextField);
        
        plateNumSearchButton = new JButton("Search");
        plateNumSearchButton.setBounds(280,90,70,25);
        plateNumSearchButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		plateNumSearchPressed();
        	}
        });
        dataPanel.add(plateNumSearchButton);
        
        nameTitleLabel = new JLabel("Name-Surname");
        nameTitleLabel.setBounds(20, 160, 100, 25);
        dataPanel.add(nameTitleLabel);
        
        nameTextField = new JTextField();
        nameTextField.setBounds(120, 160, 230, 25);
        dataPanel.add(nameTextField);
        
        brandTitleLabel = new JLabel("Model");
        brandTitleLabel.setBounds(20, 195, 100, 25);
        dataPanel.add(brandTitleLabel);
        
        brandTextField = new JTextField();
        brandTextField.setBounds(120, 195, 230, 25);
        dataPanel.add(brandTextField);
        
        addressTitleLabel = new JLabel("Address");
        addressTitleLabel.setBounds(20, 230, 100, 25);
        dataPanel.add(addressTitleLabel);
        
        addressTextField = new JTextField();
        addressTextField.setBounds(120, 230, 360, 25);
        dataPanel.add(addressTextField);
        
        
        telTitleLabel = new JLabel("Tel.");
        telTitleLabel.setBounds(20, 265, 70, 25);
        dataPanel.add(telTitleLabel);
        
        telTextField = new JTextField("");
        telTextField.setBounds(120, 265, 160, 25);
        dataPanel.add(telTextField);
        
        
        typeLabel = new JLabel("ประเภท");
        typeLabel.setBounds(20, 300, 100, 25);
        dataPanel.add(typeLabel);
        
        monthlyAllRadioButton = new JRadioButton("All", false);
        monthlyAllRadioButton.setBounds(120, 300, 70, 25);
        dataPanel.add(monthlyAllRadioButton);
        
        monthlyDayRadioButton = new JRadioButton("Day", false);
        monthlyDayRadioButton.setBounds(200, 300, 70, 25);
        dataPanel.add(monthlyDayRadioButton);
        
        monthlyNightRadioButton = new JRadioButton("Night", false);
        monthlyNightRadioButton.setBounds(280, 300, 70, 25);
        dataPanel.add(monthlyNightRadioButton);
        
        monthlyTypeButtonGroup = new ButtonGroup();
        monthlyTypeButtonGroup.add(monthlyAllRadioButton);
        monthlyTypeButtonGroup.add(monthlyDayRadioButton);
        monthlyTypeButtonGroup.add(monthlyNightRadioButton);
        
        carRadioButton = new JRadioButton("รถ", false);
        carRadioButton.setBounds(360, 300, 70, 25);
        dataPanel.add(carRadioButton);
        
        motorcycleRadioButton = new JRadioButton("จยย.", false);
        motorcycleRadioButton.setBounds(440, 300, 70, 25);
        dataPanel.add(motorcycleRadioButton);
        
        carTypeButtonGroup = new ButtonGroup();
        carTypeButtonGroup.add(carRadioButton);
        carTypeButtonGroup.add(motorcycleRadioButton);
        
        issueTitleLabel = new JLabel("Issue Date");
        issueTitleLabel.setBounds(20,335,100,25);
        dataPanel.add(issueTitleLabel);
        
        issueDatePicker = new JXDatePicker();
        issueDatePicker.setBounds(120, 335, 160, 25);
        issueDatePicker.setDate(new Date(0));
        dataPanel.add(issueDatePicker);
        
        expTitleLabel = new JLabel("Exp. Date");
        expTitleLabel.setBounds(20, 370, 100, 25);
        dataPanel.add(expTitleLabel);
        
        expDatePicker = new JXDatePicker();
        expDatePicker.setBounds(120, 370, 160, 25);
        expDatePicker.setDate(new Date(0));
        dataPanel.add(expDatePicker);
        
        saveChangesButton = new JButton("Save");
        saveChangesButton.setBounds(20,440,70,25);
        saveChangesButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		saveChangesPressed();
        	}
        });
        dataPanel.add(saveChangesButton);
        saveChangesButton.setVisible(false);
        
        removeRecordButton = new JButton("Delete");
        removeRecordButton.setBounds(330,440,70,25);
        removeRecordButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		removeRecordPressed();
        	}
        });
        removeRecordButton.setBackground(Color.RED);
        dataPanel.add(removeRecordButton);
        removeRecordButton.setVisible(false);
        
        removeBarcodeButton = new JButton("Delete");
        removeBarcodeButton.setBounds(330,440,70,25);
        removeBarcodeButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		removeBarcodePressed();
        	}
        });
        removeBarcodeButton.setBackground(Color.RED);
        dataPanel.add(removeBarcodeButton);
        removeBarcodeButton.setVisible(false);
        
        addRecordButton = new JButton("Add Member");
        addRecordButton.setBounds(20,440,110,25);
        addRecordButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		addRecordPressed();
        	}
        });
        dataPanel.add(addRecordButton);
        
        addBarcodeButton = new JButton("Add Barcode");
        addBarcodeButton.setBounds(330,440,110,25);
        addBarcodeButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		addBarcodePressed();
        	}
        });
        dataPanel.add(addBarcodeButton);
        
        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(450,440,70,25);
        cancelButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		cancelPressed();
        	}
        });
        dataPanel.add(cancelButton);
        
        logLabel = new JLabel("");
        logLabel.setBounds(20,475,360,90);
        logLabel.setForeground(Color.RED);
        dataPanel.add(logLabel);
        
        
        transactionEditorButton = new JButton("Transaction Editor");
        transactionEditorButton.setBounds(360,530,140,25);
        transactionEditorButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		transactionEditorPressed();
        	}
        });
        dataPanel.add(transactionEditorButton);
        
        teDialog = new TransactionEditorDialog(null, mainServer);
        teDialog.pack();
        teDialog.setVisible(false);
        
        CustomGuiUtilities.makeTransparent(dataPanel.getComponents());
        
        reportPanel = new MyPanel();
        reportPanel.setBounds(50,240,360,370);
        reportPanel.setBackground(bgColor);
        reportPanel.setOpaque(false);
        reportPanel.setLayout(null);
        layeredPane.add(reportPanel, new Integer(LAYER_LEVEL_CONTENT));
        
        reportTitleLabel = new JLabel("Info");
        reportTitleLabel.setBounds(155, 20, 60, 25);
        reportTitleLabel.setHorizontalAlignment(JLabel.CENTER);
        reportPanel.add(reportTitleLabel);
        
        infoParkingEntriesButton = new JButton("Build Parking Entries Info");
        infoParkingEntriesButton.setBounds(50, 60, 270, 25);
        infoParkingEntriesButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		infoParkingEntriesPressed();
        	}
        });
        reportPanel.add(infoParkingEntriesButton);
        
        infoMemberEntriesButton = new JButton("Build Members Info");
        infoMemberEntriesButton.setBounds(50, 95, 270, 25);
        infoMemberEntriesButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		infoMemberEntriesPressed();
        	}
        });
        reportPanel.add(infoMemberEntriesButton);
        
        infoBarcodesButton = new JButton("Build Barcodes Info");
        infoBarcodesButton.setBounds(50, 130, 270, 25);
        infoBarcodesButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		infoBarcodesPressed();
        	}
        });
        reportPanel.add(infoBarcodesButton);
        
        infoDailyDateLabel = new JLabel("Date:");
        infoDailyDateLabel.setBounds(50, 170, 40, 25);
        reportPanel.add(infoDailyDateLabel);
        
        infoDailyDateTextField = new JTextField();
        infoDailyDateTextField.setBounds(100, 170, 30, 25);
        reportPanel.add(infoDailyDateTextField);
        
        infoDailyMonthLabel = new JLabel("Month:");
        infoDailyMonthLabel.setBounds(150, 170, 40, 25);
        reportPanel.add(infoDailyMonthLabel);
        
        infoDailyMonthTextField = new JTextField();
        infoDailyMonthTextField.setBounds(200, 170, 30, 25);
        reportPanel.add(infoDailyMonthTextField);
        
        infoDailyButton = new JButton("Build Daily Info");
        infoDailyButton.setBounds(50, 205, 270, 25);
        infoDailyButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		infoDailyPressed();
        	}
        });
        reportPanel.add(infoDailyButton);
        
        infoMonthlyMonthLabel = new JLabel("Month:");
        infoMonthlyMonthLabel.setBounds(150, 245, 40, 25);
        reportPanel.add(infoMonthlyMonthLabel);
        
        infoMonthlyMonthTextField = new JTextField();
        infoMonthlyMonthTextField.setBounds(200, 245, 30, 25);
        reportPanel.add(infoMonthlyMonthTextField);
        
        infoMonthlyButton = new JButton("Build Monthly Info");
        infoMonthlyButton.setBounds(50, 280, 270, 25);
        infoMonthlyButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		infoMonthlyPressed();
        	}
        });
        reportPanel.add(infoMonthlyButton);
        
        reportProcessedMonthlyButton = new JButton("Build Monthly Report");
        reportProcessedMonthlyButton.setBounds(50, 315, 270, 25);
        reportProcessedMonthlyButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		reportProcessedMonthlyPressed();
        	}
        });
        reportPanel.add(reportProcessedMonthlyButton);
        
        CustomGuiUtilities.makeTransparent(reportPanel.getComponents());
        
        add(layeredPane);
        
        reconfigure_notConnectedState();
        reconfigure_standbyState();
		
	}
	
	private void exit() {
		if (mainServer != null) {
			mainServer.stop();
		}
	}
	
	private static void createAndShowServerGUI() {
    	
    	CustomGuiUtilities.useCustomLookAndFeel();
    	
    	JFrame frame = new JFrame("Parking Records Server");
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainGui = new MainServerGui(frame);
        mainGui.setOpaque(true);
        frame.setContentPane(mainGui);
      
        frame.setVisible(true);
        frame.pack();
        frame.setResizable(false);
        
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        WindowListener exitListener = new WindowAdapter() {
        	
        	@Override
        	public void windowClosing(WindowEvent e) {
        		mainGui.exit();
        		System.exit(0);
        	}
        	
        };
        frame.addWindowListener(exitListener);
        
	}
	
	private void startButtonPressed() {
		
		int aPortNum;
		
		try {
			aPortNum = Integer.parseInt(portTextField.getText());
		} catch (Exception e) {
			aPortNum = ConfigValues.defaultPort;
		}
		
		if (mainServer != null) {
			mainServer.stop();
			mainServer.setPort(aPortNum);
			mainServer.startServerLobby();
			startButton.setVisible(false);
			displayLog("STARTED",true);
		}
	}
	
	private void submitNewPasswordButtonPressed() {
		
		char[] pca = passwordTextField.getPassword();
		
		if (newPasswordTextField.getPassword().length == 0) {
			displayLog("PLEASE ENTER NEW PASSWORD", false);
			passwordTextField.setText("");
			newPasswordTextField.setText("");
			
			Arrays.fill(pca,'0');
			return;
		}
		
		if (isPasswordCorrect(pca)) {
			savePassword(newPasswordTextField.getPassword());
			displayLog("PASSWORD CHANGED", true);
		} else {
			displayLog("INCORRECT PASSWORD", false);
		}
		
		passwordTextField.setText("");
		newPasswordTextField.setText("");
		
		Arrays.fill(pca,'0');
		
	}
	
	private void barcodeSearchPressed() {
		
		
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		String bc = getBarcodeString();
		
		if (bc==null) {
			
			displayLog("INVALID BARCODE",false);
			
			return;
		}
		
		
		int bcIndex = mainServer.getBarcodeIndex(bc);
		ParkingEntry pe = mainServer.getParkingEntryFromBarcode(bc);
		//String plateNumStr = mainServer.getParkingPlateNumStringFromBarcode(bc);
		MemberCarEntry mce = mainServer.getMemberEntryFromBarcode(bc);

		if (mce != null) {
			
			mainServer.setCurrentMemberCarEntry(mce);
			
			barcodeSearchButton.setVisible(false);
			plateNumTextField.setText(mce.getPlateNumString());
			plateNumSearchButton.setVisible(false);
			
			nameTextField.setText(mce.firstname);
			brandTextField.setText(mce.brand);
			addressTextField.setText(mce.address);
			telTextField.setText(mce.tel);
			
			switch (mce.memberType) {
			
			case MONTHLY_ALL:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(false);
				monthlyNightRadioButton.setSelected(false);
				monthlyAllRadioButton.setSelected(true);
				break;
			
			case MONTHLY_DAY:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(true);
				monthlyNightRadioButton.setSelected(false);
				monthlyAllRadioButton.setSelected(false);
				break;
				
			case MONTHLY_NIGHT:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(false);
				monthlyNightRadioButton.setSelected(true);
				monthlyAllRadioButton.setSelected(false);
				break;
				
			default:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(false);
				monthlyNightRadioButton.setSelected(false);
				monthlyAllRadioButton.setSelected(false);
			
			}
			
			switch (mce.entryType) {
			
			case CAR:
				carTypeButtonGroup.clearSelection();
				carRadioButton.setSelected(true);
				motorcycleRadioButton.setSelected(false);
				
				break;
				
			case MOTORCYCLE:
				carTypeButtonGroup.clearSelection();
				carRadioButton.setSelected(false);
				motorcycleRadioButton.setSelected(true);
				
				break;
				
			default:
				carTypeButtonGroup.clearSelection();
				carRadioButton.setSelected(true);
				motorcycleRadioButton.setSelected(false);
				
				break;
		
			}
			
			issueDatePicker.setDate(new Date(mce.issueDate));
			expDatePicker.setDate(new Date(mce.expirationDate));
			
			if (pe != null) {
				
				mainServer.setCurrentParkingEntry(pe);
				
				barcodeTextField.setEditable(false);
				plateNumTextField.setEditable(true);
				
				nameTextField.setEditable(true);
				brandTextField.setEditable(true);
				addressTextField.setEditable(true);
				telTextField.setEditable(true);
				
				monthlyDayRadioButton.setEnabled(true);
				monthlyNightRadioButton.setEnabled(true);
				monthlyAllRadioButton.setEnabled(true);
				
				carRadioButton.setEnabled(true);
				motorcycleRadioButton.setEnabled(true);
				
				issueDatePicker.setEditable(true);
				expDatePicker.setEditable(true);
				
				saveChangesButton.setVisible(true);
				removeRecordButton.setVisible(false);
				removeBarcodeButton.setVisible(false);
				addRecordButton.setVisible(false);
				addBarcodeButton.setVisible(false);
				cancelButton.setVisible(true);
				
				displayLog("Member found. Status: PARKING",true);
				
			} else {
				
				barcodeTextField.setEditable(true);
				plateNumTextField.setEditable(true);
				
				nameTextField.setEditable(true);
				brandTextField.setEditable(true);
				addressTextField.setEditable(true);
				telTextField.setEditable(true);
				
				monthlyDayRadioButton.setEnabled(true);
				monthlyNightRadioButton.setEnabled(true);
				monthlyAllRadioButton.setEnabled(true);
				
				carRadioButton.setEnabled(true);
				motorcycleRadioButton.setEnabled(true);
				
				issueDatePicker.setEditable(true);
				expDatePicker.setEditable(true);
				
				saveChangesButton.setVisible(true);
				removeRecordButton.setVisible(true);
				removeBarcodeButton.setVisible(false);
				addRecordButton.setVisible(false);
				addBarcodeButton.setVisible(false);
				cancelButton.setVisible(true);
				
				displayLog("Member found. Status: NOT PARKING",true);
				
			}
			
		} else if (bcIndex >= 0) {
			
			mainServer.setCurrentBarcodeIndex(bcIndex);
			
			barcodeTextField.setEditable(false);
			barcodeSearchButton.setVisible(false);
			plateNumTextField.setEditable(false);
			plateNumSearchButton.setVisible(false);
			
			nameTextField.setText("");
			brandTextField.setText("");
			addressTextField.setText("");
			telTextField.setText("");
			
			monthlyTypeButtonGroup.clearSelection();
			monthlyDayRadioButton.setSelected(false);
			monthlyNightRadioButton.setSelected(false);
			monthlyAllRadioButton.setSelected(false);
			
			carTypeButtonGroup.clearSelection();
			carRadioButton.setSelected(false);
			motorcycleRadioButton.setSelected(false);
			
			issueDatePicker.setDate(new Date(0));
			expDatePicker.setDate(new Date(0));
			
			nameTextField.setEditable(false);
			brandTextField.setEditable(false);
			addressTextField.setEditable(false);
			telTextField.setEditable(false);
			
			monthlyDayRadioButton.setEnabled(false);
			monthlyNightRadioButton.setEnabled(false);
			monthlyAllRadioButton.setEnabled(false);
			
			carRadioButton.setEnabled(false);
			motorcycleRadioButton.setEnabled(false);
			
			issueDatePicker.setEditable(false);
			expDatePicker.setEditable(false);
			
			if (pe != null) {
				
				mainServer.setCurrentParkingEntry(pe);
				
				plateNumTextField.setText(pe.getPlateNumString());
				
				saveChangesButton.setVisible(false);
				removeRecordButton.setVisible(false);
				removeBarcodeButton.setVisible(false);
				addRecordButton.setVisible(false);
				addBarcodeButton.setVisible(false);
				cancelButton.setVisible(true);
				
				switch (pe.entryType) {
				
				case CAR:
					carTypeButtonGroup.clearSelection();
					carRadioButton.setSelected(true);
					motorcycleRadioButton.setSelected(false);
					break;
					
				case MOTORCYCLE:
					carTypeButtonGroup.clearSelection();
					carRadioButton.setSelected(false);
					motorcycleRadioButton.setSelected(true);
					break;
					
				default:
					carTypeButtonGroup.clearSelection();
					carRadioButton.setSelected(true);
					motorcycleRadioButton.setSelected(false);
			
				}
				
				displayLog("Barcode found. Status: PARKING",true);
				
			} else {
				
				plateNumTextField.setText("");
				
				saveChangesButton.setVisible(false);
				removeRecordButton.setVisible(false);
				removeBarcodeButton.setVisible(true);
				addRecordButton.setVisible(false);
				addBarcodeButton.setVisible(false);
				cancelButton.setVisible(true);
				
				displayLog("Barcode found. Status: NOT PARKING",true);
				
			}
			
		} else {
			
			plateNumTextField.setText("");
			
			barcodeSearchButton.setVisible(false);
			plateNumSearchButton.setVisible(false);
			
			barcodeTextField.setEditable(false);
			plateNumTextField.setEditable(true);
			
			nameTextField.setEditable(true);
			brandTextField.setEditable(true);
			addressTextField.setEditable(true);
			telTextField.setEditable(true);
			
			monthlyDayRadioButton.setEnabled(true);
			monthlyNightRadioButton.setEnabled(true);
			monthlyAllRadioButton.setEnabled(true);
			
			carRadioButton.setEnabled(true);
			motorcycleRadioButton.setEnabled(true);
			
			issueDatePicker.setEditable(true);
			expDatePicker.setEditable(true);
			
			saveChangesButton.setVisible(false);
			removeRecordButton.setVisible(false);
			removeBarcodeButton.setVisible(false);
			addRecordButton.setVisible(true);
			addBarcodeButton.setVisible(true);
			cancelButton.setVisible(true);
			
			displayLog("BARCODE NOT FOUND", false);
			
		}
		
	}
	
	private void plateNumSearchPressed() {
	
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		String pnStr = plateNumTextField.getText();
		if (pnStr.length() <= 0) {
			displayLog("Invalid plate number.", false);
			return;
		}
		
		ParkingEntry pe = mainServer.getParkingEntryFromPlateNumStr(pnStr);
		MemberCarEntry mce = mainServer.getMemberEntryFromPlateNumStr(pnStr);
		String bc = "";
		
		if (pe != null) {
			bc = pe.barcodeId;
		} else if (mce != null) {
			bc = mce.barcodeId;
		}
		
		
		if (mce != null) {
			
			mainServer.setCurrentMemberCarEntry(mce);
			
			barcodeSearchButton.setVisible(false);
			barcodeTextField.setText(bc);
			plateNumSearchButton.setVisible(false);
			
			nameTextField.setText(mce.firstname);
			brandTextField.setText(mce.brand);
			addressTextField.setText(mce.address);
			telTextField.setText(mce.tel);
			
			switch (mce.memberType) {
			
			case MONTHLY_ALL:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(false);
				monthlyNightRadioButton.setSelected(false);
				monthlyAllRadioButton.setSelected(true);
				break;
			
			case MONTHLY_DAY:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(true);
				monthlyNightRadioButton.setSelected(false);
				monthlyAllRadioButton.setSelected(false);
				break;
				
			case MONTHLY_NIGHT:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(false);
				monthlyNightRadioButton.setSelected(true);
				monthlyAllRadioButton.setSelected(false);
				break;
				
			default:
				monthlyTypeButtonGroup.clearSelection();
				monthlyDayRadioButton.setSelected(false);
				monthlyNightRadioButton.setSelected(false);
				monthlyAllRadioButton.setSelected(false);
			
			}
			
			switch (mce.entryType) {
				
				case CAR:
					carTypeButtonGroup.clearSelection();
					carRadioButton.setSelected(true);
					motorcycleRadioButton.setSelected(false);
					break;
					
				case MOTORCYCLE:
					carTypeButtonGroup.clearSelection();
					carRadioButton.setSelected(false);
					motorcycleRadioButton.setSelected(true);
					break;
					
				default:
					carTypeButtonGroup.clearSelection();
					carRadioButton.setSelected(true);
					motorcycleRadioButton.setSelected(false);
					break;
			
			}
			
			issueDatePicker.setDate(new Date(mce.issueDate));
			expDatePicker.setDate(new Date(mce.expirationDate));
			
			if (pe != null) {
				
				mainServer.setCurrentParkingEntry(pe);
				
				barcodeTextField.setEditable(false);
				plateNumTextField.setEditable(true);
				
				nameTextField.setEditable(true);
				brandTextField.setEditable(true);
				addressTextField.setEditable(true);
				telTextField.setEditable(true);
				
				monthlyDayRadioButton.setEnabled(true);
				monthlyNightRadioButton.setEnabled(true);
				monthlyAllRadioButton.setEnabled(true);
				
				carRadioButton.setEnabled(true);
				motorcycleRadioButton.setEnabled(true);
				
				issueDatePicker.setEditable(true);
				expDatePicker.setEditable(true);
				
				saveChangesButton.setVisible(true);
				removeRecordButton.setVisible(false);
				removeBarcodeButton.setVisible(false);
				addRecordButton.setVisible(false);
				addBarcodeButton.setVisible(false);
				cancelButton.setVisible(true);
				
				displayLog("Member found. Status: PARKING",true);
				
			} else {
				
				barcodeTextField.setEditable(true);
				plateNumTextField.setEditable(true);
				
				nameTextField.setEditable(true);
				brandTextField.setEditable(true);
				addressTextField.setEditable(true);
				telTextField.setEditable(true);
				
				monthlyDayRadioButton.setEnabled(true);
				monthlyNightRadioButton.setEnabled(true);
				monthlyAllRadioButton.setEnabled(true);
				
				carRadioButton.setEnabled(true);
				motorcycleRadioButton.setEnabled(true);
				
				issueDatePicker.setEditable(true);
				expDatePicker.setEditable(true);
				
				saveChangesButton.setVisible(true);
				removeRecordButton.setVisible(true);
				removeBarcodeButton.setVisible(false);
				addRecordButton.setVisible(false);
				addBarcodeButton.setVisible(false);
				cancelButton.setVisible(true);
				
				displayLog("Member found. Status: NOT PARKING",true);
			
			}
		
		} else if (pe != null) {
			
			mainServer.setCurrentParkingEntry(pe);
			
			barcodeTextField.setEditable(false);
			barcodeSearchButton.setVisible(false);
			plateNumTextField.setEditable(false);
			plateNumSearchButton.setVisible(false);
			
			barcodeTextField.setText(bc);
			
			nameTextField.setText("");
			brandTextField.setText("");
			addressTextField.setText("");
			telTextField.setText("");
			
			monthlyTypeButtonGroup.clearSelection();
			monthlyDayRadioButton.setSelected(false);
			monthlyNightRadioButton.setSelected(false);
			monthlyAllRadioButton.setSelected(false);
			
			issueDatePicker.setDate(new Date(0));
			expDatePicker.setDate(new Date(0));
			
			nameTextField.setEditable(false);
			brandTextField.setEditable(false);
			addressTextField.setEditable(false);
			telTextField.setEditable(false);
			
			monthlyDayRadioButton.setEnabled(false);
			monthlyNightRadioButton.setEnabled(false);
			monthlyAllRadioButton.setEnabled(false);
			
			carRadioButton.setEnabled(false);
			motorcycleRadioButton.setEnabled(false);
			
			issueDatePicker.setEditable(false);
			expDatePicker.setEditable(false);
				
			saveChangesButton.setVisible(false);
			removeRecordButton.setVisible(false);
			removeBarcodeButton.setVisible(false);
			addRecordButton.setVisible(false);
			addBarcodeButton.setVisible(false);
			cancelButton.setVisible(true);
			
			switch (pe.entryType) {
			
			case CAR:
				carTypeButtonGroup.clearSelection();
				carRadioButton.setSelected(true);
				motorcycleRadioButton.setSelected(false);
				break;
				
			case MOTORCYCLE:
				carTypeButtonGroup.clearSelection();
				carRadioButton.setSelected(false);
				motorcycleRadioButton.setSelected(true);
				break;
				
			default:
				carTypeButtonGroup.clearSelection();
				carRadioButton.setSelected(true);
				motorcycleRadioButton.setSelected(false);
		
			}
				
			displayLog("Parking vehicle found.",true);
			
		} else {
			
			barcodeTextField.setText("");
			
			barcodeSearchButton.setVisible(false);
			plateNumSearchButton.setVisible(false);
			
			barcodeTextField.setEditable(true);
			plateNumTextField.setEditable(false);
			
			nameTextField.setEditable(true);
			brandTextField.setEditable(true);
			addressTextField.setEditable(true);
			telTextField.setEditable(true);
			
			monthlyDayRadioButton.setEnabled(true);
			monthlyNightRadioButton.setEnabled(true);
			monthlyAllRadioButton.setEnabled(true);
			
			carRadioButton.setEnabled(true);
			motorcycleRadioButton.setEnabled(true);
			
			issueDatePicker.setEditable(true);
			expDatePicker.setEditable(true);
			
			saveChangesButton.setVisible(false);
			removeRecordButton.setVisible(false);
			removeBarcodeButton.setVisible(false);
			addRecordButton.setVisible(true);
			addBarcodeButton.setVisible(false);
			cancelButton.setVisible(true);
			
			displayLog("PLATE NUMBER NOT FOUND", false);
			
		}
		
		
	}
	
	private void saveChangesPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		String bc = getBarcodeString();
		
		if (mainServer.getCurrentMemberCarEntry() == null) {
			
			displayLog("ERROR: changing info failed",false);
			
			mainServer.setCurrentBarcodeIndex(-1);
			mainServer.setCurrentMemberCarEntry(null);
			mainServer.setCurrentParkingEntry(null);
			
			resetFields();
			return;
		}
		
		if (bc==null) {
			displayLog("ERROR: The barcode is invalid.",false);
			return;
		}
		
		if (mainServer.getBarcodeIndex(bc) >= 0) {
			displayLog("ERROR: The barcode is already used in normal barcode list.",false);
			return;
		}
		
		MemberCarEntry memberFromBarcode = mainServer.getMemberEntryFromBarcode(bc);
		
		if (memberFromBarcode != null && memberFromBarcode != mainServer.getCurrentMemberCarEntry()) {
			displayLog("ERROR: The barcode is used by another member.",false);
			return;
		}
		
		String pnStr = plateNumTextField.getText();
		
		if (pnStr.length() <= 0) {
			displayLog("ERROR: Please enter plate number.",false);
			return;
		}
		
//		MemberCarEntry memberFromPlateNum = mainServer.getMemberEntryFromPlateNumStr(pnStr);
//		
//		if (memberFromPlateNum != null && memberFromPlateNum != mainServer.getCurrentMemberCarEntry()) {
//			displayLog("ERROR: The plate number is used by another member.",false);
//			return;
//		}
		
		if (nameTextField.getText().length() <= 0) {
			displayLog("ERROR: Please enter a name.",false);
			return;
		}
		
		if (brandTextField.getText().length() <= 0) {
			displayLog("ERROR: Please enter a model.",false);
			return;
		}
		
		if (addressTextField.getText().length() <= 0) {
			displayLog("ERROR: Please enter an address.",false);
			return;
		}
		
		
		if ((!monthlyDayRadioButton.isSelected()) && (!monthlyNightRadioButton.isSelected()) && (!monthlyAllRadioButton.isSelected())) {
			displayLog("ERROR: Please pick a member type.",false);
			return;
		}
		
		if ((!carRadioButton.isSelected()) && (!motorcycleRadioButton.isSelected())) {
			displayLog("ERROR: Please pick a vehicle type.", false);
			return;
		}
		
		if (issueDatePicker.getDate().compareTo(expDatePicker.getDate())>=0) {
			displayLog("ERROR: Expiration date must be after issue date.", false);
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to CHANGE information of member "+bc+ ": "+ pnStr +" ?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Changing info cancelled.",false);
			return;
		}
		
		MemberCarEntry infoEntry = new MemberCarEntry();
		infoEntry.barcodeId = bc;
		//infoEntry.plateNumberBytes = pnStr.getBytes();
		infoEntry.plateNumString = pnStr;
		if (monthlyDayRadioButton.isSelected()) {
			infoEntry.memberType = MemberType.MONTHLY_DAY;
		} else if (monthlyNightRadioButton.isSelected()) {
			infoEntry.memberType = MemberType.MONTHLY_NIGHT;
		} else if (monthlyAllRadioButton.isSelected()) {
			infoEntry.memberType = MemberType.MONTHLY_ALL;
		}
		
		if (carRadioButton.isSelected()) {
			infoEntry.entryType = EntryType.CAR;
		} else if (motorcycleRadioButton.isSelected()) {
			infoEntry.entryType = EntryType.MOTORCYCLE;
		}
		
		infoEntry.issueDate = issueDatePicker.getDate().getTime();
		infoEntry.expirationDate = expDatePicker.getDate().getTime();
		infoEntry.firstname = nameTextField.getText();
		infoEntry.address = addressTextField.getText();
		infoEntry.brand = brandTextField.getText();
		infoEntry.tel = telTextField.getText();
		
		if (mainServer.changeCurrentMemberInfo(infoEntry)) {
			displayLog("Member info updated", true);
		} else {
			displayLog("ERROR: changing info failed",false);
		}
		
		/*
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		resetFields();
		*/
		
		passwordTextField.setText("");
		
	}
	
	private void addRecordPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		String bc = getBarcodeString();
		
		if (bc==null) {
			displayLog("ERROR: The barcode is invalid.",false);
			return;
		}
		
		if ((mainServer.getBarcodeIndex(bc) >= 0) || mainServer.getMemberEntryFromBarcode(bc) != null) {
			displayLog("ERROR: The barcode is already used.",false);
			return;
		}
		
		String pnStr = plateNumTextField.getText();
		
		if (pnStr.length() <= 0) {
			displayLog("ERROR: Please enter plate number.",false);
			return;
		}
		
//		if (mainServer.getMemberEntryFromPlateNumStr(pnStr) != null) {
//			displayLog("ERROR: The plate number is already used.",false);
//			return;
//		}
		
		if (nameTextField.getText().length() <= 0) {
			displayLog("ERROR: Please enter a name.",false);
			return;
		}
		
		if (brandTextField.getText().length() <= 0) {
			displayLog("ERROR: Please enter a model.",false);
			return;
		}
		
		if (addressTextField.getText().length() <= 0) {
			displayLog("ERROR: Please enter an address.",false);
			return;
		}
		
		
		if ((!monthlyDayRadioButton.isSelected()) && (!monthlyNightRadioButton.isSelected()) && (!monthlyAllRadioButton.isSelected())) {
			displayLog("ERROR: Please pick a member type.",false);
			return;
		}
		
		if ((!carRadioButton.isSelected()) && (!motorcycleRadioButton.isSelected())) {
			displayLog("ERROR: Please pick a vehicle type.",false);
			return;
		}
		
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		
		if (issueDatePicker.getDate().equals(c.getTime())) {
			displayLog("ERROR: Please pick an issue date.", false);
			return;
		}
		
		if (expDatePicker.getDate().equals(c.getTime())) {
			displayLog("ERROR: Please pick an expiration date.", false);
			return;
		}
		
		if (issueDatePicker.getDate().compareTo(expDatePicker.getDate())>=0) {
			displayLog("ERROR: Expiration date must be after issue date.", false);
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to ADD "+bc+ ": "+ pnStr +" to the member list?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Adding member cancelled.",false);
			return;
		}
		
		MemberCarEntry mce = new MemberCarEntry();
		mce.barcodeId = bc;
		//mce.plateNumberBytes = pnStr.getBytes();
		mce.plateNumString = pnStr;
		if (monthlyDayRadioButton.isSelected()) {
			mce.memberType = MemberType.MONTHLY_DAY;
		} else if (monthlyNightRadioButton.isSelected()) {
			mce.memberType = MemberType.MONTHLY_NIGHT;
		} else if (monthlyAllRadioButton.isSelected()) {
			mce.memberType = MemberType.MONTHLY_ALL;
		}
		
		if (carRadioButton.isSelected()) {
			mce.entryType = EntryType.CAR;
		} else if (motorcycleRadioButton.isSelected()) {
			mce.entryType = EntryType.MOTORCYCLE;
		}
		
		mce.issueDate = issueDatePicker.getDate().getTime();
		mce.expirationDate = expDatePicker.getDate().getTime();
		mce.firstname = nameTextField.getText();
		mce.address = addressTextField.getText();
		mce.brand = brandTextField.getText();
		mce.tel = telTextField.getText();
		
		if (mainServer.addMember(mce)) {
			displayLog("Member: "+bc+" has been successfully added.", true);
		} else {
			displayLog("ERROR: The barcode or plate number is already used.",false);
		}
		
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		resetFields();
		
	}
	
	private void addBarcodePressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		String bc = getBarcodeString();
		
		if (bc == null) {
			displayLog("ERROR: The barcode is invalid.",false);
			mainServer.setCurrentBarcodeIndex(-1);
			mainServer.setCurrentMemberCarEntry(null);
			mainServer.setCurrentParkingEntry(null);
			
			resetFields();
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to ADD "+bc+" to the barcode list?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Adding barcode cancelled.",false);
			return;
		}
		
		if (mainServer.addBarcode(bc)) {
			displayLog("Barcode: "+bc+" has been successfully added.", true);
		} else {
			displayLog("ERROR: The barcode is already in use.",false);
		}
		
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		resetFields();
		
	}
	
	private void removeRecordPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		MemberCarEntry mce = mainServer.getCurrentMemberCarEntry();
		
		if (mce == null) {
			displayLog("ERROR: Removing member failed",false);
			mainServer.setCurrentBarcodeIndex(-1);
			mainServer.setCurrentMemberCarEntry(null);
			mainServer.setCurrentParkingEntry(null);
			
			resetFields();
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to REMOVE "+mce.barcodeId+": "+mce.getPlateNumString()+" from the member list?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Removing member cancelled.",false);
			return;
		}
		
		if (mainServer.removeCurrentMember()) {
			displayLog("Member: "+mce.barcodeId+": "+mce.getPlateNumString()+" has been successfully removed.", true);
			
		} else {
			displayLog("ERROR: Removing member failed",false);
		}
		
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		resetFields();
		
	}
	
	private void removeBarcodePressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		String bc = getBarcodeString();
		
		if (bc==null) {
			displayLog("ERROR: The barcode is invalid.",false);
			mainServer.setCurrentBarcodeIndex(-1);
			mainServer.setCurrentMemberCarEntry(null);
			mainServer.setCurrentParkingEntry(null);
			
			resetFields();
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to REMOVE "+bc+" from the barcode list?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Removing barcode cancelled.",false);
			return;
		}
		
		if (mainServer.removeBarcode(bc)) {
			displayLog("Barcode: "+bc+" has been successfully removed.", true);
		} else {
			displayLog("ERROR: The barcode is being used by a vehicle/member, or it is not in the system.",false);
		}
		
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		resetFields();
		
	}
	
	private void cancelPressed() {
		
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		//barcodeTextField.setText("");
		resetFields();
		displayLog("Canceled",false);
		
	}
	
	private void transactionEditorPressed() {
		
		if (!isPasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		mainServer.setCurrentBarcodeIndex(-1);
		mainServer.setCurrentMemberCarEntry(null);
		mainServer.setCurrentParkingEntry(null);
		
		resetFields();
		
		teDialog.setLocationRelativeTo(parentFrame);
		teDialog.setVisible(true);
		
	}
	
	private void infoParkingEntriesPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to rebuild parking entries info?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			mainServer.buildParkingReport();
			displayLog("Parking entries info built.",true);
		} else {
			displayLog("Building parking entries info cancelled.", false);
		}
		
		passwordTextField.setText("");
	}
	
	private void infoMemberEntriesPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to rebuild members info?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			mainServer.buildMemberReport();
			displayLog("Members info built.",true);
		} else {
			displayLog("Building members info cancelled.", false);
		}
		
		passwordTextField.setText("");
	}
	
	private void infoBarcodesPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to rebuild barcode info?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			mainServer.buildBarcodeReport();
			displayLog("Barcode info built.",true);
		} else {
			displayLog("Building barcode info cancelled.", false);
		}
		
		passwordTextField.setText("");
	}
	
	private void infoDailyPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		int date = MyUtil.getDateIntFromText(infoDailyDateTextField.getText());
		int month = MyUtil.getMonthIntFromText(infoDailyMonthTextField.getText());
		
		if (date <= 0) {
			displayLog("Invalid date.",false);
			passwordTextField.setText("");
			return;
		}
		
		if (month <= 0 || month >12) {
			displayLog("Invalid month", false);
			passwordTextField.setText("");
			return;
		}
		
		month -= 1;
		
		Calendar cDate = Calendar.getInstance();
		cDate.set(Calendar.MONTH, month);
		if (date > cDate.getActualMaximum(Calendar.DATE)) {
			displayLog("Invalid date.",false);
			passwordTextField.setText("");
			return;
		}
		cDate.set(Calendar.DATE, date);
		
		SimpleDateFormat dayMonthFormatter = new SimpleDateFormat("dd MMMMM");
		
		String dateStr = dayMonthFormatter.format(cDate.getTime());
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to build daily info for "+dateStr+"?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			mainServer.buildDailyTransactionReport(cDate);
			displayLog("Daily info for "+ dateStr +" built.",true);
		} else {
			displayLog("Building daily info cancelled.", false);
		}
		
		passwordTextField.setText("");
		
	}
	
	private void infoMonthlyPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		int month = MyUtil.getMonthIntFromText(infoMonthlyMonthTextField.getText());
		
		if (month <= 0 || month >12) {
			displayLog("Invalid month", false);
			passwordTextField.setText("");
			return;
		}
		
		month -= 1;
		
		Calendar cMonth = Calendar.getInstance();
		cMonth.set(Calendar.MONTH, month);
		
		SimpleDateFormat monthFormatter = new SimpleDateFormat("MMMMM");
		
		String monthStr = monthFormatter.format(cMonth.getTime());
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to build monthly info for "+monthStr+"?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			mainServer.buildMonthlyTransactionReport(month);
			displayLog("Monthly info for "+ monthStr +" built.",true);
		} else {
			displayLog("Building monthly info cancelled.", false);
		}
		
		passwordTextField.setText("");
		
	}
	
	private void reportProcessedMonthlyPressed() {
		
		if (!isPasswordCorrect() && !isEmployeePasswordCorrect()) {
			displayLog("ERROR: Incorrect password.",false);
			passwordTextField.setText("");
			return;
		}
		
		int month = MyUtil.getMonthIntFromText(infoMonthlyMonthTextField.getText());
		
		if (month <= 0 || month >12) {
			displayLog("Invalid month", false);
			passwordTextField.setText("");
			return;
		}
		
		month -= 1;
		
		Calendar cMonth = Calendar.getInstance();
		cMonth.set(Calendar.MONTH, month);
		
		SimpleDateFormat monthFormatter = new SimpleDateFormat("MMMMM");
		
		String monthStr = monthFormatter.format(cMonth.getTime());
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to build precessed monthly report for "+monthStr+"?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			mainServer.buildProcessedMonthlyTransactionReport(month,mainServer.processedReportModNum);
			displayLog("Processed monthly report for "+ monthStr +" built.",true);
		} else {
			displayLog("Building processed monthly report cancelled.", false);
		}
		
		passwordTextField.setText("");
		
	}
	
	private void displayLog(String msg, boolean isGood) {
		logLabel.setText(msg);
		if (isGood) {
			logLabel.setForeground(new Color(0,255,0));
		} else {
			logLabel.setForeground(new Color(255,0,0));
		}
	}
	
	public static void main(String[] args) {
		
		loadPassword();
		loadEmployeePassword();
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowServerGUI();
            }
        });
		
		/*
		Calendar c11 = Calendar.getInstance();
		c11.add(Calendar.MONTH, -4);
		
		Calendar c12 = Calendar.getInstance();
		c12.add(Calendar.MONTH, -3);
		
		Calendar c21 = Calendar.getInstance();
		c21.add(Calendar.MONTH, -1);
		Calendar c22 = Calendar.getInstance();
		
		ParkingEntry pe1 = new ParkingEntry(c11,c12,EntryType.MOTORCYCLE);
		ParkingEntry pe2 = new ParkingEntry(c21,c22,EntryType.NORMAL);
		
		ArrayList<ParkingEntry> pArray = new ArrayList<ParkingEntry>();
		pArray.add(pe1);
		pArray.add(pe2);
		*/
		/*
		try {
			FileOutputStream fos = new FileOutputStream("parkingList");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(pArray);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		/*
		try {
			FileInputStream fis = new FileInputStream("parkingList");
			ObjectInputStream ois = new ObjectInputStream(fis);
			ArrayList<ParkingEntry> read = (ArrayList<ParkingEntry>) ois.readObject();
			for (int i = 0; i<read.size(); i++) {
				Calendar c1 = Calendar.getInstance();
				c1.setTimeInMillis(read.get(i).entryUTC);
				Calendar c2 = Calendar.getInstance();
				c2.setTimeInMillis(read.get(i).exitUTC);
				
				System.out.println("" + i + ": "+ c1.get(Calendar.MONTH) + " "+c2.get(Calendar.MONTH) + read.get(i).entryType.toString());
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
		
	}
	
	private boolean isPasswordCorrect() {
		return isPasswordCorrect(passwordTextField.getPassword());
	}
	
	private boolean isEmployeePasswordCorrect() {
		return isEmployeePasswordCorrect(passwordTextField.getPassword());
	}
	
	private static boolean isPasswordCorrect(char[] input) {
		
		char[] correctPassword = loadPassword();
		
	    boolean isCorrect = true;

	    if (input.length != correctPassword.length) {
	        isCorrect = false;
	    } else {
	        isCorrect = Arrays.equals (input, correctPassword);
	    }

	    Arrays.fill(correctPassword, '0');
	    
	    return isCorrect;
	}
	
private static boolean isEmployeePasswordCorrect(char[] input) {
		
		char[] correctPassword = loadEmployeePassword();
		
	    boolean isCorrect = true;

	    if (input.length != correctPassword.length) {
	        isCorrect = false;
	    } else {
	        isCorrect = Arrays.equals (input, correctPassword);
	    }

	    Arrays.fill(correctPassword, '0');
	    
	    return isCorrect;
	}
	
	private static char[] loadPassword() {
		
		char[] correctPassword = defaultPassword.clone();
		
		try {
			FileInputStream fis = new FileInputStream(filename_password);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object ro = ois.readObject();
			if (ro instanceof PasswordObject) {
				correctPassword = ((PasswordObject) ro).password;
			} else {
				correctPassword = defaultPassword;
				savePassword(correctPassword);
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			//e.printStackTrace();
			correctPassword = defaultPassword;
			savePassword(correctPassword);
		}
		
		return correctPassword;
	}
	
	private static char[] loadEmployeePassword() {
		
		char[] correctPassword = defaultEmployeePassword.clone();
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(filename_employee_password));
			String str = br.readLine();
			if (str.length() > 0) {
				correctPassword = str.toCharArray();
			} else {
				correctPassword = defaultEmployeePassword;
				saveEmployeePassword(correctPassword);
			}
			br.close();
		} catch (Exception e) {
			//e.printStackTrace();
			correctPassword = defaultEmployeePassword;
			saveEmployeePassword(correctPassword);
		}
		
		return correctPassword;
	}
	
	private static void savePassword(char[] pca) {
		try {
			PasswordObject po = new PasswordObject(pca);
			File dir = new File(filename_password).getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(filename_password);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(po);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static void saveEmployeePassword(char[] pca) {
		
		try {
			
			String str = String.valueOf(pca);
			File file = new File(filename_employee_password);
			File dir = file.getParentFile();
			dir.mkdir();
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(str);
			bw.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private String getBarcodeString() {
		
		String str = barcodeTextField.getText();
//		int barcodeInt = -1;
//		try {
//			barcodeInt = Integer.parseInt(str);
//		} catch (NumberFormatException nfe) {
//			barcodeInt = -1;
//		}
//		if (barcodeInt < 0) {
//			return null;
//		} else {
//			return ""+barcodeInt;
//		}
		return str;
		
	}
	
	public void reconfigure_notConnectedState() {
		
		portTextField.setText(""+ConfigValues.defaultPort);
		displayLog("NOT STARTED",false);
		
	}
	
	public void reconfigure_standbyState() {
		
		passwordTextField.setText("");
		newPasswordTextField.setText("");
		
		//submitNewPasswordButton;
		
		resetFields();
		
		//logLabel.setText("");
		//displayLog("",true);
		
	}
	
	public void resetFields() {
		
		passwordTextField.setText("");
		
		barcodeTextField.setText("");
		barcodeTextField.setEditable(true);
		barcodeSearchButton.setVisible(true);

		plateNumTextField.setText("");
		plateNumTextField.setEditable(true);
		plateNumSearchButton.setVisible(true);

		nameTextField.setText("");
		nameTextField.setEditable(false);
		brandTextField.setText("");
		brandTextField.setEditable(false);
		addressTextField.setText("");
		addressTextField.setEditable(false);
		telTextField.setText("");
		telTextField.setEditable(false);

		monthlyDayRadioButton.setEnabled(false);
		monthlyNightRadioButton.setEnabled(false);
		monthlyAllRadioButton.setEnabled(false);
		
		carRadioButton.setEnabled(false);
		motorcycleRadioButton.setEnabled(false);
		
		carTypeButtonGroup.clearSelection();
		
		monthlyDayRadioButton.setSelected(false);
		monthlyNightRadioButton.setSelected(false);
		monthlyAllRadioButton.setSelected(false);
		monthlyTypeButtonGroup.clearSelection();
		
		issueDatePicker.setDate(new Date());
		issueDatePicker.setEditable(false);
		expDatePicker.setDate(new Date());
		expDatePicker.setEditable(false);
		
		saveChangesButton.setVisible(false);
		removeRecordButton.setVisible(false);
		removeBarcodeButton.setVisible(false);
		addRecordButton.setVisible(false);
		addBarcodeButton.setVisible(false);
		cancelButton.setVisible(false);
	}
	
}
