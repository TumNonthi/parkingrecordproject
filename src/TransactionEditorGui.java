import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.plaf.metal.MetalComboBoxButton;

import org.jdesktop.swingx.JXDatePicker;


public class TransactionEditorGui extends JPanel {

	private MainServer mainServer;
	
	private final int LAYER_LEVEL_BG = 10;
	private final int LAYER_LEVEL_CONTENT = 20;
	
	private Color bgColor = new Color(0,0,0,160);
	
	private JLayeredPane layeredPane;
	
	private JLabel bgLabel;
	
	private MyPanel pePanel;
	
	private JLabel peTitleLabel;
	
	private JLabel transactionIdTitleLabel;
	private JTextField transactionIdTextField;
	private JButton transactionIdSearchButton;
	
	private JLabel peBarcodeTitleLabel;
	private JTextField peBarcodeTextField;
	private JButton peBarcodeSearchButton;
	
	private JLabel pePlateNumTitleLabel;
	private JTextField pePlateNumTextField;
	private JButton pePlateNumSearchButton;
	
	private JLabel peTypeLabel;
	private JRadioButton peMonthlyNoneRadioButton;
	private JRadioButton peMonthlyDayRadioButton;
	private JRadioButton peMonthlyNightRadioButton;
	private JRadioButton peMonthlyAllRadioButton;
	private ButtonGroup peMonthlyTypeButtonGroup;
	
	private JLabel expLabel;
	private JXDatePicker expDatePicker;
	
	private JLabel peEntryTimeTitleLabel;
	private JXDatePicker peEntryDatePicker;
	
	private JLabel peEntryHourLabel;
	private JTextField peEntryHourTextField;
	
	private JLabel peEntryMinLabel;
	private JTextField peEntryMinTextField;
	
	private JButton peCheckInButton;
	private JButton peSaveChangesButton;
	
	private JLabel peExitTimeTitleLabel;
	private JXDatePicker peExitDatePicker;
	
	private JLabel peExitHourLabel;
	private JTextField peExitHourTextField;
	
	private JLabel peExitMinLabel;
	private JTextField peExitMinTextField;
	
	private JLabel motorcycleLabel;
	private JRadioButton motorcycleYesRadioButton;
	private JRadioButton motorcycleNoRadioButton;
	private ButtonGroup motorcycleButtonGroup;
	
	private JLabel ckTitleLabel;
	private JRadioButton ckYesRadioButton;
	private JRadioButton ckNoRadioButton;
	private ButtonGroup ckButtonGroup;
	
	private JLabel transactionPriceTitleLabel;
	private JLabel transactionPriceLabel;
	
	private JButton peCheckOutButton;
	private JButton peDeleteButton;
	
	private JButton transactionSaveChangesButton;
	private JButton transactionDeleteButton;
	
	private JButton cancelButton;
	
	private JLabel logLabel;
	
	private ParkingEntry currentTransaction;
	
	
	public TransactionEditorGui(MainServer mainServer) {
	
		this.mainServer = mainServer;
		
		setPreferredSize(new Dimension(680,510));
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		layeredPane = new JLayeredPane();
	
		bgLabel = new JLabel();
        Icon bgIcon = new ImageIcon("UI/bg/bg01.jpg");
        bgLabel.setBounds(0,0,680,510);
        bgLabel.setIcon(bgIcon);
        layeredPane.add(bgLabel, new Integer(LAYER_LEVEL_BG));
        
       	pePanel = new MyPanel();
       	pePanel.setBounds(20,20,640,470);
       	pePanel.setBackground(bgColor);
       	pePanel.setOpaque(false);
       	pePanel.setLayout(null);
        layeredPane.add(pePanel, new Integer(LAYER_LEVEL_CONTENT));
        
        
        peTitleLabel = new JLabel("Transaction / Parking Entry");
        peTitleLabel.setBounds(220, 20, 160, 25);
        pePanel.add(peTitleLabel);
        
        transactionIdTitleLabel = new JLabel("Transaction ID");
        transactionIdTitleLabel.setBounds(20,55,100,25);
        pePanel.add(transactionIdTitleLabel);
        
        transactionIdTextField = new JTextField("");
        transactionIdTextField.setBounds(120,55,150,25);
        pePanel.add (transactionIdTextField);
        
        transactionIdSearchButton = new JButton("Search Transaction");
        transactionIdSearchButton.setBounds(280,55,140,25);
        transactionIdSearchButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		transactionIdSearchPressed();
        	}
        });
        pePanel.add(transactionIdSearchButton);
        
        peBarcodeTitleLabel = new JLabel("Barcode");
        peBarcodeTitleLabel.setBounds(20,90,100,25);
        pePanel.add (peBarcodeTitleLabel);
        
        peBarcodeTextField = new JTextField("");
        peBarcodeTextField.setBounds(120,90,150,25);
        pePanel.add (peBarcodeTextField);
        
        peBarcodeSearchButton = new JButton("Search Parking Entry");
        peBarcodeSearchButton.setBounds(280,90,150,25);
        peBarcodeSearchButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		peBarcodeSearchPressed();
        	}
        });
        pePanel.add(peBarcodeSearchButton);
        
        pePlateNumTitleLabel = new JLabel("ทบ. รถ");
        pePlateNumTitleLabel.setBounds(20,125,80,25);
        pePanel.add (pePlateNumTitleLabel);
        
        pePlateNumTextField = new JTextField("");
        pePlateNumTextField.setBounds(120,125,150,25);
        pePanel.add (pePlateNumTextField);
        
        pePlateNumSearchButton = new JButton("Search Parking Entry");
        pePlateNumSearchButton.setBounds(280,125,150,25);
        pePlateNumSearchButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		pePlateNumSearchPressed();
        	}
        });
        pePanel.add(pePlateNumSearchButton);
        
        peTypeLabel = new JLabel("ประเภท");
        peTypeLabel.setBounds(20, 160, 100, 25);
        pePanel.add(peTypeLabel);
        
        peMonthlyAllRadioButton = new JRadioButton("All", false);
        peMonthlyAllRadioButton.setBounds(120, 160, 70, 25);
        pePanel.add(peMonthlyAllRadioButton);
        
        peMonthlyDayRadioButton = new JRadioButton("Day", false);
        peMonthlyDayRadioButton.setBounds(200, 160, 70, 25);
        pePanel.add(peMonthlyDayRadioButton);
        
        peMonthlyNightRadioButton = new JRadioButton("Night", false);
        peMonthlyNightRadioButton.setBounds(280, 160, 70, 25);
        pePanel.add(peMonthlyNightRadioButton);
        
        peMonthlyNoneRadioButton = new JRadioButton("None", false);
        peMonthlyNoneRadioButton.setBounds(360, 160, 70, 25);
        pePanel.add(peMonthlyNoneRadioButton);
        
        peMonthlyTypeButtonGroup = new ButtonGroup();
        peMonthlyTypeButtonGroup.add(peMonthlyAllRadioButton);
        peMonthlyTypeButtonGroup.add(peMonthlyDayRadioButton);
        peMonthlyTypeButtonGroup.add(peMonthlyNightRadioButton);
        peMonthlyTypeButtonGroup.add(peMonthlyNoneRadioButton);
        
        expLabel = new JLabel("exp.");
        expLabel.setBounds(440, 160, 30, 25);
        pePanel.add(expLabel);
        
        expDatePicker = new JXDatePicker();
        expDatePicker.setBounds(470, 160, 160, 25);
        expDatePicker.setDate(new Date(0));
        pePanel.add(expDatePicker);
        
        /*
        String[] hourList = new String[24];
        for (int i = 0; i<24; i++) {
        	hourList[i] = ""+i;
        }
        
        String[] minList = new String[60];
        for (int i = 0; i<60; i++) {
        	minList[i] = ""+i;
        }
        */
        
        peEntryTimeTitleLabel = new JLabel("Entry Time");
        peEntryTimeTitleLabel.setBounds(20,195,100,25);
        pePanel.add(peEntryTimeTitleLabel);
        
        peEntryDatePicker = new JXDatePicker();
        peEntryDatePicker.setBounds(120, 195, 160, 25);
        peEntryDatePicker.setDate(new Date(0));
        pePanel.add(peEntryDatePicker);
        
        peEntryHourLabel = new JLabel("H:");
        peEntryHourLabel.setBounds(300, 195, 25, 25);
        pePanel.add(peEntryHourLabel);
        
        peEntryHourTextField = new JTextField("0");
        peEntryHourTextField.setBounds(325, 195, 50, 25);
        pePanel.add(peEntryHourTextField);
        
        peEntryMinLabel = new JLabel("M:");
        peEntryMinLabel.setBounds(380, 195, 25, 25);
        pePanel.add(peEntryMinLabel);
        
        peEntryMinTextField = new JTextField("0");
        peEntryMinTextField.setBounds(405, 195, 50, 25);
        pePanel.add(peEntryMinTextField);
        
        peCheckInButton = new JButton("Check-in");
        peCheckInButton.setBounds(20, 230, 80, 25);
        peCheckInButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		peCheckInPressed();
        	}
        });
        pePanel.add(peCheckInButton);
        
        peSaveChangesButton = new JButton("Save Changes");
        peSaveChangesButton.setBounds(120, 230, 120, 25);
        peSaveChangesButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		peSaveChangesPressed();
        	}
        });
        pePanel.add(peSaveChangesButton);
        
        peDeleteButton = new JButton("Delete Parking Entry");
        peDeleteButton.setBounds(260, 230, 160, 25);
        peDeleteButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		peDeletePressed();
        	}
        });
        
        peDeleteButton.setBackground(Color.RED);
        pePanel.add(peDeleteButton);
        
        
        peExitTimeTitleLabel = new JLabel("Exit Time");
        peExitTimeTitleLabel.setBounds(20,275,100,25);
        pePanel.add(peExitTimeTitleLabel);
        
        peExitDatePicker = new JXDatePicker();
        peExitDatePicker.setBounds(120, 275, 160, 25);
        peExitDatePicker.setDate(new Date(0));
        pePanel.add(peExitDatePicker);
        
        peExitHourLabel = new JLabel("H:");
        peExitHourLabel.setBounds(300, 275, 25, 25);
        pePanel.add(peExitHourLabel);
        
        peExitHourTextField = new JTextField("0");
        peExitHourTextField.setBounds(325, 275, 50, 25);
        pePanel.add(peExitHourTextField);
        
        peExitMinLabel = new JLabel("M:");
        peExitMinLabel.setBounds(380, 275, 25, 25);
        pePanel.add(peExitMinLabel);
        
        peExitMinTextField = new JTextField("0");
        peExitMinTextField.setBounds(405, 275, 50, 25);
        pePanel.add(peExitMinTextField);
        
        
        motorcycleLabel = new JLabel("Motorcycle");
        motorcycleLabel.setBounds(430, 240, 65, 25);
        pePanel.add(motorcycleLabel);
        
        motorcycleYesRadioButton = new JRadioButton("Yes", false);
        motorcycleYesRadioButton.setBounds(500, 240, 50, 25);
        pePanel.add (motorcycleYesRadioButton);
        
        motorcycleNoRadioButton = new JRadioButton("No", false);
        motorcycleNoRadioButton.setBounds(555, 240, 50, 25);
        pePanel.add (motorcycleNoRadioButton);
        
        motorcycleButtonGroup = new ButtonGroup();
        motorcycleButtonGroup.add(motorcycleYesRadioButton);
        motorcycleButtonGroup.add(motorcycleNoRadioButton);
        
        
        ckTitleLabel = new JLabel("CK");
        ckTitleLabel.setBounds(465, 275, 30, 25);
        pePanel.add (ckTitleLabel);
        
        ckYesRadioButton = new JRadioButton("Yes", false);
        ckYesRadioButton.setBounds(500, 275, 50, 25);
        pePanel.add (ckYesRadioButton);
        
        ckNoRadioButton = new JRadioButton("No", false);
        ckNoRadioButton.setBounds(555, 275, 50, 25);
        pePanel.add (ckNoRadioButton);
        
        ckButtonGroup = new ButtonGroup();
        ckButtonGroup.add(ckYesRadioButton);
        ckButtonGroup.add(ckNoRadioButton);
        
        
        peCheckOutButton = new JButton("Check-out");
        peCheckOutButton.setBounds(20, 310, 90, 25);
        peCheckOutButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		peCheckOutPressed();
        	}
        });
        pePanel.add(peCheckOutButton);
        
        transactionPriceTitleLabel = new JLabel("Price");
        transactionPriceTitleLabel.setBounds(20, 345, 80, 25);
        pePanel.add(transactionPriceTitleLabel);
        
        transactionPriceLabel = new JLabel("-");
        transactionPriceLabel.setBounds(120, 345, 460, 25);
        pePanel.add(transactionPriceLabel);
        
        transactionSaveChangesButton = new JButton("Save Changes");
        transactionSaveChangesButton.setBounds(120, 380, 120, 25);
        transactionSaveChangesButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		transactionSaveChangesPressed();
        	}
        });
        pePanel.add(transactionSaveChangesButton);
        
        transactionDeleteButton = new JButton("Delete Transaction");
        transactionDeleteButton.setBounds(260, 380, 160, 25);
        transactionDeleteButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		transactionDeletePressed();
        	}
        });
        
        transactionDeleteButton.setBackground(Color.RED);
        pePanel.add(transactionDeleteButton);
        
        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(440, 380, 100, 25);
        cancelButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		cancelPressed();
        	}
        });
        pePanel.add(cancelButton);
        
        logLabel = new JLabel("Editor Opened");
        logLabel.setBounds(20, 415, 500, 25);
        logLabel.setForeground(Color.GREEN);
        
        pePanel.add(logLabel);
        
        
        CustomGuiUtilities.makeTransparent(pePanel.getComponents());
        
        add(layeredPane);
        
        resetPage();
		
	}
	
	private void transactionIdSearchPressed() {
		
		currentTransaction = null;
		mainServer.setCurrentParkingEntry(null);

		Calendar cEntry = Calendar.getInstance();
		Calendar cExit = Calendar.getInstance();
		Calendar cExp = Calendar.getInstance();
		
		String id = getTransactionIdString();
		
		if (id==null) {
			displayLog("Invalid transaction ID.",false);
			return;
		}
		
		ParkingEntry pe = mainServer.getParkingEntryFromTransactionId(id);
		
		if (pe != null) {
			
			mainServer.setCurrentParkingEntry(pe);
			
			disableFields();
			
			peBarcodeTextField.setText(pe.barcodeId);
			pePlateNumTextField.setText(pe.getPlateNumString());
			
			if (pe.memberType == MemberType.MONTHLY_ALL) {
				peMonthlyAllRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_DAY) {
				peMonthlyDayRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_NIGHT) {
				peMonthlyNightRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else {
				peMonthlyNoneRadioButton.setSelected(true);
			}
			
			if (pe.isCK) {
				ckYesRadioButton.setSelected(true);
			}
			
			if (pe.entryType == EntryType.CAR) {
				motorcycleNoRadioButton.setSelected(true);
			} else if (pe.entryType == EntryType.MOTORCYCLE) {
				motorcycleYesRadioButton.setSelected(true);
			}
			
			cExp.setTimeInMillis(pe.expirationDate);
			expDatePicker.setDate(cExp.getTime());
			
			cEntry.setTimeInMillis(pe.entryUTC);
			
			peEntryDatePicker.setDate(cEntry.getTime());
			peExitDatePicker.setDate(cExit.getTime());
			
			peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
			peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
			peExitHourTextField.setText(""+cExit.get(Calendar.HOUR_OF_DAY));
			peExitMinTextField.setText(""+cExit.get(Calendar.MINUTE));
			
			peSaveChangesButton.setVisible(true);
			peCheckOutButton.setVisible(true);
			peDeleteButton.setVisible(true);
			cancelButton.setVisible(true);
			
			peBarcodeTextField.setEditable(true);
			pePlateNumTextField.setEditable(true);
			peMonthlyAllRadioButton.setEnabled(true);
			peMonthlyDayRadioButton.setEnabled(true);
			peMonthlyNightRadioButton.setEnabled(true);
			peMonthlyNoneRadioButton.setEnabled(true);
			expDatePicker.setEditable(true);
			motorcycleYesRadioButton.setEnabled(true);
			motorcycleNoRadioButton.setEnabled(true);
			ckYesRadioButton.setEnabled(true);
			ckNoRadioButton.setEnabled(true);
			peEntryDatePicker.setEditable(true);
			peEntryHourTextField.setEditable(true);
			peEntryMinTextField.setEditable(true);
			peExitDatePicker.setEditable(true);
			peExitHourTextField.setEditable(true);
			peExitMinTextField.setEditable(true);
			
			displayLog("Parking entry found.", true);
			return;
			
		}
		
		pe = mainServer.getTransaction(id);
		if (pe != null) {
			
			currentTransaction = pe;
			
			disableFields();
			
			peBarcodeTextField.setText(pe.barcodeId);
			pePlateNumTextField.setText(pe.getPlateNumString());
			
			if (pe.memberType == MemberType.MONTHLY_ALL) {
				peMonthlyAllRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_DAY) {
				peMonthlyDayRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_NIGHT) {
				peMonthlyNightRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else {
				peMonthlyNoneRadioButton.setSelected(true);
			}
			
			if (pe.isCK) {
				ckYesRadioButton.setSelected(true);
			}
			
			if (pe.entryType == EntryType.CAR) {
				motorcycleNoRadioButton.setSelected(true);
			} else if (pe.entryType == EntryType.MOTORCYCLE) {
				motorcycleYesRadioButton.setSelected(true);
			}
			
			cExp.setTimeInMillis(pe.expirationDate);
			expDatePicker.setDate(cExp.getTime());
			
			cEntry.setTimeInMillis(pe.entryUTC);
			cExit.setTimeInMillis(pe.exitUTC);
			
			peEntryDatePicker.setDate(cEntry.getTime());
			peExitDatePicker.setDate(cExit.getTime());
			
			peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
			peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
			peExitHourTextField.setText(""+cExit.get(Calendar.HOUR_OF_DAY));
			peExitMinTextField.setText(""+cExit.get(Calendar.MINUTE));
			
			transactionPriceLabel.setText(""+pe.price);
			
			transactionSaveChangesButton.setVisible(true);
			transactionDeleteButton.setVisible(true);
			cancelButton.setVisible(true);
			
			peBarcodeTextField.setEditable(true);
			pePlateNumTextField.setEditable(true);
			peMonthlyAllRadioButton.setEnabled(true);
			peMonthlyDayRadioButton.setEnabled(true);
			peMonthlyNightRadioButton.setEnabled(true);
			peMonthlyNoneRadioButton.setEnabled(true);
			expDatePicker.setEditable(true);
			motorcycleYesRadioButton.setEnabled(true);
			motorcycleNoRadioButton.setEnabled(true);
			ckYesRadioButton.setEnabled(true);
			ckNoRadioButton.setEnabled(true);
			peEntryDatePicker.setEditable(true);
			peEntryHourTextField.setEditable(true);
			peEntryMinTextField.setEditable(true);
			peExitDatePicker.setEditable(true);
			peExitHourTextField.setEditable(true);
			peExitMinTextField.setEditable(true);
			
			displayLog("Completed transaction found.", true);
			return;
			
		}
		
		displayLog("No result found.",false);
		return;
		
	}
	
	private void peBarcodeSearchPressed() {
		
		currentTransaction = null;
		mainServer.setCurrentParkingEntry(null);
		
		transactionIdTextField.setText("");

		Calendar cEntry = Calendar.getInstance();
		Calendar cExit = Calendar.getInstance();
		Calendar cExp = Calendar.getInstance();
		
		String bc = getBarcodeString();
		
		if (bc==null) {
			displayLog("Invalid barcode.",false);
			return;
		}
		
		ParkingEntry pe = mainServer.getParkingEntryFromBarcode(bc);
		MemberCarEntry mce = mainServer.getMemberEntryFromBarcode(bc);
		
		if (pe != null) {
			
			mainServer.setCurrentParkingEntry(pe);
			
			disableFields();
			
			transactionIdTextField.setText(pe.uniqueId);
			pePlateNumTextField.setText(pe.getPlateNumString());
			
			if (pe.memberType == MemberType.MONTHLY_ALL) {
				peMonthlyAllRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_DAY) {
				peMonthlyDayRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_NIGHT) {
				peMonthlyNightRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else {
				peMonthlyNoneRadioButton.setSelected(true);
			}
			
			if (pe.isCK) {
				ckYesRadioButton.setSelected(true);
			}
			
			if (pe.entryType == EntryType.CAR) {
				motorcycleNoRadioButton.setSelected(true);
			} else if (pe.entryType == EntryType.MOTORCYCLE) {
				motorcycleYesRadioButton.setSelected(true);
			}
			
			cExp.setTimeInMillis(pe.expirationDate);
			
			expDatePicker.setDate(cExp.getTime());
			
			cEntry.setTimeInMillis(pe.entryUTC);
			
			peEntryDatePicker.setDate(cEntry.getTime());
			peExitDatePicker.setDate(cExit.getTime());
			
			peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
			peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
			peExitHourTextField.setText(""+cExit.get(Calendar.HOUR_OF_DAY));
			peExitMinTextField.setText(""+cExit.get(Calendar.MINUTE));
			
			peSaveChangesButton.setVisible(true);
			peCheckOutButton.setVisible(true);
			peDeleteButton.setVisible(true);
			cancelButton.setVisible(true);
			
			if (mce != null) {
				displayLog("Parking member found.", true);
			} else {
				displayLog("Parking entry found.", true);
			}
			
			peBarcodeTextField.setEditable(true);
			pePlateNumTextField.setEditable(true);
			
			peMonthlyAllRadioButton.setEnabled(true);
			peMonthlyDayRadioButton.setEnabled(true);
			peMonthlyNightRadioButton.setEnabled(true);
			peMonthlyNoneRadioButton.setEnabled(true);
			
			expDatePicker.setEditable(true);
			
			motorcycleYesRadioButton.setEnabled(true);
			motorcycleNoRadioButton.setEnabled(true);
			
			ckYesRadioButton.setEnabled(true);
			ckNoRadioButton.setEnabled(true);
			
			peEntryDatePicker.setEditable(true);
			peEntryHourTextField.setEditable(true);
			peEntryMinTextField.setEditable(true);
			peExitDatePicker.setEditable(true);
			peExitHourTextField.setEditable(true);
			peExitMinTextField.setEditable(true);
			
			return;
			
		}
		
		displayLog("No result found.", false);
		
		if (mainServer.isInBarcodeList(bc)) {
			
			peMonthlyNoneRadioButton.setSelected(true);
			
			disableFields();
			
			peCheckInButton.setVisible(true);
			cancelButton.setVisible(true);
			
			pePlateNumTextField.setEditable(true);
			
			peEntryDatePicker.setDate(cEntry.getTime());
			peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
			peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
			
			peMonthlyAllRadioButton.setEnabled(true);
			peMonthlyDayRadioButton.setEnabled(true);
			peMonthlyNightRadioButton.setEnabled(true);
			peMonthlyNoneRadioButton.setEnabled(true);
			expDatePicker.setEditable(true);
			
			motorcycleYesRadioButton.setEnabled(true);
			motorcycleNoRadioButton.setEnabled(true);
			
			peEntryDatePicker.setEditable(true);
			peEntryHourTextField.setEditable(true);
			peEntryMinTextField.setEditable(true);
			
			displayLog("No Parking Entry found - Available barcode found.", false);
			
		} else {
			
			if (mce != null) {
				
				disableFields();
				
				pePlateNumTextField.setText(mce.getPlateNumString());
				
				peCheckInButton.setVisible(true);
				cancelButton.setVisible(true);
				
				if (mce.memberType == MemberType.MONTHLY_ALL) {
					peMonthlyAllRadioButton.setSelected(true);
					ckNoRadioButton.setSelected(true);
				} else if (mce.memberType == MemberType.MONTHLY_DAY) {
					peMonthlyDayRadioButton.setSelected(true);
					ckNoRadioButton.setSelected(true);
				} else if (mce.memberType == MemberType.MONTHLY_NIGHT) {
					peMonthlyNightRadioButton.setSelected(true);
					ckNoRadioButton.setSelected(true);
				}
				
				if (mce.entryType == EntryType.CAR) {
					motorcycleNoRadioButton.setSelected(true);
				} else if (mce.entryType == EntryType.MOTORCYCLE) {
					motorcycleYesRadioButton.setSelected(true);
				}
				
				cExp.setTimeInMillis(mce.expirationDate);
				
				expDatePicker.setDate(cExp.getTime());
				
				expDatePicker.setEditable(true);
				
				pePlateNumTextField.setEditable(true);
				
				peMonthlyAllRadioButton.setEnabled(true);
				peMonthlyDayRadioButton.setEnabled(true);
				peMonthlyNightRadioButton.setEnabled(true);
				peMonthlyNoneRadioButton.setEnabled(true);
				expDatePicker.setEditable(true);
				
				motorcycleYesRadioButton.setEnabled(true);
				motorcycleNoRadioButton.setEnabled(true);
				
				peEntryDatePicker.setDate(cEntry.getTime());
				peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
				peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
				
				peEntryDatePicker.setEditable(true);
				peEntryHourTextField.setEditable(true);
				peEntryMinTextField.setEditable(true);
				
				displayLog("No Parking Entry found - Available member found.", false);
				
			}
			
		}
		
	}
	
	private void pePlateNumSearchPressed() {
		
		currentTransaction = null;
		mainServer.setCurrentParkingEntry(null);
		
		transactionIdTextField.setText("");

		Calendar cEntry = Calendar.getInstance();
		Calendar cExit = Calendar.getInstance();
		Calendar cExp = Calendar.getInstance();
		
		String pnStr = pePlateNumTextField.getText();
		
		if (pnStr.length() <= 0) {
			displayLog("Invalid plate number.", false);
			return;
		}
		
		ParkingEntry pe = mainServer.getParkingEntryFromPlateNumStr(pnStr);
		MemberCarEntry mce = mainServer.getMemberEntryFromPlateNumStr(pnStr);
		
		if (pe != null) {
			
			mainServer.setCurrentParkingEntry(pe);
			
			disableFields();
			
			transactionIdTextField.setText(pe.uniqueId);
			peBarcodeTextField.setText(pe.barcodeId);
			
			if (pe.memberType == MemberType.MONTHLY_ALL) {
				peMonthlyAllRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_DAY) {
				peMonthlyDayRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else if (pe.memberType == MemberType.MONTHLY_NIGHT) {
				peMonthlyNightRadioButton.setSelected(true);
				ckNoRadioButton.setSelected(true);
			} else {
				peMonthlyNoneRadioButton.setSelected(true);
			}
			
			if (pe.isCK) {
				ckYesRadioButton.setSelected(true);
			}
			
			if (pe.entryType == EntryType.CAR) {
				motorcycleNoRadioButton.setSelected(true);
			} else if (pe.entryType == EntryType.MOTORCYCLE) {
				motorcycleYesRadioButton.setSelected(true);
			}
			
			cExp.setTimeInMillis(pe.expirationDate);
			
			expDatePicker.setDate(cExp.getTime());
			
			cEntry.setTimeInMillis(pe.entryUTC);
			
			peEntryDatePicker.setDate(cEntry.getTime());
			peExitDatePicker.setDate(cExit.getTime());
			
			peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
			peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
			peExitHourTextField.setText(""+cExit.get(Calendar.HOUR_OF_DAY));
			peExitMinTextField.setText(""+cExit.get(Calendar.MINUTE));
			
			peSaveChangesButton.setVisible(true);
			peCheckOutButton.setVisible(true);
			peDeleteButton.setVisible(true);
			cancelButton.setVisible(true);
			
			peBarcodeTextField.setEditable(true);
			pePlateNumTextField.setEditable(true);
			peMonthlyAllRadioButton.setEnabled(true);
			peMonthlyDayRadioButton.setEnabled(true);
			peMonthlyNightRadioButton.setEnabled(true);
			peMonthlyNoneRadioButton.setEnabled(true);
			expDatePicker.setEditable(true);
			motorcycleYesRadioButton.setEnabled(true);
			motorcycleNoRadioButton.setEnabled(true);
			ckYesRadioButton.setEnabled(true);
			ckNoRadioButton.setEnabled(true);
			peEntryDatePicker.setEditable(true);
			peEntryHourTextField.setEditable(true);
			peEntryMinTextField.setEditable(true);
			peExitDatePicker.setEditable(true);
			peExitHourTextField.setEditable(true);
			peExitMinTextField.setEditable(true);
			
			displayLog("Parking entry found.", true);
			return;
			
		}
		
		if (mce != null) {
			
			if (JOptionPane.showConfirmDialog(null, "No parking entry found, but a member's info is found\nDo you want to fetch the info?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				
				disableFields();
				
				peBarcodeTextField.setText(mce.barcodeId);
				
				peCheckInButton.setVisible(true);
				cancelButton.setVisible(true);
				
				if (mce.memberType == MemberType.MONTHLY_ALL) {
					peMonthlyAllRadioButton.setSelected(true);
					ckNoRadioButton.setSelected(true);
				} else if (mce.memberType == MemberType.MONTHLY_DAY) {
					peMonthlyDayRadioButton.setSelected(true);
					ckNoRadioButton.setSelected(true);
				} else if (mce.memberType == MemberType.MONTHLY_NIGHT) {
					peMonthlyNightRadioButton.setSelected(true);
					ckNoRadioButton.setSelected(true);
				}
				
				if (mce.entryType == EntryType.CAR) {
					motorcycleNoRadioButton.setSelected(true);
				} else if (mce.entryType == EntryType.MOTORCYCLE) {
					motorcycleYesRadioButton.setSelected(true);
				}
				
				cExp.setTimeInMillis(mce.expirationDate);
				
				expDatePicker.setDate(cExp.getTime());
				
				expDatePicker.setEditable(true);
				
				peBarcodeTextField.setEditable(true);
				
				peMonthlyAllRadioButton.setEnabled(true);
				peMonthlyDayRadioButton.setEnabled(true);
				peMonthlyNightRadioButton.setEnabled(true);
				peMonthlyNoneRadioButton.setEnabled(true);
				expDatePicker.setEditable(true);
				
				motorcycleYesRadioButton.setEnabled(true);
				motorcycleNoRadioButton.setEnabled(true);
				
				peEntryDatePicker.setDate(cEntry.getTime());
				peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
				peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
				
				peEntryDatePicker.setEditable(true);
				peEntryHourTextField.setEditable(true);
				peEntryMinTextField.setEditable(true);
				
				displayLog("No Parking Entry found - Available member found.", false);
				return;
				
			}
			
		}
		
		pePlateNumTextField.setEditable(false);
		
		peMonthlyNoneRadioButton.setSelected(true);
		
		disableFields();
		
		peCheckInButton.setVisible(true);
		cancelButton.setVisible(true);
		
		peBarcodeTextField.setEditable(true);
		
		peMonthlyAllRadioButton.setEnabled(true);
		peMonthlyDayRadioButton.setEnabled(true);
		peMonthlyNightRadioButton.setEnabled(true);
		peMonthlyNoneRadioButton.setEnabled(true);
		expDatePicker.setEditable(true);
		
		motorcycleYesRadioButton.setEnabled(true);
		motorcycleNoRadioButton.setEnabled(true);
		
		peEntryDatePicker.setDate(cEntry.getTime());
		peEntryHourTextField.setText(""+cEntry.get(Calendar.HOUR_OF_DAY));
		peEntryMinTextField.setText(""+cEntry.get(Calendar.MINUTE));
		
		peEntryDatePicker.setEditable(true);
		peEntryHourTextField.setEditable(true);
		peEntryMinTextField.setEditable(true);
		
		displayLog("No result found.", false);
		
	}
	
	private void peCheckInPressed() {
		
		String bc = getBarcodeString();
		
		if (bc == null) {
			displayLog("Invalid barcode.", false);
			return;
		}
		
		ParkingEntry pe = new ParkingEntry();
		pe.barcodeId = bc;
		
		String pnStr = pePlateNumTextField.getText();
		
		if (pnStr.length() <= 0) {
			displayLog("Please enter the plate number.",false);
			return;
		}
		
		//pe.plateNumberBytes = pnStr.getBytes();
		pe.plateNumString = pnStr;
		
//		if (!peMonthlyNoneRadioButton.isSelected() && motorcycleYesRadioButton.isSelected()) {
//			displayLog("A member cannot have Motorcycle type checked.",false);
//			return;
//		}
		
		if (peMonthlyAllRadioButton.isSelected()) {
			pe.memberType = MemberType.MONTHLY_ALL;
		} else if (peMonthlyDayRadioButton.isSelected()) {
			pe.memberType = MemberType.MONTHLY_DAY;
		} else if (peMonthlyNightRadioButton.isSelected()) {
			pe.memberType = MemberType.MONTHLY_NIGHT;
		} else {
			pe.memberType = MemberType.NONE;
		}
		
		if (motorcycleYesRadioButton.isSelected()) {
			pe.entryType = EntryType.MOTORCYCLE;
		} else {
			pe.entryType = EntryType.CAR;
		}
		
		Calendar cEntry = Calendar.getInstance();
		cEntry.setTime(peEntryDatePicker.getDate());
		
		int entryHour = getHourIntFromText(peEntryHourTextField.getText());
		if (entryHour < 0) {
			displayLog("Invalid entry hour.",false);
			return;
		}
		int entryMin = getMinIntFromText(peEntryMinTextField.getText());
		if (entryMin < 0) {
			displayLog("Invalid entry min.",false);
			return;
		}
		
		cEntry.set(Calendar.HOUR_OF_DAY, entryHour);
		cEntry.set(Calendar.MINUTE, entryMin);
		
		Calendar cNow = Calendar.getInstance();
		
		Calendar cExp = Calendar.getInstance();
		cExp.setTime(expDatePicker.getDate());
		pe.expirationDate = cExp.getTimeInMillis();
		
		if (cNow.compareTo(cEntry) < 0) {
			displayLog("Invalid entry time.",false);
			return;
		}
		
		pe.entryUTC = cEntry.getTimeInMillis();
		
		MemberCarEntry mce = mainServer.getMemberEntryFromBarcode(bc);
		
		if (!mainServer.isInBarcodeList(bc) && (mce==null)) {
			displayLog("This barcode is not in the system.",false);
			return;
		}
		
		if (mainServer.getParkingEntryFromBarcode(bc) != null) {
			displayLog("There's another vehicle with this barcode in the parking system.",false);
			return;
		}
		
//		if (mainServer.getParkingEntryFromPlateNumStr(pnStr)!=null) {
//			displayLog("There's another vehicle with this plate number in the parking system.",false);
//			return;
//		}
		
		if (mce != null) {
			
			if (!mce.getPlateNumString().equals(pnStr)) {
				
				if (JOptionPane.showConfirmDialog(null, "This barcode belongs to another member with plate number: "+mce.getPlateNumString()+"\nDo you want to proceed?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					
				} else {
					displayLog("Check-in cancelled.",false);
					return;
				}
				
			} else if (mce.entryType != pe.entryType) {
				
				if (JOptionPane.showConfirmDialog(null, "The entry type of this member is different.\nDo you want to proceed?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					
				} else {
					displayLog("Check-in cancelled.",false);
					return;
				}
				
			}
			
		}
		
		if (JOptionPane.showConfirmDialog(null, "Are you sure you want to check-in?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Check-in cancelled.",false);
			return;
		}
		
		ParkingEntry addedEntry = mainServer.addParkingEntry(pe);
		if (addedEntry != null) {
			displayLog("Parking Entry added. ID: "+addedEntry.uniqueId+" BC: "+addedEntry.barcodeId+" PN: "+addedEntry.getPlateNumString(),true);
		} else {
			displayLog("Adding parking entry failed.",false);
		}
		
		resetPage();
		
	}
	
	private void peSaveChangesPressed() {
		
		ParkingEntry pe = mainServer.getCurrentParkingEntry();
		
		ParkingEntry infoEntry = new ParkingEntry();
		infoEntry.copyInfo(pe);
		
		if (pe == null) {
			
			displayLog("ERROR: Removing parking entry failed", false);
			
			currentTransaction = null;
			mainServer.setCurrentParkingEntry(null);
			
			resetPage();
			return;
		}
		
		String bc = getBarcodeString();
		
		if (bc == null) {
			displayLog("Invalid barcode.", false);
			return;
		}
		
		infoEntry.barcodeId = bc;
		
		String pnStr = pePlateNumTextField.getText();
		
		if (pnStr.length() <= 0) {
			displayLog("Please enter the plate number.",false);
			return;
		}
		
		//infoEntry.plateNumberBytes = pnStr.getBytes();
		infoEntry.plateNumString = pnStr;
		
//		if (!peMonthlyNoneRadioButton.isSelected() && motorcycleYesRadioButton.isSelected()) {
//			displayLog("A member cannot have Motorcycle type checked.",false);
//			return;
//		}
		
		if (peMonthlyAllRadioButton.isSelected()) {
			infoEntry.memberType = MemberType.MONTHLY_ALL;
		} else if (peMonthlyDayRadioButton.isSelected()) {
			infoEntry.memberType = MemberType.MONTHLY_DAY;
		} else if (peMonthlyNightRadioButton.isSelected()) {
			infoEntry.memberType = MemberType.MONTHLY_NIGHT;
		} else {
			infoEntry.memberType = MemberType.NONE;
		}
		
		if (motorcycleYesRadioButton.isSelected()) {
			infoEntry.entryType = EntryType.MOTORCYCLE;
		} else {
			infoEntry.entryType = EntryType.CAR;
		}
		
		Calendar cEntry = Calendar.getInstance();
		cEntry.setTime(peEntryDatePicker.getDate());
		
		int entryHour = getHourIntFromText(peEntryHourTextField.getText());
		if (entryHour < 0) {
			displayLog("Invalid entry hour.",false);
			return;
		}
		int entryMin = getMinIntFromText(peEntryMinTextField.getText());
		if (entryMin < 0) {
			displayLog("Invalid entry min.",false);
			return;
		}
		
		cEntry.set(Calendar.HOUR_OF_DAY, entryHour);
		cEntry.set(Calendar.MINUTE, entryMin);
		
		Calendar cNow = Calendar.getInstance();
		
		Calendar cExp = Calendar.getInstance();
		cExp.setTime(expDatePicker.getDate());
		infoEntry.expirationDate = cExp.getTimeInMillis();
		
		if (cNow.compareTo(cEntry) < 0) {
			displayLog("Invalid entry time.",false);
			return;
		}
		
		infoEntry.entryUTC = cEntry.getTimeInMillis();
		
		MemberCarEntry mce = mainServer.getMemberEntryFromBarcode(bc);
		
		if (!mainServer.isInBarcodeList(bc) && (mce==null)) {
			displayLog("This barcode is not in the system.",false);
			return;
		}
		
		ParkingEntry peFromBarcode = mainServer.getParkingEntryFromBarcode(bc);
		
		if (peFromBarcode != null && peFromBarcode != pe) {
			displayLog("There's another vehicle with this barcode in the parking system.",false);
			return;
		}
		
//		ParkingEntry peFromPlateNum = mainServer.getParkingEntryFromPlateNumStr(pnStr);
//		
//		if (peFromPlateNum!=null && peFromPlateNum != pe) {
//			displayLog("There's another vehicle with this plate number in the parking system.",false);
//			return;
//		}
		
		if (JOptionPane.showConfirmDialog(null, "Are you sure you want to save these changes?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Changing info cancelled.",false);
			return;
		}
		
		if (mainServer.changeCurrentParkingEntryInfo(infoEntry)) {
			displayLog("Parking entry info updated",true);
		} else {
			displayLog("ERROR: changing parking entry info failed.",false);
		}
		
		//resetPage();
		
	}
	
	private void peDeletePressed() {
		
		ParkingEntry pe = mainServer.getCurrentParkingEntry();
		
		if (pe == null) {
			
			displayLog("ERROR: Removing parking entry failed", false);
			
			currentTransaction = null;
			mainServer.setCurrentParkingEntry(null);
			
			resetPage();
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to REMOVE "+pe.barcodeId+": "+pe.getPlateNumString()+" from the PARKING list?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Removing parking entry cancelled.",false);
			return;
		}
		
		if (mainServer.removeCurrentParkingEntry()) {
			displayLog("Parking entry: "+pe.barcodeId+": "+pe.getPlateNumString()+" has been successfully removed.", true);
		} else {
			displayLog("ERROR: Removing parking entry failed",false);
		}
		
		currentTransaction = null;
		mainServer.setCurrentParkingEntry(null);
		
		resetPage();
		
	}

	private void peCheckOutPressed() {
	
		ParkingEntry pe = mainServer.getCurrentParkingEntry();
		if (pe == null) {
			
			displayLog("ERROR: Check-out failed", false);
			
			currentTransaction = null;
			mainServer.setCurrentParkingEntry(null);
			
			resetPage();
			return;
		}
		
		ParkingEntry infoEntry = new ParkingEntry();
		infoEntry.copyInfo(pe);
		
		if (pe.memberType != MemberType.NONE && ckYesRadioButton.isSelected()) {
			displayLog("A member cannot have CK checked.",false);
			return;
		}
		
		if (ckYesRadioButton.isSelected()) {
			infoEntry.isCK = true;
		}
		
		Calendar cNow = Calendar.getInstance();
		
		Calendar cExit = Calendar.getInstance();
		cExit.setTime(peExitDatePicker.getDate());
		
		int exitHour = getHourIntFromText(peExitHourTextField.getText());
		if (exitHour<0) {
			displayLog("Invalid exit hour.",false);
			return;
		}
		int exitMin = getMinIntFromText(peExitMinTextField.getText());
		if (exitMin < 0) {
			displayLog("Invalid exit min.",false);
			return;
		}
		
		cExit.set(Calendar.HOUR_OF_DAY, exitHour);
		cExit.set(Calendar.MINUTE, exitMin);
		
		Calendar cEntry = Calendar.getInstance();
		cEntry.setTimeInMillis(infoEntry.entryUTC);
		
		if (cNow.compareTo(cExit) < 0) {
			displayLog("Invalid exit time.",false);
			return;
		}
		if (cExit.compareTo(cEntry) < 0) {
			displayLog("Invalid exit time.",false);
			return;
		}
		
		infoEntry.exitUTC = cExit.getTimeInMillis();
		pe.exitUTC = cExit.getTimeInMillis();
		
		MemberCarEntry mce = mainServer.getMemberEntryFromBarcode(pe.barcodeId);
		if (mce != null) {
			if (mce.getPlateNumString().equals(pe.getPlateNumString()) && mce.entryType==pe.entryType && mce.expirationDate!=pe.expirationDate) {
				
				if (JOptionPane.showConfirmDialog(null, "The expiration date of this member had been changed\nDo you want to use the new expiration date from member info?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					infoEntry.expirationDate = mce.expirationDate;
				}
				
			}
		}
		
		Calendar cExp = Calendar.getInstance();
		cExp.setTimeInMillis(infoEntry.expirationDate);
		
		int price = ParkingEntry.CalculatePrice(cEntry, cExit, infoEntry.entryType, infoEntry.memberType, infoEntry.isCK, cExp);
		
		if (JOptionPane.showConfirmDialog(null, "Are you sure you want to check-out?\n***Unsaved changes will not be applied to the transaction.\nPrice: "+price, "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
			infoEntry.uniqueId = pe.uniqueId;
			infoEntry.price = price;
			
			if (mainServer.addTransaction(infoEntry)) {
				mainServer.removeCurrentParkingEntry();
				displayLog("Check-out completed. ID: "+pe.uniqueId+" BC: "+pe.barcodeId+" PN: "+pe.getPlateNumString()+" PRICE: "+infoEntry.price, true);
				currentTransaction = null;
				mainServer.setCurrentParkingEntry(null);
				
				resetPage();
				return;
			} else {
				displayLog("ERROR: Check-out failed.", false);
			}
			
		} else {
			
		}
		
	}
	
	private void transactionSaveChangesPressed() {
		
		if (currentTransaction == null) {
			
			displayLog("ERROR: Saving transaction info failed", false);
			
			currentTransaction = null;
			mainServer.setCurrentParkingEntry(null);
			
			resetPage();
			return;
		}
		
		ParkingEntry newTransaction = new ParkingEntry();
		newTransaction.copyInfo(currentTransaction);
		newTransaction.uniqueId = currentTransaction.uniqueId;
		
		String bc = getBarcodeString();
		
		if (bc == null) {
			displayLog("Invalid barcode.", false);
			return;
		}
		
		newTransaction.barcodeId = bc;
		
		String pnStr = pePlateNumTextField.getText();
		
		if (pnStr.length() <= 0) {
			displayLog("Please enter the plate number.",false);
			return;
		}
		
		//newTransaction.plateNumberBytes = pnStr.getBytes();
		newTransaction.plateNumString = pnStr;
		
		if (!peMonthlyNoneRadioButton.isSelected() && ckYesRadioButton.isSelected()) {
			displayLog("A member cannot have CK checked.",false);
			return;
		}
		
//		if (!peMonthlyNoneRadioButton.isSelected() && motorcycleYesRadioButton.isSelected()) {
//			displayLog("A member cannot have Motorcycle type checked.",false);
//			return;
//		}
		
//		if (ckYesRadioButton.isSelected() && motorcycleYesRadioButton.isSelected()) {
//			displayLog("A motorcycle cannot have CK checked.",false);
//			return;
//		}
		
		if (peMonthlyAllRadioButton.isSelected()) {
			newTransaction.memberType = MemberType.MONTHLY_ALL;
		} else if (peMonthlyDayRadioButton.isSelected()) {
			newTransaction.memberType = MemberType.MONTHLY_DAY;
		} else if (peMonthlyNightRadioButton.isSelected()) {
			newTransaction.memberType = MemberType.MONTHLY_NIGHT;
		} else {
			newTransaction.memberType = MemberType.NONE;
		}
		
		if (motorcycleYesRadioButton.isSelected()) {
			newTransaction.entryType = EntryType.MOTORCYCLE;
		} else {
			newTransaction.entryType = EntryType.CAR;
		}
		
		if (ckYesRadioButton.isSelected()) {
			newTransaction.isCK = true;
		} else {
			newTransaction.isCK = false;
		}
		
		Calendar cEntry = Calendar.getInstance();
		cEntry.setTime(peEntryDatePicker.getDate());
		
		int entryHour = getHourIntFromText(peEntryHourTextField.getText());
		if (entryHour < 0) {
			displayLog("Invalid entry hour.",false);
			return;
		}
		int entryMin = getMinIntFromText(peEntryMinTextField.getText());
		if (entryMin < 0) {
			displayLog("Invalid entry min.",false);
			return;
		}
		
		cEntry.set(Calendar.HOUR_OF_DAY, entryHour);
		cEntry.set(Calendar.MINUTE, entryMin);
		
		Calendar cNow = Calendar.getInstance();
		
		Calendar cExp = Calendar.getInstance();
		cExp.setTime(expDatePicker.getDate());
		newTransaction.expirationDate = cExp.getTimeInMillis();
		
		if (cNow.compareTo(cEntry) < 0) {
			displayLog("Invalid entry time.",false);
			return;
		}
		
		newTransaction.entryUTC = cEntry.getTimeInMillis();
		
		Calendar cExit = Calendar.getInstance();
		cExit.setTime(peExitDatePicker.getDate());
		
		int exitHour = getHourIntFromText(peExitHourTextField.getText());
		if (exitHour<0) {
			displayLog("Invalid exit hour.",false);
			return;
		}
		int exitMin = getMinIntFromText(peExitMinTextField.getText());
		if (exitMin < 0) {
			displayLog("Invalid exit min.",false);
			return;
		}
		
		cExit.set(Calendar.HOUR_OF_DAY, exitHour);
		cExit.set(Calendar.MINUTE, exitMin);
		
		if (cNow.compareTo(cExit) < 0) {
			displayLog("Invalid exit time.",false);
			return;
		}
		if (cExit.compareTo(cEntry) < 0) {
			displayLog("Invalid exit time.",false);
			return;
		}
		
		newTransaction.exitUTC = cExit.getTimeInMillis();
		
		int price = ParkingEntry.CalculatePrice(cEntry, cExit, newTransaction.entryType, newTransaction.memberType, newTransaction.isCK, cExp);
		
		if (JOptionPane.showConfirmDialog(null, "Are you sure you want to change transaction info?\nPrice: "+price, "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {

			newTransaction.price = price;
			
			if (mainServer.changeTransactionInfo(currentTransaction, newTransaction)) {
				displayLog("Saving transaction info completed. ID: "+newTransaction.uniqueId+" BC: "+newTransaction.barcodeId+" PN: "+newTransaction.getPlateNumString()+" PRICE: "+newTransaction.price, true);
				currentTransaction = null;
				mainServer.setCurrentParkingEntry(null);
				
				resetPage();
				return;
			} else {
				displayLog("ERROR: Saving transaction info failed.", false);
			}
			
		} else {
			
		}
		
	}
	
	private void transactionDeletePressed() {
	
		if (currentTransaction == null) {
			
			displayLog("ERROR: Removing transaction failed", false);
			
			currentTransaction = null;
			mainServer.setCurrentParkingEntry(null);
			
			resetPage();
			return;
		}
		
		if (JOptionPane.showConfirmDialog(null, "Do you want to REMOVE transaction: "+currentTransaction.uniqueId+" from the TRANSACTION list?", "WARNING", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
			
		} else {
			displayLog("Removing transaction cancelled.",false);
			return;
		}
		
		if (mainServer.removeTransaction(currentTransaction.uniqueId)) {
			displayLog("Transaction: "+currentTransaction.uniqueId+" has been successfully removed.", true);
		} else {
			displayLog("ERROR: Removing transaction failed",false);
		}
		
		currentTransaction = null;
		mainServer.setCurrentParkingEntry(null);
		
		resetPage();
		
	}
	
	private void cancelPressed() {
		
		resetPage();
		
		displayLog("Cancelled", false);
		
	}
	
	public void resetPage() {
		
		resetFields();
		disableFields();
		
		transactionIdTextField.setEditable(true);
		peBarcodeTextField.setEditable(true);
		pePlateNumTextField.setEditable(true);
		
		transactionIdSearchButton.setVisible(true);
		peBarcodeSearchButton.setVisible(true);
		pePlateNumSearchButton.setVisible(true);
		
		transactionIdTextField.requestFocusInWindow();
		
	}
	
	public void resetFields() {
		
		currentTransaction = null;
		mainServer.setCurrentParkingEntry(null);
		
		transactionIdTextField.setText("");
		peBarcodeTextField.setText("");
		pePlateNumTextField.setText("");
		peMonthlyTypeButtonGroup.clearSelection();
		expDatePicker.setDate(new Date());
		peEntryDatePicker.setDate(new Date());
		peEntryHourTextField.setText("0");
		peEntryMinTextField.setText("0");
		peExitDatePicker.setDate(new Date());
		peExitHourTextField.setText("0");
		peExitMinTextField.setText("0");
		motorcycleButtonGroup.clearSelection();
		ckButtonGroup.clearSelection();
		transactionPriceLabel.setText("-");
		
	}
	
	public void disableFields() {
		
		transactionIdTextField.setEditable(false);
		peBarcodeTextField.setEditable(false);
		pePlateNumTextField.setEditable(false);
		peMonthlyDayRadioButton.setEnabled(false);
		peMonthlyNightRadioButton.setEnabled(false);
		peMonthlyAllRadioButton.setEnabled(false);
		peMonthlyNoneRadioButton.setEnabled(false);
		expDatePicker.setEditable(false);
		peEntryDatePicker.setEditable(false);
		peEntryHourTextField.setEditable(false);
		peEntryMinTextField.setEditable(false);
		peExitDatePicker.setEditable(false);
		peExitHourTextField.setEditable(false);
		peExitMinTextField.setEditable(false);
		motorcycleYesRadioButton.setEnabled(false);
		motorcycleNoRadioButton.setEnabled(false);
		ckYesRadioButton.setEnabled(false);
		ckNoRadioButton.setEnabled(false);
		
		transactionIdSearchButton.setVisible(false);
		peBarcodeSearchButton.setVisible(false);
		pePlateNumSearchButton.setVisible(false);
		peCheckInButton.setVisible(false);
		peSaveChangesButton.setVisible(false);
		peDeleteButton.setVisible(false);
		peCheckOutButton.setVisible(false);
		transactionSaveChangesButton.setVisible(false);
		transactionDeleteButton.setVisible(false);
		cancelButton.setVisible(false);
		
	}
	
	private void displayLog(String msg, boolean isGood) {
		logLabel.setText(msg);
		if (isGood) {
			logLabel.setForeground(new Color(0,255,0));
		} else {
			logLabel.setForeground(new Color(255,0,0));
		}
	}
	
	private String getBarcodeString() {
		
		String str = peBarcodeTextField.getText();
//		int barcodeInt = -1;
//		try {
//			barcodeInt = Integer.parseInt(str);
//		} catch (NumberFormatException nfe) {
//			barcodeInt = -1;
//		}
//		if (barcodeInt < 0) {
//			return null;
//		} else {
//			return ""+barcodeInt;
//		}
		return str;
		
	}
	
	private String getTransactionIdString() {
		
		String str = transactionIdTextField.getText();
		int idInt = -1;
		try {
			idInt = Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			idInt = -1;
		}
		if (idInt < 0) {
			return null;
		} else {
			return ""+idInt;
		}
		
	}
	
	public int getHourIntFromText(String txt) {
		
		int retVal;
		
		try {
			retVal = Integer.parseInt(txt);
		} catch(Exception e) {
			retVal = -1;
		}
		
		if (retVal >= 24) retVal = -1;
		
		return retVal;
		
	}
	
	public int getMinIntFromText(String txt) {
		
		int retVal;
		
		try {
			retVal = Integer.parseInt(txt);
		} catch(Exception e) {
			retVal = -1;
		}
		
		if (retVal >= 60) retVal = -1;
		
		return retVal;
		
	}
	
}
