import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Calendar;

enum GateClientState {

	STANDBY, WAITING_FOR_REQUEST_RESPONSE, WAITING_FOR_CHECK_IN_RESULT, WAITING_FOR_CHECK_OUT_RESULT, CHECKING_IN, CHECKING_OUT

}

public class GateClient {

	private ObjectInputStream sInput;
	private ObjectOutputStream sOutput;

	private Socket socket;

	private String serverIP;
	private int port;

	private String username;
	private int id;

	private GateGui gateGui;

	private boolean keepGoing = true;

	private int waitingId = 0;
	private boolean timeoutTimerRunning = true;
	private boolean isWaiting = false;

	private boolean isConnected = false;

	public GateClientState gateClientState = GateClientState.STANDBY;

	public int timeoutDuration = 30000;
	private int timeoutCounter = 0;

	public ParkingEntry currentParkingEntry;

	private int pingGapMillis = 60000;
	private PingPongThread pingPongThread = null;

	public GateClient(String serverIP, int port, String username) {
		this.serverIP = serverIP;
		this.port = port;
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public int getClientId() {
		return id;
	}

	public void setGateGui(GateGui aGui) {
		this.gateGui = aGui;
	}

	public void setServerIP(String serverIP) {
		this.serverIP = serverIP;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void shiftWaitingId() {
		waitingId = (waitingId + 1) % 10000;
	}

	public void stopTimeoutTimer() {
		timeoutTimerRunning = false;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public boolean start() {

		new TimeoutTimer().start();

		try {
			socket = new Socket(serverIP, port);
		} catch (Exception e) {
			displayLog(ConfigValues.LOG_ERROR, "Error connecting to server: "
					+ e);
			return false;
		}

		displayLog(ConfigValues.LOG_NORMAL,
				"Connection successful " + socket.getInetAddress() + " : "
						+ socket.getPort());

		try {
			sInput = new ObjectInputStream(socket.getInputStream());
			sOutput = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException eIO) {
			displayLog(ConfigValues.LOG_ERROR, "Error creating I/O Streams: "
					+ eIO);
			return false;
		}

		new ServerListener().start();

		// gameGUI = new GameGUI(this);
		// lobbyGUI = new ClientLobbyGUI(this);

		try {
			sOutput.writeObject(username);
		} catch (IOException eIO) {
			displayLog(ConfigValues.LOG_ERROR, "Error logging in: " + eIO);
			disconnect();
			gateGui = null;
			return false;
		}
		return true;
	}

	public void logout() {

		timeoutTimerRunning = false;
		keepGoing = false;

		if (sOutput != null) {
			sendMessage(new PR_ControlMessage(PR_ControlMessage.LOGOUT,
					serverIP, id, PR_ControlMessage.SERVER_ID));
		}

		disconnect();

	}

	public void disconnect() {

		timeoutTimerRunning = false;
		keepGoing = false;
		isConnected = false;

		if (pingPongThread != null) {
			pingPongThread.stopPingPong();
			pingPongThread.interrupt();
		}

		try {
			if (sInput != null) {
				sInput.close();
				sInput = null;
			}
		} catch (Exception e) {

		}

		try {
			if (sOutput != null) {
				sOutput.close();
				sOutput = null;
			}
		} catch (Exception e) {

		}

		try {
			if (socket != null) {
				socket.close();
				socket = null;
			}
		} catch (Exception e) {

		}

		if (gateGui != null) {
			gateGui.displayLog("NOT CONNECTED", false, false);
		}
	}

	private synchronized void setInfo_CheckIn(PR_ControlMessage cm) {

		gateGui.disableFields();

		String bc = cm.getMessage();
		MemberCarEntry mce = cm.getMemberCarEntry();

		Calendar cExp = Calendar.getInstance();
		Calendar cNow = Calendar.getInstance();

		gateClientState = GateClientState.CHECKING_IN;

		currentParkingEntry = new ParkingEntry();

		if (mce != null) {
			currentParkingEntry.barcodeId = mce.barcodeId;
			// currentParkingEntry.plateNumberBytes = mce.plateNumberBytes;
			currentParkingEntry.plateNumString = mce.plateNumString;
			currentParkingEntry.entryType = mce.entryType;
			currentParkingEntry.memberType = mce.memberType;
			currentParkingEntry.expirationDate = mce.expirationDate;
			currentParkingEntry.entryUTC = cm.getTimestamp();

			cExp.setTimeInMillis(mce.expirationDate);
			cExp.set(Calendar.HOUR_OF_DAY, 23);
			cExp.set(Calendar.MINUTE, 59);
			cExp.set(Calendar.SECOND, 59);
			cExp.set(Calendar.MILLISECOND, 0);

			if (mce.memberType == MemberType.NONE) {
				gateGui.alertRequestResultError("Membership: " + bc
						+ " has conflicting information.", true, false);
				gateClientState = GateClientState.STANDBY;
				currentParkingEntry = null;
				return;
			}

			if (cExp.compareTo(cNow) <= 0) {
				displayLog(ConfigValues.LOG_ERROR, "Membership: " + bc
						+ " has expired");
				gateGui.alertMemberExpired(bc, cExp, "Error");
				gateGui.reconfigure_requestState();
				gateClientState = GateClientState.STANDBY;
				currentParkingEntry = null;
				return;
			} else if (cExp.getTimeInMillis() - cNow.getTimeInMillis() <= 691200000l) {
				gateGui.alertMemberAlmostExpired(bc, cExp);
			}

			gateGui.showInfoAvailableMemberFound(mce, cExp);

		} else {

			currentParkingEntry.barcodeId = bc;
			currentParkingEntry.memberType = MemberType.NONE;
			currentParkingEntry.entryUTC = cm.getTimestamp();

			gateGui.showInfoAvailableBarcodeFound(bc);

		}

	}

	private synchronized void setInfo_CheckOut(PR_ControlMessage cm) {

		gateGui.disableFields();

		ParkingEntry pe = cm.getParkingEntry();
		MemberCarEntry mce = cm.getMemberCarEntry();

		Calendar cEntry = Calendar.getInstance();
		Calendar cExit = Calendar.getInstance();
		Calendar cExp = Calendar.getInstance();
		Calendar cNow = Calendar.getInstance();

		if (pe == null) {
			gateGui.alertRequestResultError(
					"Error: Error receiving information.", true, true);
			gateClientState = GateClientState.STANDBY;
			currentParkingEntry = null;
			return;
		}

		String bc = pe.barcodeId;

		gateClientState = GateClientState.CHECKING_OUT;

		currentParkingEntry = new ParkingEntry();
		currentParkingEntry.copyInfo(pe);
		currentParkingEntry.uniqueId = pe.uniqueId;

		cEntry.setTimeInMillis(pe.entryUTC);
		cExit.setTimeInMillis(pe.exitUTC);

		if (mce != null || pe.memberType == MemberType.MONTHLY_ALL
				|| pe.memberType == MemberType.MONTHLY_DAY
				|| pe.memberType == MemberType.MONTHLY_NIGHT) {
			cExp.setTimeInMillis(pe.expirationDate);
			cExp.set(Calendar.HOUR_OF_DAY, 23);
			cExp.set(Calendar.MINUTE, 59);
			cExp.set(Calendar.SECOND, 59);
			cExp.set(Calendar.MILLISECOND, 0);

			gateGui.showInfoParkingEntryFound(pe, cEntry, cExit, cExp, true);

			if (cExp.compareTo(cNow) <= 0) {
				gateGui.alertMemberExpired(bc, cExp, "Warning");
			} else if (cExp.getTimeInMillis() - cNow.getTimeInMillis() <= 691200000l) {
				gateGui.alertMemberAlmostExpired(bc, cExp);
			}

		} else {
			pe.memberType = MemberType.NONE;
			gateGui.showInfoParkingEntryFound(pe, cEntry, cExit, cExp, false);
		}

	}

	public synchronized void submitCheckInOrCheckOut(ParkingEntry infoEntry) {

		gateGui.disableFields();

		if (gateClientState == GateClientState.CHECKING_IN) {
			if ((currentParkingEntry.memberType != MemberType.MONTHLY_ALL)
					&& (currentParkingEntry.memberType != MemberType.MONTHLY_DAY)
					&& (currentParkingEntry.memberType != MemberType.MONTHLY_NIGHT)) {
				// currentParkingEntry.plateNumberBytes =
				// infoEntry.plateNumberBytes;
				currentParkingEntry.plateNumString = infoEntry.plateNumString;
				currentParkingEntry.entryType = infoEntry.entryType;
				currentParkingEntry.memberType = MemberType.NONE;
			}
			ParkingEntry entryToSend = new ParkingEntry();
			entryToSend.copyInfo(currentParkingEntry);
			PR_ControlMessage msg = new PR_ControlMessage(
					PR_ControlMessage.CHECK_IN, currentParkingEntry.barcodeId,
					id, PR_ControlMessage.SERVER_ID, entryToSend);
			if (sendMessageAndWait(msg,
					GateClientState.WAITING_FOR_CHECK_IN_RESULT)) {

			} else {
				gateGui.requestTimeout();
			}
		} else if (gateClientState == GateClientState.CHECKING_OUT) {
			ParkingEntry entryToCheckOut = new ParkingEntry();
			entryToCheckOut.copyInfo(currentParkingEntry);
			entryToCheckOut.uniqueId = currentParkingEntry.uniqueId;
			PR_ControlMessage msg = new PR_ControlMessage(
					PR_ControlMessage.CHECK_OUT, currentParkingEntry.barcodeId,
					id, PR_ControlMessage.SERVER_ID, entryToCheckOut);
			if (sendMessageAndWait(msg,
					GateClientState.WAITING_FOR_CHECK_OUT_RESULT)) {

			} else {
				gateGui.requestTimeout();
			}
		} else {
			gateGui.alertRequestResultError(
					"Error: Inconsistent client's state.", true, true);
			gateClientState = GateClientState.STANDBY;
			currentParkingEntry = null;
		}

	}

	public synchronized boolean sendMessage(PR_ControlMessage cm) {
		try {
			sOutput.writeObject(cm);
		} catch (IOException e) {
			displayLog(ConfigValues.LOG_ERROR, "Error sending message: " + e);
			return false;
		}
		return true;
	}

	public synchronized boolean sendMessageAndWait(PR_ControlMessage cm,
			GateClientState state) {

		timeoutCounter = timeoutDuration;

		shiftWaitingId();
		cm.setWaitingId(waitingId);

		gateClientState = state;
		isWaiting = true;

		return sendMessage(cm);
	}

	private synchronized boolean startPingPongRoutine() {
		if (pingPongThread == null) {
			pingPongThread = new PingPongThread();
			pingPongThread.start();
			return true;
		} else {
			return false;
		}
	}

	private void displayLog(int type, String message) {
		switch (type) {
		case ConfigValues.LOG_NORMAL:
			System.out.println(message);
			if (gateGui != null) {
				gateGui.displayLog(message, true);
			}
			break;
		case ConfigValues.LOG_ERROR:
			System.err.println(message);
			if (gateGui != null) {
				gateGui.displayLog(message, false);
			}
			break;
		}
	}

	class ServerListener extends Thread {

		public void run() {

			keepGoing = true;

			while (keepGoing) {

				try {

					PR_ControlMessage cm = (PR_ControlMessage) sInput
							.readObject();

					switch (cm.getType()) {

					case PR_ControlMessage.LOGIN_SUCCESSFUL_LOBBY:
						
						startPingPongRoutine();

						if (gateClientState == GateClientState.STANDBY) {
							gateGui.reconfigure_standbyState();
							currentParkingEntry = null;
						} else if (gateClientState == GateClientState.WAITING_FOR_REQUEST_RESPONSE) {
							gateGui.reconfigure_requestState();
							gateClientState = GateClientState.STANDBY;
							currentParkingEntry = null;
						} else if (gateClientState == GateClientState.WAITING_FOR_CHECK_IN_RESULT) {
							gateGui.reconfigure_checkInState();
							gateClientState = GateClientState.CHECKING_IN;
							// currentParkingEntry = null;
						} else if (gateClientState == GateClientState.WAITING_FOR_CHECK_OUT_RESULT) {
							gateGui.reconfigure_checkOutState();
							gateClientState = GateClientState.CHECKING_OUT;
							// currentParkingEntry = null;
						} else {
							gateGui.reconfigure_standbyState();
							gateClientState = GateClientState.STANDBY;
							currentParkingEntry = null;
						}

						try {
							id = Integer.parseInt(cm.getMessage());
						} catch (Exception e) {
							e.printStackTrace();
							id = -1;
						}
						isConnected = true;
						gateGui.displayLog("CONNECTED.", true);
						break;

					case PR_ControlMessage.ID_RESULT_FOUND:
						if (cm.getWaitingId() == waitingId) {
							isWaiting = false;
							if (cm.getParkingEntry() != null) {
								setInfo_CheckOut(cm);
							} else {
								setInfo_CheckIn(cm);
							}

						}
						break;

					case PR_ControlMessage.ID_RESULT_NOTFOUND:
						if (cm.getWaitingId() == waitingId) {
							isWaiting = false;
							gateClientState = GateClientState.STANDBY;
							gateGui.alertBarcodeIdNotFound(cm.getMessage());
							currentParkingEntry = null;
						}
						break;

					case PR_ControlMessage.CHECK_IN_CONFIRMED:
						isWaiting = false;
						gateClientState = GateClientState.STANDBY;
						currentParkingEntry = null;
						gateGui.printCheckInInfo(cm.getParkingEntry());
						gateGui.alertCheckInConfirmed(cm);
						break;

					case PR_ControlMessage.CHECK_OUT_CONFIRMED:
						isWaiting = false;
						gateClientState = GateClientState.STANDBY;
						currentParkingEntry = null;
						gateGui.alertCheckOutConfirmed(cm);
						break;

					case PR_ControlMessage.CHECK_IN_ERROR:
						isWaiting = false;
						gateClientState = GateClientState.STANDBY;
						currentParkingEntry = null;
						gateGui.alertRequestResultError(cm.getMessage(), true,
								true);
						break;

					case PR_ControlMessage.CHECK_OUT_ERROR:
						isWaiting = false;
						gateClientState = GateClientState.STANDBY;
						currentParkingEntry = null;
						gateGui.alertRequestResultError(cm.getMessage(), true,
								true);
						break;

					case PR_ControlMessage.LOGOUT:
						// gateGui.exit();
						// disconnect();
						logout();
						gateGui.reconfigure_notConnectedState();
						break;

					case PR_ControlMessage.PING_EMPTY:
						sendMessage(new PR_ControlMessage(
								PR_ControlMessage.PING_EMPTY_RESPONSE, "PONG",
								id, PR_ControlMessage.SERVER_ID));
						break;

					}

				} catch (IOException eIO) {

				} catch (ClassNotFoundException eCNF) {

				}

			}
		}

	}

	class TimeoutTimer extends Thread {

		public void run() {

			timeoutTimerRunning = true;

			while (timeoutTimerRunning) {

				try {
					Thread.sleep(1000);
					if (timeoutCounter > 0) {
						timeoutCounter -= 1000;
					}

					if (timeoutCounter <= 0 && isWaiting) {
						isWaiting = false;
						if (gateClientState == GateClientState.WAITING_FOR_REQUEST_RESPONSE) {
							shiftWaitingId();
							gateGui.requestTimeout();
						}
					}

				} catch (Exception e) {

				}

			}

		}

	}

	class PingPongThread extends Thread {

		boolean pingPongGoing = true;
		boolean pingPongInterrupted = false;

		public void run() {

			pingPongGoing = true;
			pingPongInterrupted = false;

			while (pingPongGoing) {

				try {
					Thread.sleep(pingGapMillis);

				} catch (InterruptedException e) {
					pingPongInterrupted = true;
				}

				if (!pingPongInterrupted) {
					sendMessage(new PR_ControlMessage(
							PR_ControlMessage.PING_EMPTY, "PING", id,
							PR_ControlMessage.SERVER_ID));
				}
			}

		}

		public void stopPingPong() {
			pingPongGoing = false;
		}

	}

}
