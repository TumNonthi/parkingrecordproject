import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.swing.*;


public class GateGui extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5526528634719451279L;

	private static GateGui mainGui;

	private final int LAYER_LEVEL_BG = 10;
	private final int LAYER_LEVEL_CONTENT = 20;
	
	private Color bgColor = new Color(0,0,0,160);
	
	//private JFrame parentFrame;
	
	private JLayeredPane layeredPane;
	
	private JLabel bgLabel;
	
	private MyPanel mainInputPanel;
	
	private JLabel barcodeLabel;
	private JTextField barcodeTextField;
	private JButton barcodeButton;
	
	private JLabel plateNumTitleLabel;
	private JTextField plateNumTextField;
//	private JButton requestButton;
	
	private JButton cancelButton;
	
	private JLabel typeLabel;
	private JRadioButton memberRadioButton;
	private JRadioButton carRadioButton;
	private JRadioButton motorcycleRadioButton;
	private JRadioButton memberDayRadioButton;
	private JRadioButton memberNightRadioButton;
	private JRadioButton memberAllRadioButton;
	private ButtonGroup carTypeButtonGroup;
	private ButtonGroup memberTypeButtonGroup;
	
	private JLabel expDateTitleLabel;
	private JLabel expDateLabel;
	
	private JLabel entryDateTitleLabel;
	private JLabel entryDateLabel;
	
	private JLabel exitDateTitleLabel;
	private JLabel exitDateLabel;
	
	private JLabel ckTitleLabel;
	private JRadioButton ckYesRadioButton;
	private JRadioButton ckNoRadioButton;
	private ButtonGroup ckButtonGroup;
	
	private JButton submitButton;
	
	
	private MyPanel addressInputPanel;
	
	private JLabel ipAddressTitleLabel;
	private JLabel portTitleLabel;
	
	private JTextField ipAddressTextField;
	private JTextField portTextField;
	
	private JButton connectButton;
	
	private JLabel logLabel;
	
	private MyPanel clockPanel;
	private JLabel clockDateLabel;
	private JLabel clockTimeLabel;
	
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMMMM yyyy");
	private SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
	
	
	private MyPanel pricePanel;
	private JLabel plateNumDisplayLabel;
	private JLabel priceLabel;
	
	
	private GateClient gateClient;
	public int connectTimeoutDuration = 5000;
	
	public GateGui() {
		setPreferredSize(new Dimension(1024,640));
		//setLayout(null);
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		layeredPane = new JLayeredPane();
        //layeredPane.setPreferredSize(new Dimension(1024, 768));
        
        bgLabel = new JLabel();
        Icon bgIcon = new ImageIcon("UI/bg/bg01.jpg");
        //bgLabel.setBounds(0,0,1366,768);
        bgLabel.setBounds(0,0,1024,640);
        bgLabel.setIcon(bgIcon);
        layeredPane.add(bgLabel, new Integer(LAYER_LEVEL_BG));
        
        mainInputPanel = new MyPanel();
        mainInputPanel.setBounds(50, 30, 400, 355);
        mainInputPanel.setBackground(bgColor);
        mainInputPanel.setOpaque(false);
        mainInputPanel.setLayout(null);
        layeredPane.add(mainInputPanel, new Integer(LAYER_LEVEL_CONTENT));
        
        barcodeLabel = new JLabel("Barcode");
        barcodeLabel.setBounds(20,20,80,25);
        mainInputPanel.add (barcodeLabel);
        
        barcodeTextField = new JTextField("");
        barcodeTextField.setBounds(110,20,150,25);
        mainInputPanel.add (barcodeTextField);
        
        barcodeButton = new JButton("Search");
        barcodeButton.setBounds(270,20,80,25);
        barcodeButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		searchButtonPressed();
        	}
        });
        mainInputPanel.add(barcodeButton);
        
        plateNumTitleLabel = new JLabel("ทบ. รถ");
        plateNumTitleLabel.setBounds(20,55,80,25);
        mainInputPanel.add (plateNumTitleLabel);
        
        plateNumTextField = new JTextField("");
        plateNumTextField.setBounds(110,55,120,25);
        mainInputPanel.add (plateNumTextField);
        
        /*
        requestButton = new JButton("Request");
        requestButton.setBounds(270,55,80,25);
        requestButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		requestButtonPressed();
        	}
        });
        mainInputPanel.add(requestButton);
        */
        
        cancelButton = new JButton("Cancel");
        cancelButton.setBounds(270,20,80,60);
        cancelButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		cancelButtonPressed();
        	}
        });
        mainInputPanel.add(cancelButton);
        
        typeLabel = new JLabel("ประเภท");
        typeLabel.setBounds(20, 90, 80, 25);
        mainInputPanel.add (typeLabel);
        
        memberRadioButton = new JRadioButton("Member", false);
        memberRadioButton.setBounds(110, 90, 70, 25);
        memberRadioButton.setEnabled(false);
        mainInputPanel.add (memberRadioButton);
        
        carRadioButton = new JRadioButton("รถ", false);
        carRadioButton.setBounds(190, 90, 70, 25);
        mainInputPanel.add (carRadioButton);
        
        motorcycleRadioButton = new JRadioButton("จยย.", false);
        motorcycleRadioButton.setBounds(270, 90, 100, 25);
        mainInputPanel.add (motorcycleRadioButton);
        
        carTypeButtonGroup = new ButtonGroup();
//        carTypeButtonGroup.add(memberRadioButton);
        carTypeButtonGroup.add(carRadioButton);
        carTypeButtonGroup.add(motorcycleRadioButton);
        
        memberAllRadioButton = new JRadioButton("All", false);
        memberAllRadioButton.setBounds(110, 125, 70, 25);
        memberAllRadioButton.setEnabled(false);
        mainInputPanel.add(memberAllRadioButton);
        
        memberDayRadioButton = new JRadioButton("Day", false);
        memberDayRadioButton.setBounds(190, 125, 70, 25);
        memberDayRadioButton.setEnabled(false);
        mainInputPanel.add(memberDayRadioButton);
        
        memberNightRadioButton = new JRadioButton("Night", false);
        memberNightRadioButton.setBounds(270, 125, 70, 25);
        memberNightRadioButton.setEnabled(false);
        mainInputPanel.add(memberNightRadioButton);
        
        memberTypeButtonGroup = new ButtonGroup();
        memberTypeButtonGroup.add(memberAllRadioButton);
        memberTypeButtonGroup.add(memberDayRadioButton);
        memberTypeButtonGroup.add(memberNightRadioButton);
        
        expDateTitleLabel = new JLabel("Exp. Date");
        expDateTitleLabel.setBounds(20, 160, 80, 25);
        mainInputPanel.add(expDateTitleLabel);
        
        //SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
        
        //expDateLabel = new JLabel(formatter.format(Calendar.getInstance().getTime()));
        
        expDateLabel = new JLabel();
        expDateLabel.setBounds(110, 160, 200, 25);
        mainInputPanel.add(expDateLabel);
        
        /*
        JXDatePicker datePicker = new JXDatePicker();
        datePicker.setBounds(110, 160, 200, 25);
        datePicker.setDate(Calendar.getInstance().getTime());
        mainInputPanel.add(datePicker);
        */
        
        
        entryDateTitleLabel = new JLabel("Entry Time");
        entryDateTitleLabel.setBounds(20, 230, 80, 25);
        mainInputPanel.add(entryDateTitleLabel);
        entryDateLabel = new JLabel();
        entryDateLabel.setBounds(110, 230, 300, 25);
        mainInputPanel.add(entryDateLabel);
        
        exitDateTitleLabel = new JLabel("Exit Time");
        exitDateTitleLabel.setBounds(20, 265, 80, 25);
        mainInputPanel.add(exitDateTitleLabel);
        exitDateLabel = new JLabel();
        exitDateLabel.setBounds(110, 265, 300, 25);
        mainInputPanel.add(exitDateLabel);
        
        
        ckTitleLabel = new JLabel("CK");
        ckTitleLabel.setBounds(20, 300, 80, 25);
        mainInputPanel.add (ckTitleLabel);
        
        ckYesRadioButton = new JRadioButton("Yes", false);
        ckYesRadioButton.setBounds(110, 300, 70, 25);
        mainInputPanel.add (ckYesRadioButton);
        
        ckNoRadioButton = new JRadioButton("No", false);
        ckNoRadioButton.setBounds(190, 300, 70, 25);
        mainInputPanel.add (ckNoRadioButton);
        
        ckButtonGroup = new ButtonGroup();
        ckButtonGroup.add(ckYesRadioButton);
        ckButtonGroup.add(ckNoRadioButton);
        
        submitButton = new JButton("Submit");
        submitButton.setBounds(270,300,70,25);
        submitButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		submitButtonPressed();
        	}
        });
        mainInputPanel.add(submitButton);
        
        CustomGuiUtilities.makeTransparent(mainInputPanel.getComponents());
        
        
        addressInputPanel = new MyPanel();
        addressInputPanel.setBounds(50, 415, 400, 195);
        addressInputPanel.setBackground(bgColor);
        addressInputPanel.setOpaque(false);
        addressInputPanel.setLayout(null);
        layeredPane.add(addressInputPanel, new Integer(LAYER_LEVEL_CONTENT));
        
        ipAddressTitleLabel = new JLabel("IP Address");
        ipAddressTitleLabel.setBounds(20,20,80,25);
        addressInputPanel.add (ipAddressTitleLabel);
        
        ipAddressTextField = new JTextField("");
        ipAddressTextField.setBounds(110,20,150,25);
        addressInputPanel.add (ipAddressTextField);
        
        portTitleLabel = new JLabel("Port");
        portTitleLabel.setBounds(20,55,80,25);
        addressInputPanel.add (portTitleLabel);
        
        portTextField = new JTextField("");
        portTextField.setBounds(110,55,150,25);
        addressInputPanel.add (portTextField);
        
        connectButton = new JButton("Connect");
        connectButton.setBounds(270,20,100,60);
        connectButton.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		connectButtonPressed();
        	}
        });
        addressInputPanel.add(connectButton);
        
        logLabel = new JLabel("ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRST");
        logLabel.setBounds(20,90,360,90);
        logLabel.setForeground(Color.RED);
        addressInputPanel.add(logLabel);
        
        CustomGuiUtilities.makeTransparent(addressInputPanel.getComponents());
        
        
        clockPanel = new MyPanel();
        clockPanel.setBounds(480, 415, 500, 195);
        clockPanel.setBackground(bgColor);
        clockPanel.setOpaque(false);
        clockPanel.setLayout(null);
        layeredPane.add(clockPanel, new Integer(LAYER_LEVEL_CONTENT));
        
        clockDateLabel = new JLabel();
        clockDateLabel.setBounds(20, 20, 460, 40);
        clockDateLabel.setFont(clockDateLabel.getFont().deriveFont(32f));
        clockPanel.add(clockDateLabel);
        
        clockTimeLabel = new JLabel();
        clockTimeLabel.setBounds(20, 80, 460, 110);
        clockTimeLabel.setFont(clockDateLabel.getFont().deriveFont(96f));
        clockPanel.add(clockTimeLabel);
        
        new Thread() {
        	
        	public void run() {
        		while (true) {
        			Calendar aCalendar = Calendar.getInstance();
        	        clockDateLabel.setText(dateFormatter.format(aCalendar.getTime()));
        	        clockTimeLabel.setText(timeFormatter.format(aCalendar.getTime()));
        			try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
        		}
        	}
        	
        }.start();
        
        CustomGuiUtilities.makeTransparent(clockPanel.getComponents());
        
        
        pricePanel = new MyPanel();
        pricePanel.setBounds(480, 30, 500, 355);
        pricePanel.setBackground(bgColor);
        pricePanel.setOpaque(false);
        pricePanel.setLayout(null);
        layeredPane.add(pricePanel, new Integer(LAYER_LEVEL_CONTENT));
        
        plateNumDisplayLabel = new JLabel("2กฐ8520");
        plateNumDisplayLabel.setBounds(20, 25, 460, 80);
        plateNumDisplayLabel.setFont(plateNumDisplayLabel.getFont().deriveFont(64f));
        plateNumDisplayLabel.setHorizontalAlignment(SwingConstants.CENTER);
        plateNumDisplayLabel.setForeground(new Color(0,255,255));
        pricePanel.add(plateNumDisplayLabel);
        
        priceLabel = new JLabel("9999");
        priceLabel.setBounds(20, 130, 460, 200);
        priceLabel.setFont(priceLabel.getFont().deriveFont(180f));
        priceLabel.setHorizontalAlignment(SwingConstants.CENTER);
        priceLabel.setForeground(new Color(0,255,0));
        pricePanel.add(priceLabel);
        
        CustomGuiUtilities.makeTransparent(pricePanel.getComponents());
        
        add(layeredPane);
        
        gateClient = new GateClient(ConfigValues.getDefaultIpAddress(), ConfigValues.defaultPort, "maingate");
        gateClient.setGateGui(this);
        
        reconfigure_notConnectedState();
        
	}
	
	
//	public void setParentFrame(JFrame aFrame) {
//		parentFrame = aFrame;
//	}
	
	
	public void displayLog(String msg, boolean isGood) {
		displayLog(msg, isGood, true);
	}
	
	public void displayLog(String msg, boolean isGood, boolean shouldPopupError) {
		logLabel.setText(msg);
		if (isGood) {
			logLabel.setForeground(new Color(0,255,0));
		} else {
			if (shouldPopupError) {
				JOptionPane.showMessageDialog(null, msg,"Warning",JOptionPane.ERROR_MESSAGE);
			}
			logLabel.setForeground(new Color(255,0,0));
		}
	}
	
	public void exit() {
		if (gateClient != null) {
			gateClient.stopTimeoutTimer();
			gateClient.logout();
		}
	}
	
	private static void createAndShowGUI() {
	    	
	    	CustomGuiUtilities.useCustomLookAndFeel();
	    	
	    	JFrame frame = new JFrame("Parking Records");
	        
	        mainGui = new GateGui();
//	        mainGui.setParentFrame(frame);
	        mainGui.setOpaque(true);
	        frame.setContentPane(mainGui);
	      
	        frame.setVisible(true);
	        frame.pack();
	        frame.setResizable(false);
	        
	        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	        WindowListener exitListener = new WindowAdapter() {
	        	
	        	@Override
	        	public void windowClosing(WindowEvent e) {
	        		mainGui.exit();
	        		System.exit(0);
	        	}
	        	
	        };
	        frame.addWindowListener(exitListener);
	        
	}
	
	public void disableFields() {
		barcodeTextField.setEditable(false);
		barcodeButton.setVisible(false);
		plateNumTextField.setEditable(false);
		cancelButton.setVisible(false);

		memberRadioButton.setEnabled(false);
		motorcycleRadioButton.setEnabled(false);
		carRadioButton.setEnabled(false);
		memberDayRadioButton.setEnabled(false);
		memberNightRadioButton.setEnabled(false);
		memberAllRadioButton.setEnabled(false);

		ckYesRadioButton.setEnabled(false);
		ckNoRadioButton.setEnabled(false);
		
		submitButton.setVisible(false);
		
		connectButton.setVisible(false);
	}
	
	public void resetFields() {
		barcodeTextField.setText("");
		plateNumTextField.setText("");
		
		memberRadioButton.setSelected(false);
		motorcycleRadioButton.setSelected(false);
		carRadioButton.setSelected(false);
		carTypeButtonGroup.clearSelection();
		memberDayRadioButton.setSelected(false);
		memberNightRadioButton.setSelected(false);
		memberAllRadioButton.setSelected(false);
		memberTypeButtonGroup.clearSelection();
		
		expDateLabel.setText("");
		
		entryDateLabel.setText("");
		
		exitDateLabel.setText("");
		
		ckYesRadioButton.setSelected(false);
		ckNoRadioButton.setSelected(false);
		ckButtonGroup.clearSelection();
		
		plateNumDisplayLabel.setText("");
		priceLabel.setText("");
	}
	
	public void reconfigure_notConnectedState() {
		
		barcodeTextField.setText("");
		barcodeTextField.setEditable(false);
		barcodeButton.setVisible(false);
		
		plateNumTextField.setText("");
		plateNumTextField.setEditable(false);
		//requestButton.setVisible(false);
		
		cancelButton.setVisible(false);
		
		memberRadioButton.setSelected(false);
		motorcycleRadioButton.setSelected(false);
		carRadioButton.setSelected(false);
		carTypeButtonGroup.clearSelection();
		memberDayRadioButton.setSelected(false);
		memberNightRadioButton.setSelected(false);
		memberAllRadioButton.setSelected(false);
		memberTypeButtonGroup.clearSelection();
		
		memberRadioButton.setEnabled(false);
		motorcycleRadioButton.setEnabled(false);
		carRadioButton.setEnabled(false);
		memberDayRadioButton.setEnabled(false);
		memberNightRadioButton.setEnabled(false);
		memberAllRadioButton.setEnabled(false);
		
		expDateLabel.setText("");
		
		entryDateLabel.setText("");
		
		exitDateLabel.setText("");

		ckYesRadioButton.setSelected(false);
		ckNoRadioButton.setSelected(false);
		ckButtonGroup.clearSelection();
		ckYesRadioButton.setEnabled(false);
		ckNoRadioButton.setEnabled(false);
		
		submitButton.setVisible(false);
		
		ipAddressTextField.setText(ConfigValues.getDefaultIpAddress());
		portTextField.setText(""+ConfigValues.defaultPort);
		
		//connectButton.setEnabled(true);
		connectButton.setVisible(true);
		
		displayLog("NOT CONNECTED. Please Login to server.", false);
		
		plateNumDisplayLabel.setText("");
		priceLabel.setText("");
		
	}

	public void reconfigure_standbyState() {
		
		barcodeTextField.setText("");
		barcodeTextField.setEditable(true);
		barcodeButton.setVisible(true);
		
		plateNumTextField.setText("");
		plateNumTextField.setEditable(true);
		//requestButton.setVisible(true);
		
		cancelButton.setVisible(false);
		
		memberRadioButton.setSelected(false);
		motorcycleRadioButton.setSelected(false);
		carRadioButton.setSelected(true);
		carTypeButtonGroup.clearSelection();
		memberDayRadioButton.setSelected(false);
		memberNightRadioButton.setSelected(false);
		memberAllRadioButton.setSelected(false);
		memberTypeButtonGroup.clearSelection();
		
		memberRadioButton.setEnabled(false);
		motorcycleRadioButton.setEnabled(true);
		carRadioButton.setEnabled(true);
		memberDayRadioButton.setEnabled(false);
		memberNightRadioButton.setEnabled(false);
		memberAllRadioButton.setEnabled(false);
		
		expDateLabel.setText("");
		
		entryDateLabel.setText("");
		
		exitDateLabel.setText("");


		ckButtonGroup.clearSelection();
		ckYesRadioButton.setSelected(false);
		ckNoRadioButton.setSelected(true);
		
		ckYesRadioButton.setEnabled(true);
		ckNoRadioButton.setEnabled(true);
		
		submitButton.setVisible(false);
		
		//ipAddressTextField.setText(ConfigValues.defaultIpAddress);
		//portTextField.setText(""+ConfigValues.defaultPort);
		
		//connectButton.setEnabled(false);
		connectButton.setVisible(false);
		
		//displayLog("CONNECTED.");
		
		plateNumDisplayLabel.setText("");
		priceLabel.setText("");
		
	}
	
	public void reconfigure_requestState() {
		
//		barcodeTextField.setText("");
		barcodeTextField.setEditable(true);
		barcodeButton.setVisible(true);
		
//		plateNumTextField.setText("");
		plateNumTextField.setEditable(true);
		
		cancelButton.setVisible(false);
		
//		memberRadioButton.setSelected(false);
//		motorcycleRadioButton.setSelected(false);
//		carRadioButton.setSelected(false);
//		carTypeButtonGroup.clearSelection();
		memberDayRadioButton.setSelected(false);
		memberNightRadioButton.setSelected(false);
		memberAllRadioButton.setSelected(false);
		memberTypeButtonGroup.clearSelection();
		
		memberRadioButton.setEnabled(false);
		motorcycleRadioButton.setEnabled(true);
		carRadioButton.setEnabled(true);
		memberDayRadioButton.setEnabled(false);
		memberNightRadioButton.setEnabled(false);
		memberAllRadioButton.setEnabled(false);
		
		expDateLabel.setText("");
		
		entryDateLabel.setText("");
		
		exitDateLabel.setText("");
		
		ckYesRadioButton.setEnabled(true);
		ckNoRadioButton.setEnabled(true);
		
		submitButton.setVisible(false);
		
		connectButton.setVisible(false);
		
		plateNumDisplayLabel.setText("");
		priceLabel.setText("");
		
	}
	
	public void reconfigure_checkInState() {
		
		barcodeTextField.setEditable(true);
		barcodeButton.setVisible(false);
		
		if (memberRadioButton.isSelected()) {
			plateNumTextField.setEditable(false);
		} else {
			plateNumTextField.setEditable(true);
		}
		
		cancelButton.setVisible(true);
		
		memberRadioButton.setEnabled(false);
		motorcycleRadioButton.setEnabled(true);
		carRadioButton.setEnabled(true);
		memberDayRadioButton.setEnabled(false);
		memberNightRadioButton.setEnabled(false);
		memberAllRadioButton.setEnabled(false);
		
		ckYesRadioButton.setEnabled(false);
		ckNoRadioButton.setEnabled(false);
		
		submitButton.setVisible(true);
		
		connectButton.setVisible(false);
		
	}
	
	public void reconfigure_checkOutState() {
		
		barcodeTextField.setEditable(false);
		barcodeButton.setVisible(false);
		
		plateNumTextField.setEditable(false);
		
		cancelButton.setVisible(true);
		
		memberRadioButton.setEnabled(false);
		motorcycleRadioButton.setEnabled(false);
		carRadioButton.setEnabled(false);
		memberDayRadioButton.setEnabled(false);
		memberNightRadioButton.setEnabled(false);
		memberAllRadioButton.setEnabled(false);
		
		ckYesRadioButton.setEnabled(false);
		ckNoRadioButton.setEnabled(false);
		
		submitButton.setVisible(true);
		
		connectButton.setVisible(false);
		
	}
	
	private void connectButtonPressed() {
		
		//connectButton.setEnabled(false);
		connectButton.setVisible(false);
		
		new Thread() {
        	
        	public void run() {
        		try {
					Thread.sleep(connectTimeoutDuration);
					if (!gateClient.isConnected()) {
						connectButton.setEnabled(true);
						connectButton.setVisible(true);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
        	}
        	
        }.start();
		
		int aPortNum;
		
		try {
			aPortNum = Integer.parseInt(portTextField.getText());
		} catch (Exception e) {
			aPortNum = ConfigValues.defaultPort;
		}
		
		gateClient.disconnect();
		gateClient.setServerIP(ipAddressTextField.getText());
		gateClient.setPort(aPortNum);
		if (gateClient.start()) {
			displayLog("CONNECTING.", true);
		} else {
			gateClient.disconnect();
		}
	}
	
	private void searchButtonPressed() {
		
		String bc = getBarcodeString();
		if (bc == null) {
			displayLog("Please enter a Barcode ID.", false);
			return;
		}
		
		gateClient.currentParkingEntry = null;
		
		disableFields();
		
		ParkingEntry infoEntry = new ParkingEntry();
		infoEntry.barcodeId = bc;
		if (ckYesRadioButton.isSelected()) {
			infoEntry.isCK = true;
		} else {
			infoEntry.isCK = false;
		}
		
		PR_ControlMessage cm = new PR_ControlMessage(PR_ControlMessage.ID_SEARCH, bc, gateClient.getClientId(), PR_ControlMessage.SERVER_ID, infoEntry);
		
		if (gateClient.sendMessageAndWait(cm, GateClientState.WAITING_FOR_REQUEST_RESPONSE)) {
			
		} else {
			requestTimeout();
		}
		
	}
	
	public void requestTimeout() {
		gateClient.logout();
		connectButton.setVisible(true);
		JOptionPane.showMessageDialog(null, "Error connecting to server!","Error",JOptionPane.ERROR_MESSAGE);
	}
	
	public void alertCheckInConfirmed(PR_ControlMessage cm) {
		
		ParkingEntry infoEntry = cm.getParkingEntry();
		
		SimpleDateFormat aTimeFormatter = new SimpleDateFormat("HH:mm");
		
		Calendar cEntry = Calendar.getInstance();
		cEntry.setTimeInMillis(infoEntry.entryUTC);
		
		String msg = "Ticket No: "+infoEntry.uniqueId+"\n"
				+ "Barcode: "+infoEntry.barcodeId+"\n"
				+ "Plate No: "+infoEntry.getPlateNumString()+"\n"
				+ "Entering Time: "+dateFormatter.format(cEntry.getTime())+" - "
				+ aTimeFormatter.format(cEntry.getTime());
		
		gateClient.currentParkingEntry = null;
		displayLog("Check-in successful.",true);
		reconfigure_standbyState();
		JOptionPane.showMessageDialog(null, msg,"Check-in successful",JOptionPane.INFORMATION_MESSAGE);
		
	}
	
	public void alertCheckOutConfirmed(PR_ControlMessage cm) {
		
		ParkingEntry infoEntry = cm.getParkingEntry();
		
		SimpleDateFormat aTimeFormatter = new SimpleDateFormat("HH:mm");
		
		Calendar cEntry = Calendar.getInstance();
		cEntry.setTimeInMillis(infoEntry.entryUTC);
		
		Calendar cExit = Calendar.getInstance();
		cExit.setTimeInMillis(infoEntry.exitUTC);
		
		String msg = "Ticket No: "+infoEntry.uniqueId+"\n"
				+ "Barcode: "+infoEntry.barcodeId+"\n"
				+ "Plate No: "+infoEntry.getPlateNumString()+"\n"
				+ "Entering Time: "+dateFormatter.format(cEntry.getTime())+" - "
				+ aTimeFormatter.format(cEntry.getTime())+"\n"
				+ "Exit Time: "+dateFormatter.format(cExit.getTime())+" - "
				+ aTimeFormatter.format(cExit.getTime())+"\n"
				+ "Price: "+infoEntry.price;
		
		gateClient.currentParkingEntry = null;
		displayLog("Check-out successful.",true);
		reconfigure_standbyState();
		JOptionPane.showMessageDialog(null, msg,"Check-out successful",JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void alertRequestResultError(String errMsg, boolean shouldResetBarcodeField, boolean shouldResetToStandby) {
		gateClient.currentParkingEntry = null;
		displayLog(errMsg,false);
		JOptionPane.showMessageDialog(null, errMsg,"Error",JOptionPane.ERROR_MESSAGE);
		reconfigure_requestState();
		if (shouldResetBarcodeField) {
			barcodeTextField.setText("");
		}
		if (shouldResetToStandby) {
			reconfigure_standbyState();
		}
	}
	
	public void alertBarcodeIdNotFound(String bc) {
		
		gateClient.currentParkingEntry = null;
		
		if (bc==null) {
			bc = "";
		}
		displayLog("Barcode: "+bc+" cannot be found in the system.",false);
		barcodeTextField.setText("");
		reconfigure_requestState();
		JOptionPane.showMessageDialog(null, "Barcode: "+bc+" cannot be found in the system.","Barcode Not Found",JOptionPane.ERROR_MESSAGE);
	}
	
	public void showInfoParkingEntryFound(ParkingEntry pe, Calendar cEntry, Calendar cExit, Calendar cExp, boolean isMember) {
		
		disableFields();
		
		String bc = pe.barcodeId;
		barcodeTextField.setText(bc);
		
		plateNumTextField.setText(pe.getPlateNumString());
		plateNumDisplayLabel.setText(pe.getPlateNumString());
		priceLabel.setText(""+pe.price);
		
		if (pe.entryType == EntryType.CAR) {
			carRadioButton.setSelected(true);
			//ckNoRadioButton.setSelected(true);
			memberTypeButtonGroup.clearSelection();
		} else if (pe.entryType == EntryType.MOTORCYCLE) {
			motorcycleRadioButton.setSelected(true);
			//ckNoRadioButton.setSelected(true);
			memberTypeButtonGroup.clearSelection();
		}
		
		if (pe.isCK) {
			ckYesRadioButton.setSelected(true);
		} else {
			ckNoRadioButton.setSelected(true);
		}
		
		if (pe.memberType == MemberType.MONTHLY_ALL) {
			memberRadioButton.setSelected(true);
			memberAllRadioButton.setSelected(true);
			ckNoRadioButton.setSelected(true);
		} else if (pe.memberType == MemberType.MONTHLY_DAY) {
			memberRadioButton.setSelected(true);
			memberDayRadioButton.setSelected(true);
			ckNoRadioButton.setSelected(true);
		} else if (pe.memberType == MemberType.MONTHLY_NIGHT) {
			memberRadioButton.setSelected(true);
			memberNightRadioButton.setSelected(true);
			ckNoRadioButton.setSelected(true);
		} else {
			memberRadioButton.setSelected(false);
		}
		
		entryDateLabel.setText(dateFormatter.format(cEntry.getTime())+" - "+timeFormatter.format(cEntry.getTime()));
		exitDateLabel.setText(dateFormatter.format(cExit.getTime())+" - "+timeFormatter.format(cExit.getTime()));
		
		if (isMember) {
			expDateLabel.setText(dateFormatter.format(cExp.getTime()));
			displayLog("Parking Member Found.",true);
		} else {
			expDateLabel.setText("");
			displayLog("Parking Vehicle Found.",true);
		}
		
		submitButton.setVisible(true);
		cancelButton.setVisible(true);
	}
	
	public void showInfoAvailableMemberFound(MemberCarEntry mce, Calendar cExp) {
		
		disableFields();
		
		String bc = mce.barcodeId;
		
		barcodeTextField.setText(bc);
		
		plateNumTextField.setText(mce.getPlateNumString());
		
		memberRadioButton.setSelected(false);
		
		if (mce.entryType == EntryType.CAR) {
			carRadioButton.setSelected(true);
		} else if (mce.entryType == EntryType.MOTORCYCLE) {
			motorcycleRadioButton.setSelected(true);
		}
		
		if (mce.memberType == MemberType.MONTHLY_ALL) {
			memberRadioButton.setSelected(true);
			memberAllRadioButton.setSelected(true);
			ckNoRadioButton.setSelected(true);
		} else if (mce.memberType == MemberType.MONTHLY_DAY) {
			memberRadioButton.setSelected(true);
			memberDayRadioButton.setSelected(true);ckNoRadioButton.setSelected(true);
			ckNoRadioButton.setSelected(true);
		} else if (mce.memberType == MemberType.MONTHLY_NIGHT) {
			memberRadioButton.setSelected(true);
			memberNightRadioButton.setSelected(true);
			ckNoRadioButton.setSelected(true);
		}
		
		expDateLabel.setText(dateFormatter.format(cExp.getTime()));
		
		plateNumDisplayLabel.setText("");
		priceLabel.setText("");
		submitButton.setVisible(true);
		cancelButton.setVisible(true);
		
		displayLog("Member Found.",true);
		
	}
	
	public void showInfoAvailableBarcodeFound(String bc) {
		
		disableFields();
		
		barcodeTextField.setText(bc);
		
		memberRadioButton.setSelected(false);
		
//		if (memberRadioButton.isSelected()) {
//			carRadioButton.setSelected(true);
//		}
		carTypeButtonGroup.clearSelection();
		memberTypeButtonGroup.clearSelection();
		
		expDateLabel.setText("");
		
		plateNumDisplayLabel.setText("");
		priceLabel.setText("");
		plateNumTextField.setEditable(true);
		carRadioButton.setEnabled(true);
		motorcycleRadioButton.setEnabled(true);
		submitButton.setVisible(true);
		cancelButton.setVisible(true);
		
		displayLog("Available Barcode Found.",true);
		
	}
	

	
	public void alertMemberExpired(String bc, Calendar cExp, String title) {
		JOptionPane.showMessageDialog(null, "Membership: "+bc+" has expired.",title,JOptionPane.ERROR_MESSAGE);
	}
	
	public void alertMemberAlmostExpired(String bc, Calendar cExp) {
		JOptionPane.showMessageDialog(null, "Membership: "+bc+" will expire soon.\nExpiration date: "+dateFormatter.format(cExp.getTime()),"Warning",JOptionPane.INFORMATION_MESSAGE);
	}
	
//	private void requestButtonPressed() {
//		
//	}
	
	private void cancelButtonPressed() {
		gateClient.currentParkingEntry = null;
		gateClient.shiftWaitingId();
		reconfigure_standbyState();
		displayLog("Cancelled.",false);
	}
	
	private void submitButtonPressed() {
		
		if (gateClient.gateClientState == GateClientState.CHECKING_IN && plateNumTextField.getText().length() <= 0) {
			displayLog("Please enter a plate number.",false);
			return;
		}
		
		if (!carRadioButton.isSelected() && !motorcycleRadioButton.isSelected()) {
			displayLog("Please select a vehicle type.",false);
			return;
		}
		
		if (gateClient.gateClientState == GateClientState.CHECKING_IN) {
			if (JOptionPane.showConfirmDialog(null, "Are you sure you want to CHECK-IN?", "Check-IN", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
				displayLog("Checking-in. Please wait...", true);
			} else {
				displayLog("Check-in cancelled.",false);
				return;
			}
		} else if (gateClient.gateClientState == GateClientState.CHECKING_OUT) {
			if (JOptionPane.showConfirmDialog(null, "Are you sure you want to CHECK-OUT?", "Check-OUT", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
				displayLog("Checking-out. Please wait...", true);
			} else {
				displayLog("Check-out cancelled.",false);
				return;
			}
		} else {
			if (JOptionPane.showConfirmDialog(null, "Are you sure you want to CHECK-IN/OUT?", "Check-IN/OUT", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
				displayLog("Checking-in/out. Please wait...", false);
			} else {
				displayLog("Check-in/out cancelled.",false);
				return;
			}
		}
		
		disableFields();
		
		ParkingEntry infoEntry = new ParkingEntry();
		
		//infoEntry.plateNumberBytes = plateNumTextField.getText().getBytes();
		infoEntry.plateNumString = plateNumTextField.getText();
		if (motorcycleRadioButton.isSelected()) {
			infoEntry.entryType = EntryType.MOTORCYCLE;
		} else {
			infoEntry.entryType = EntryType.CAR;
		}
		
		gateClient.submitCheckInOrCheckOut(infoEntry);
		
	}
	
	public void printCheckInInfo(ParkingEntry infoEntry) {
		
		boolean isError = false;
		
		if (infoEntry == null) {
			JOptionPane.showMessageDialog(null, "Null information", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		Calendar cEntry = Calendar.getInstance();
		cEntry.setTimeInMillis(infoEntry.entryUTC);
		
		SimpleDateFormat aDateFormatter = new SimpleDateFormat("dd MMM yyyy");
		SimpleDateFormat aTimeFormatter = new SimpleDateFormat("HH:mm");
		
		String msg = "** Chalermkrung Center **;"
				+ ";"
				+ "Ticket No: "+infoEntry.uniqueId+";"
				+ "Barcode: "+infoEntry.barcodeId+";"
				+ "Plate No: "+infoEntry.getPlateNumString()+";"
				+ "Entry Time: ;"
				+ aDateFormatter.format(cEntry.getTime())+" - "
				+ aTimeFormatter.format(cEntry.getTime())+";"
				+ ";"
				+ "*************************";
		//TODO
		
		try {
			isError = !EntryPrinter.printCard(msg);
		} catch (Exception e) {
			isError = true;
		}
		
		if (isError) {
			if (JOptionPane.showConfirmDialog(null, "Receipt printing failed.\nDo you want to try again?", "Error", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE) == JOptionPane.YES_OPTION) {
				printCheckInInfo(infoEntry);
			} else {
				
			}
		}
		
	}
	
	private String getBarcodeString() {
		
		String str = barcodeTextField.getText();
//		int barcodeInt = -1;
//		try {
//			barcodeInt = Integer.parseInt(str);
//		} catch (NumberFormatException nfe) {
//			barcodeInt = -1;
//		}
//		if (barcodeInt < 0) {
//			return null;
//		} else {
//			return ""+barcodeInt;
//		}
		return str;
		
	}
	
	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
	}
	
}
