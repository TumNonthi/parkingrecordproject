import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;


public class ReportBuilder {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}
	
	public static void buildMonthlyReport(String reportName, Calendar cMonth, ArrayList<DayTransactionRecord> dayTransactionRecordList) {
		
		String monthStr = "";
		int month = (cMonth.get(Calendar.MONTH) + 1);
		if (month < 10) {
			monthStr += ("0"+month+".xls");
		} else {
			monthStr += (month+".xls");
		}
		
		reportName += ("_" + cMonth.get(Calendar.YEAR) + "_" + monthStr);
		
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		
		Sheet mainSheet = wb.createSheet("monthly");
		
		Row dateRow = mainSheet.createRow((short)0);
		
		dateRow.createCell(0);
		dateRow.createCell(1);
		dateRow.createCell(2);
		dateRow.createCell(3);
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				0,
				0,
				2,
				3
				));
		
		Cell dateCell = dateRow.getCell(2);
		dateCell.setCellValue(Calendar.getInstance());
		
		CellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy HH:mm"));
		dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dateCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle monthCellStyle = wb.createCellStyle();
		monthCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("MMM yyyy"));
		monthCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		monthCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		monthCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		monthCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		monthCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_top = wb.createCellStyle();
		cellStyle_top.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_top.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_top.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_top.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_top.setBorderTop(CellStyle.BORDER_THIN);
		
		dateCell.setCellStyle(dateCellStyle);
		dateRow.getCell(3).setCellStyle(dateCellStyle);
		
		dateRow.getCell(0).setCellValue(cMonth);
		dateRow.getCell(0).setCellStyle(monthCellStyle);
		
		dateRow.getCell(1).setCellValue("created:");
		dateRow.getCell(1).setCellStyle(cellStyle_top);
		
		Row row1 = mainSheet.createRow((short)1);
		Row row2 = mainSheet.createRow((short)2);
		
		for (short i = 0; i<12; i++) {
			row1.createCell(i);
			row2.createCell(i);
		}
		
		CellStyle cellStyle_headerGroup = wb.createCellStyle();
		cellStyle_headerGroup.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_headerGroup.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_headerGroup.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_headerGroup.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_headerGroup.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_header = wb.createCellStyle();
		cellStyle_header.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_header.setBorderBottom(CellStyle.BORDER_DOUBLE);
		cellStyle_header.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_data = wb.createCellStyle();
		cellStyle_data.setAlignment(CellStyle.ALIGN_RIGHT);
		cellStyle_data.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderRight(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_dateOnly = wb.createCellStyle();
		cellStyle_dateOnly.setAlignment(CellStyle.ALIGN_RIGHT);
		cellStyle_dateOnly.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_dateOnly.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_dateOnly.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_dateOnly.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy"));
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				1,
				2,
				0,
				0
				));
		
		Cell cell_headerDate = row1.getCell(0);
		cell_headerDate.setCellValue("วันที่");
		cell_headerDate.setCellStyle(cellStyle_header);
		row2.getCell(0).setCellStyle(cellStyle_header);
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				1,
				1,
				1,
				3
				));
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				1,
				1,
				4,
				5
				));
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				1,
				1,
				6,
				9
				));
		
		for (short i = 1; i<=9 ;i++) {
			row1.getCell(i).setCellStyle(cellStyle_headerGroup);
		}
		
		row1.getCell(1).setCellValue("รถขาจร");
		row1.getCell(4).setCellValue("มอเตอร์ไซค์");
		row1.getCell(6).setCellValue("จำนวนรถประจำ");
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				1,
				2,
				10,
				10
				));
		
		row1.getCell(10).setCellValue("เงินรวม");
		row1.getCell(10).setCellStyle(cellStyle_header);
		row2.getCell(10).setCellStyle(cellStyle_header);
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				1,
				2,
				11,
				11
				));
		
		row1.getCell(11).setCellValue("Sum");
		row1.getCell(11).setCellStyle(cellStyle_header);
		row2.getCell(11).setCellStyle(cellStyle_header);
		
		for (short i = 1; i<=9 ;i++) {
			row2.getCell(i).setCellStyle(cellStyle_header);
		}
		
		row2.getCell(1).setCellValue("จำนวน");
		row2.getCell(2).setCellValue("CK");
		row2.getCell(3).setCellValue("ราคา");
		row2.getCell(4).setCellValue("จำนวน");
		row2.getCell(5).setCellValue("ราคา");
		row2.getCell(6).setCellValue("กลางวัน");
		row2.getCell(7).setCellValue("กลางคืน");
		row2.getCell(8).setCellValue("ทั้งวันทั้งคืน");
		row2.getCell(9).setCellValue("ราคา");
		
		for (int i = 0; i<dayTransactionRecordList.size(); i++) {
			
			DayTransactionRecord dtr = dayTransactionRecordList.get(i);
			if (dtr != null) {
				
				Row aRow = mainSheet.createRow(3+i);
				
				for (int j = 0; j<12 ; j++) {
					aRow.createCell(j);
				}
				
				aRow.getCell(0).setCellValue(dtr.cDate);
				aRow.getCell(0).setCellStyle(cellStyle_dateOnly);
				
				aRow.getCell(1).setCellValue((dtr.normalAmount+dtr.ckAmount));
				aRow.getCell(1).setCellStyle(cellStyle_data);
				
				aRow.getCell(2).setCellValue(dtr.ckAmount);
				aRow.getCell(2).setCellStyle(cellStyle_data);
				
				aRow.getCell(3).setCellValue(dtr.normalAndCkPriceValue);
				aRow.getCell(3).setCellStyle(cellStyle_data);
				
				aRow.getCell(4).setCellValue(dtr.motorcycleAmount);
				aRow.getCell(4).setCellStyle(cellStyle_data);
				
				aRow.getCell(5).setCellValue(dtr.motorcyclePriceValue);
				aRow.getCell(5).setCellStyle(cellStyle_data);
				
				aRow.getCell(6).setCellValue(dtr.memberDayAmount);
				aRow.getCell(6).setCellStyle(cellStyle_data);
				
				aRow.getCell(7).setCellValue(dtr.memberNightAmount);
				aRow.getCell(7).setCellStyle(cellStyle_data);
				
				aRow.getCell(8).setCellValue(dtr.memberAllAmount);
				aRow.getCell(8).setCellStyle(cellStyle_data);
				
				aRow.getCell(9).setCellValue(dtr.memberPriceValue);
				aRow.getCell(9).setCellStyle(cellStyle_data);
				
				String sumFormula = "SUM(D"+(4+i)+",F"+(4+i)+",J"+(4+i)+")";
				String accSumFormula = "SUM(K4:K"+(4+i)+")";
				
				aRow.getCell(10).setCellFormula(sumFormula);
				aRow.getCell(10).setCellStyle(cellStyle_data);
				
				aRow.getCell(11).setCellFormula(accSumFormula);
				aRow.getCell(11).setCellStyle(cellStyle_data);
				
			}
			
		}
		
		mainSheet.autoSizeColumn(0, true);
		mainSheet.autoSizeColumn(10, true);
		mainSheet.autoSizeColumn(11, true);
		
		try {
			File dir = new File(reportName).getParentFile();
			dir.mkdir();
			FileOutputStream fileOut = new FileOutputStream(reportName);
			wb.write(fileOut);
			fileOut.close();
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void buildBarcodeReport (String reportName, ArrayList<BarcodeReportEntry> bcList) {
		
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		
		Sheet mainSheet = wb.createSheet("barcodes");
		
		Row dateRow = mainSheet.createRow((short)0);
		
		dateRow.createCell(0);
		dateRow.createCell(1);
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				0,
				0,
				0,
				1
				));
		
		Cell dateCell = dateRow.getCell(0);
		dateCell.setCellValue(Calendar.getInstance());
		
		CellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy HH:mm"));
		dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dateCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		dateCell.setCellStyle(dateCellStyle);
		dateRow.getCell(1).setCellStyle(dateCellStyle);
		
		Row row1 = mainSheet.createRow((short)1);
		
		for (short i = 0; i<2; i++) {
			row1.createCell(i);
		}
		
		CellStyle cellStyle_header = wb.createCellStyle();
		cellStyle_header.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_header.setBorderBottom(CellStyle.BORDER_DOUBLE);
		cellStyle_header.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_data = wb.createCellStyle();
		cellStyle_data.setAlignment(CellStyle.ALIGN_RIGHT);
		cellStyle_data.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderRight(CellStyle.BORDER_THIN);
		
		row1.getCell(0).setCellValue("list of barcode");
		row1.getCell(0).setCellStyle(cellStyle_header);
		
		row1.getCell(1).setCellValue("status");
		row1.getCell(1).setCellStyle(cellStyle_header);
		
		for (int i = 0; i<bcList.size(); i++) {
			
			BarcodeReportEntry bcEntry = bcList.get(i);
			
			if (bcEntry != null) {
				Row aRow = mainSheet.createRow(2+i);
				
				for (short j = 0; j<2; j++) {
					aRow.createCell(j);
				}
				
				aRow.getCell(0).setCellValue(bcEntry.barcodeId);
				aRow.getCell(0).setCellStyle(cellStyle_data);
				
				if (bcEntry.isParking) {
					aRow.getCell(1).setCellValue("T");
				} else {
					aRow.getCell(1).setCellValue("A");
				}
				aRow.getCell(1).setCellStyle(cellStyle_data);
			}
			
		}
		
		mainSheet.autoSizeColumn(0, false);
		
		try {
			File dir = new File(reportName).getParentFile();
			dir.mkdir();
			FileOutputStream fileOut = new FileOutputStream(reportName);
			wb.write(fileOut);
			fileOut.close();
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void buildMemberReport(String reportName, ArrayList<MemberCarEntry> memberList) {
		
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		
		Sheet mainSheet = wb.createSheet("members");
		
		Row dateRow = mainSheet.createRow((short)0);
		
		dateRow.createCell(0);
		dateRow.createCell(1);
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				0,
				0,
				0,
				1
				));
		
		Cell dateCell = dateRow.getCell(0);
		dateCell.setCellValue(Calendar.getInstance());
		
		CellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy HH:mm"));
		dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dateCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		dateCell.setCellStyle(dateCellStyle);
		dateRow.getCell(1).setCellStyle(dateCellStyle);
		
		Row row1 = mainSheet.createRow((short)1);
		
		for (short i = 0; i<4; i++) {
			row1.createCell(i);
		}
		
		CellStyle cellStyle_header = wb.createCellStyle();
		cellStyle_header.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_header.setBorderBottom(CellStyle.BORDER_DOUBLE);
		cellStyle_header.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_data = wb.createCellStyle();
		cellStyle_data.setAlignment(CellStyle.ALIGN_RIGHT);
		cellStyle_data.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderRight(CellStyle.BORDER_THIN);
		
		CellStyle dateOnlyCellStyle = wb.createCellStyle();
		dateOnlyCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy"));
		dateOnlyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dateOnlyCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		dateOnlyCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		dateOnlyCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		
		row1.getCell(0).setCellValue("Barcode");
		row1.getCell(0).setCellStyle(cellStyle_header);
		
		row1.getCell(1).setCellValue("ทะเบียน");
		row1.getCell(1).setCellStyle(cellStyle_header);
		
		row1.getCell(2).setCellValue("ประเภท");
		row1.getCell(2).setCellStyle(cellStyle_header);
		
		row1.getCell(3).setCellValue("หมดอายุ");
		row1.getCell(3).setCellStyle(cellStyle_header);
		
		for (int i = 0; i<memberList.size(); i++) {
			
			MemberCarEntry mce = memberList.get(i);
			
			if (mce != null) {
				Row aRow = mainSheet.createRow(2+i);
				
				for (short j = 0; j<4; j++) {
					aRow.createCell(j);
				}
				
				aRow.getCell(0).setCellValue(mce.barcodeId);
				aRow.getCell(0).setCellStyle(cellStyle_data);
				
				aRow.getCell(1).setCellValue(mce.getPlateNumString());
				aRow.getCell(1).setCellStyle(cellStyle_data);
				
				if (mce.memberType == MemberType.MONTHLY_DAY) {
					aRow.getCell(2).setCellValue("Day");
				} else if (mce.memberType == MemberType.MONTHLY_NIGHT) {
					aRow.getCell(2).setCellValue("Night");
				} else if (mce.memberType == MemberType.MONTHLY_ALL) {
					aRow.getCell(2).setCellValue("All");
				} else {
					aRow.getCell(2).setCellValue("ERROR");
				}
				aRow.getCell(2).setCellStyle(cellStyle_data);
				
				Calendar cExp = Calendar.getInstance();
				cExp.setTimeInMillis(mce.expirationDate);
				aRow.getCell(3).setCellValue(cExp);
				aRow.getCell(3).setCellStyle(dateOnlyCellStyle);
			}
			
		}
		
		mainSheet.autoSizeColumn(0, false);
		mainSheet.autoSizeColumn(3, false);
		
		try {
			File dir = new File(reportName).getParentFile();
			dir.mkdir();
			FileOutputStream fileOut = new FileOutputStream(reportName);
			wb.write(fileOut);
			fileOut.close();
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void buildParkingReport(String reportName, ArrayList<ParkingEntry> parkingList) {
		
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		
		Sheet mainSheet = wb.createSheet("รถในระบบ");
		
		Row dateRow = mainSheet.createRow((short)0);
		
		dateRow.createCell(0);
		dateRow.createCell(1);
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				0,
				0,
				0,
				1
				));
		
		Cell dateCell = dateRow.getCell(0);
		dateCell.setCellValue(Calendar.getInstance());
		
		CellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy HH:mm"));
		dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dateCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		dateCell.setCellStyle(dateCellStyle);
		dateRow.getCell(1).setCellStyle(dateCellStyle);
		
		Row row1 = mainSheet.createRow((short)1);
		
		for (short i = 0; i<5; i++) {
			row1.createCell(i);
		}
		
		CellStyle cellStyle_header = wb.createCellStyle();
		cellStyle_header.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_header.setBorderBottom(CellStyle.BORDER_DOUBLE);
		cellStyle_header.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_data = wb.createCellStyle();
		cellStyle_data.setAlignment(CellStyle.ALIGN_RIGHT);
		cellStyle_data.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderRight(CellStyle.BORDER_THIN);
		
		row1.getCell(0).setCellValue("Transaction");
		row1.getCell(0).setCellStyle(cellStyle_header);
		
		row1.getCell(1).setCellValue("Barcode");
		row1.getCell(1).setCellStyle(cellStyle_header);
		
		row1.getCell(2).setCellValue("ทะเบียน");
		row1.getCell(2).setCellStyle(cellStyle_header);
		
		row1.getCell(3).setCellValue("ประเภทุ");
		row1.getCell(3).setCellStyle(cellStyle_header);
		
		row1.getCell(4).setCellValue("เวลาเข้า");
		row1.getCell(4).setCellStyle(cellStyle_header);
		
		for (int i = 0; i<parkingList.size(); i++) {
			
			ParkingEntry pe = parkingList.get(i);
			
			if (pe != null) {
				Row aRow = mainSheet.createRow(2+i);
				
				for (short j = 0; j<5; j++) {
					aRow.createCell(j);
				}
				
				aRow.getCell(0).setCellValue(pe.uniqueId);
				aRow.getCell(0).setCellStyle(cellStyle_data);
				
				aRow.getCell(1).setCellValue(pe.barcodeId);
				aRow.getCell(1).setCellStyle(cellStyle_data);
				
				aRow.getCell(2).setCellValue(pe.getPlateNumString());
				aRow.getCell(2).setCellStyle(cellStyle_data);
				
				if (pe.memberType == MemberType.MONTHLY_DAY) {
					aRow.getCell(3).setCellValue("Day");
				} else if (pe.memberType == MemberType.MONTHLY_NIGHT) {
					aRow.getCell(3).setCellValue("Night");
				} else if (pe.memberType == MemberType.MONTHLY_ALL) {
					aRow.getCell(3).setCellValue("All");
				} else if (pe.isCK) {
					aRow.getCell(3).setCellValue("จ");
				} else if (pe.entryType == EntryType.CAR) {
					aRow.getCell(3).setCellValue("จ");
				} else if (pe.entryType == EntryType.MOTORCYCLE) {
					aRow.getCell(3).setCellValue("ม");
				}
				aRow.getCell(3).setCellStyle(cellStyle_data);
				
				Calendar cEntry = Calendar.getInstance();
				cEntry.setTimeInMillis(pe.entryUTC);
				aRow.getCell(4).setCellValue(cEntry);
				aRow.getCell(4).setCellStyle(dateCellStyle);
			}
			
		}
		
		mainSheet.autoSizeColumn(0, false);
		mainSheet.autoSizeColumn(1, false);
		mainSheet.autoSizeColumn(4, false);
		
		try {
			File dir = new File(reportName).getParentFile();
			dir.mkdir();
			FileOutputStream fileOut = new FileOutputStream(reportName);
			wb.write(fileOut);
			fileOut.close();
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void buildDailyReport(String reportName, Calendar cDate, ArrayList<ParkingEntry> transactionList) {
		
		Collections.sort(transactionList);
		
		String monthStr = "";
		int month = (cDate.get(Calendar.MONTH) + 1);
		if (month < 10) {
			monthStr += ("0"+month);
		} else {
			monthStr += month;
		}
		
		reportName += ("_" + cDate.get(Calendar.YEAR) + "_" + monthStr + "_");
		int date = cDate.get(Calendar.DATE);
		if (date < 10) {
			reportName += ("0" + date + ".xls");
		} else {
			reportName += (date + ".xls");
		}
		
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		
		Sheet mainSheet = wb.createSheet("Daily");
		
		Row dateRow = mainSheet.createRow((short)0);
		
		dateRow.createCell(0);
		dateRow.createCell(1);
		dateRow.createCell(2);
		dateRow.createCell(3);
		
		mainSheet.addMergedRegion(new CellRangeAddress(
				0,
				0,
				2,
				3
				));
		
		Cell dateCell = dateRow.getCell(2);
		dateCell.setCellValue(Calendar.getInstance());
		
		CellStyle dateCellStyle = wb.createCellStyle();
		dateCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy HH:mm"));
		dateCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dateCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		dateCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle dateOnlyCellStyle = wb.createCellStyle();
		dateOnlyCellStyle.setDataFormat(
				createHelper.createDataFormat().getFormat("dd/MMM/yyyy"));
		dateOnlyCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		dateOnlyCellStyle.setBorderBottom(CellStyle.BORDER_THIN);
		dateOnlyCellStyle.setBorderLeft(CellStyle.BORDER_THIN);
		dateOnlyCellStyle.setBorderRight(CellStyle.BORDER_THIN);
		dateOnlyCellStyle.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_top = wb.createCellStyle();
		cellStyle_top.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_top.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_top.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_top.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_top.setBorderTop(CellStyle.BORDER_THIN);
		
		dateCell.setCellStyle(dateCellStyle);
		dateRow.getCell(3).setCellStyle(dateCellStyle);
		
		dateRow.getCell(0).setCellValue(cDate);
		dateRow.getCell(0).setCellStyle(dateOnlyCellStyle);
		
		dateRow.getCell(1).setCellValue("created:");
		dateRow.getCell(1).setCellStyle(cellStyle_top);
		
		Row row1 = mainSheet.createRow((short)1);
		
		for (short i = 0; i<9; i++) {
			row1.createCell(i);
		}
		
		CellStyle cellStyle_header = wb.createCellStyle();
		cellStyle_header.setAlignment(CellStyle.ALIGN_CENTER);
		cellStyle_header.setBorderBottom(CellStyle.BORDER_DOUBLE);
		cellStyle_header.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderRight(CellStyle.BORDER_THIN);
		cellStyle_header.setBorderTop(CellStyle.BORDER_THIN);
		
		CellStyle cellStyle_data = wb.createCellStyle();
		cellStyle_data.setAlignment(CellStyle.ALIGN_RIGHT);
		cellStyle_data.setBorderBottom(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderLeft(CellStyle.BORDER_THIN);
		cellStyle_data.setBorderRight(CellStyle.BORDER_THIN);
		
		row1.getCell(0).setCellValue("Transaction");
		row1.getCell(0).setCellStyle(cellStyle_header);
		
		row1.getCell(1).setCellValue("Barcode");
		row1.getCell(1).setCellStyle(cellStyle_header);
		
		row1.getCell(2).setCellValue("ทะเบียน");
		row1.getCell(2).setCellStyle(cellStyle_header);
		
		row1.getCell(3).setCellValue("ประเภทุ");
		row1.getCell(3).setCellStyle(cellStyle_header);
		
		row1.getCell(4).setCellValue("เวลาเข้า");
		row1.getCell(4).setCellStyle(cellStyle_header);
		
		row1.getCell(5).setCellValue("เวลาออก");
		row1.getCell(5).setCellStyle(cellStyle_header);
		
		row1.getCell(6).setCellValue("ราคา");
		row1.getCell(6).setCellStyle(cellStyle_header);
		
		row1.getCell(7).setCellValue("CK");
		row1.getCell(7).setCellStyle(cellStyle_header);
		
		row1.getCell(8).setCellValue("SUM");
		row1.getCell(8).setCellStyle(cellStyle_header);
		
		for (int i = 0; i<transactionList.size(); i++) {
			
			ParkingEntry pe = transactionList.get(i);
			
			if (pe != null) {
				Row aRow = mainSheet.createRow(2+i);
				
				for (short j = 0; j<9; j++) {
					aRow.createCell(j);
				}
				
				aRow.getCell(0).setCellValue(pe.uniqueId);
				aRow.getCell(0).setCellStyle(cellStyle_data);
				
				aRow.getCell(1).setCellValue(pe.barcodeId);
				aRow.getCell(1).setCellStyle(cellStyle_data);
				
				aRow.getCell(2).setCellValue(pe.getPlateNumString());
				aRow.getCell(2).setCellStyle(cellStyle_data);
				
				if (pe.memberType == MemberType.MONTHLY_DAY) {
					aRow.getCell(3).setCellValue("Day");
				} else if (pe.memberType == MemberType.MONTHLY_NIGHT) {
					aRow.getCell(3).setCellValue("Night");
				} else if (pe.memberType == MemberType.MONTHLY_ALL) {
					aRow.getCell(3).setCellValue("All");
				} else if (pe.isCK) {
					aRow.getCell(3).setCellValue("จ");
				} else if (pe.entryType == EntryType.CAR) {
					aRow.getCell(3).setCellValue("จ");
				} else if (pe.entryType == EntryType.MOTORCYCLE) {
					aRow.getCell(3).setCellValue("ม");
				}
				aRow.getCell(3).setCellStyle(cellStyle_data);
				
				Calendar cEntry = Calendar.getInstance();
				cEntry.setTimeInMillis(pe.entryUTC);
				aRow.getCell(4).setCellValue(cEntry);
				aRow.getCell(4).setCellStyle(dateCellStyle);
				
				Calendar cExit = Calendar.getInstance();
				cExit.setTimeInMillis(pe.exitUTC);
				aRow.getCell(5).setCellValue(cExit);
				aRow.getCell(5).setCellStyle(dateCellStyle);
				
				aRow.getCell(6).setCellValue(pe.price);
				aRow.getCell(6).setCellStyle(cellStyle_data);
				
				if (pe.isCK) {
					aRow.getCell(7).setCellValue("CK");
				} else {
					aRow.getCell(7).setCellValue("");
				}
				aRow.getCell(7).setCellStyle(cellStyle_data);
				
				String formulaString = "SUM(G3:G" + (3+i) + ")";
				
				aRow.getCell(8).setCellFormula(formulaString);
				aRow.getCell(8).setCellStyle(cellStyle_data);
				
			}
			
		}
		
		mainSheet.autoSizeColumn(0, false);
		mainSheet.autoSizeColumn(1, false);
		mainSheet.autoSizeColumn(4, false);
		mainSheet.autoSizeColumn(5, false);
		mainSheet.autoSizeColumn(8, false);
		
		try {
			File dir = new File(reportName).getParentFile();
			dir.mkdir();
			FileOutputStream fileOut = new FileOutputStream(reportName);
			wb.write(fileOut);
			fileOut.close();
			wb.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}

class BarcodeReportEntry {
	
	String barcodeId;
	boolean isParking;
	
	public BarcodeReportEntry () {
		barcodeId = "";
		isParking = false;
	}
	
	public BarcodeReportEntry (String _barcodeId, boolean _isParking) {
		barcodeId = _barcodeId;
		isParking = _isParking;
	}
	
}
