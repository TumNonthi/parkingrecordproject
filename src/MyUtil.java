
public class MyUtil {

	public static int getHourIntFromText(String txt) {
		
		return getLimitedIntFromText(txt,24);
		
	}
	
	public static int getMinIntFromText(String txt) {
		
		return getLimitedIntFromText(txt,60);
		
	}
	
	public static int getDateIntFromText(String txt) {
		
		int retVal;
		
		try {
			retVal = Integer.parseInt(txt);
		} catch(Exception e) {
			retVal = -1;
		}
		
		if (retVal >= 32 || retVal <= 0) retVal = -1;
		
		return retVal;
		
	}
	
	public static int getMonthIntFromText(String txt) {
		
		int retVal;
		
		try {
			retVal = Integer.parseInt(txt);
		} catch(Exception e) {
			retVal = -1;
		}
		
		if (retVal >= 13 || retVal <= 0) retVal = -1;
		
		return retVal;
		
	}
	
	// cap is exclusive
	// 0(inclusive) - cap(exclusive)
	public static int getLimitedIntFromText(String txt, int cap) {
		
		int retVal;
		
		try {
			retVal = Integer.parseInt(txt);
		} catch(Exception e) {
			retVal = -1;
		}
		
		if (retVal >= cap) retVal = -1;
		
		return retVal;
	}
	
}
