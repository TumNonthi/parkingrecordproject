import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;


public class ConfigValues {

	private static final String filename_default_ip = "appdata/ip_default.txt";
	
	private static String defaultIpAddress = "";
	public static int defaultPort = 10001;
	
	public static final int LOG_NORMAL = 0, LOG_ERROR = -1;
	
	public static String getDefaultIpAddress() {
		if (defaultIpAddress.length() > 0) {
			return defaultIpAddress;
		} else {
			defaultIpAddress = loadDefaultIp();
			return defaultIpAddress;
		}
	}
	
	private static String loadDefaultIp() {
		
		String retVal = "";
		
		try {
			
			BufferedReader br = new BufferedReader(new FileReader(filename_default_ip));
			String str = br.readLine();
			if (str.length() > 0) {
				retVal = str;
			} else {
				retVal = "127.0.0.1";
			}
			br.close();
		} catch (Exception e) {
			//e.printStackTrace();
			retVal = "127.0.0.1";
			saveDefaultIp(retVal);
		}
		
		return retVal;
	}
	
	private static void saveDefaultIp(String ip) {
		
		try {
			
			File file = new File(filename_default_ip);
			File dir = file.getParentFile();
			dir.mkdir();
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(ip);
			bw.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
