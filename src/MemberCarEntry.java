import java.io.Serializable;

enum MemberType {
	NONE,
	MONTHLY_DAY,
	MONTHLY_NIGHT,
	MONTHLY_ALL
}

public class MemberCarEntry implements Serializable {

	private static final long serialVersionUID = 2L;
	
	public String barcodeId;
	//public byte[] plateNumberBytes;
	public String plateNumString;
	public EntryType entryType;
	public MemberType memberType;
	public long issueDate;
	public long expirationDate;
	public String firstname;
	public String surname;
	public String tel;
	public String address;
	public String brand;
	
	public String getPlateNumString() {
		
//		String retVal;
//		
//		try {
//			retVal = new String(plateNumberBytes);
//		} catch(Exception e) {
//			retVal = new String(plateNumberBytes);
//		}
//		return retVal;
		
		return plateNumString;
		
	}
	
	public void copyInfo(MemberCarEntry infoEntry) {
		this.barcodeId = infoEntry.barcodeId;
		//this.plateNumberBytes = infoEntry.plateNumberBytes;
		this.plateNumString = infoEntry.plateNumString;
		this.entryType = infoEntry.entryType;
		this.memberType = infoEntry.memberType;
		this.issueDate = infoEntry.issueDate;
		this.expirationDate = infoEntry.expirationDate;
		this.firstname = infoEntry.firstname;
		this.surname = infoEntry.surname;
		this.tel = infoEntry.tel;
		this.address = infoEntry.address;
		this.brand = infoEntry.brand;
	}
	
}
