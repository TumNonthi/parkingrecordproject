import java.util.Calendar;


public class DayTransactionRecord {

	Calendar cDate;
	
	int normalAmount;
	int ckAmount;
	int normalAndCkPriceValue;
	
	int motorcycleAmount;
	int motorcyclePriceValue;
	
	int memberDayAmount;
	int memberNightAmount;
	int memberAllAmount;
	int memberPriceValue;
	
	public DayTransactionRecord() {
		cDate = Calendar.getInstance();
		
		normalAmount = 0;
		ckAmount = 0;
		normalAndCkPriceValue = 0;
		
		motorcycleAmount = 0;
		motorcyclePriceValue = 0;
		
		memberDayAmount = 0;
		memberNightAmount = 0;
		memberAllAmount = 0;
		memberPriceValue = 0;
	}
	
	public DayTransactionRecord(int day, int month, int year) {
		this();
		this.cDate.set(Calendar.YEAR, year);
		this.cDate.set(Calendar.MONTH, month);
		this.cDate.set(Calendar.DAY_OF_MONTH, day);
	}

	//TODO
	public void addValuesFromTransaction(ParkingEntry aTransaction) {
		
		if (aTransaction.memberType == MemberType.MONTHLY_DAY) {
			memberDayAmount += 1;
			memberPriceValue += aTransaction.price;
		} else if (aTransaction.memberType == MemberType.MONTHLY_NIGHT) {
			memberNightAmount += 1;
			memberPriceValue += aTransaction.price;
		} else if (aTransaction.memberType == MemberType.MONTHLY_ALL) {
			memberAllAmount += 1;
			memberPriceValue += aTransaction.price;
		} else if (aTransaction.isCK) {
			ckAmount += 1;
			normalAndCkPriceValue += aTransaction.price;
		} else if (aTransaction.entryType == EntryType.CAR) {
			normalAmount += 1;
			normalAndCkPriceValue += aTransaction.price;
		} else if (aTransaction.entryType == EntryType.MOTORCYCLE) {
			motorcycleAmount += 1;
			motorcyclePriceValue += aTransaction.price;
		}
		
	}
	
	public int getTotalPriceValue() {
		return normalAndCkPriceValue+motorcyclePriceValue+memberPriceValue;
	}
	
}
