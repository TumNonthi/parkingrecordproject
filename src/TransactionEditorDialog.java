import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JFrame;


public class TransactionEditorDialog extends JDialog {

	private TransactionEditorGui editorPanel;
	
	public TransactionEditorDialog(JFrame aFrame, MainServer mainServer) {
		super(aFrame,true);
		
		editorPanel = new TransactionEditorGui(mainServer);
		
		setContentPane(editorPanel);
		setResizable(false);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				clearAndHide();
			}
		});
		
	}
	
	public void clearAndHide() {
		
		editorPanel.resetPage();
		setVisible(false);
	}
	
}
