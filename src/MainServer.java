import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Calendar;

public class MainServer {

	private static int uniqueId;

	private int port;

	private boolean isActive;

	private boolean isStarted = false;

	private ServerSocket serverSocket;

	private MainServerGui mainServerGui;

	private ArrayList<ClientListener> clientListeners;

	private ArrayList<MemberCarEntry> memberCarList;
	private MemberCarEntry currentMemberCarEntry;

	private ArrayList<ParkingEntry> parkingCarList;
	private ParkingEntry currentParkingEntry;

	private ArrayList<String> barcodeList;
	private int currentBarcodeIndex = -1;

//	private ArrayList<ArrayList<ParkingEntry>> transactionArray;
//	private ArrayList<ArrayList<ParkingEntry>> requestArray;
	
	private int currentTransactionMonth = -1;
	private ArrayList<ParkingEntry> currentTransactionList = null;
	
	private int currentTransactionId = -1;
	
	private ParkingEntry currentInfoEntry = null;

	private final String filename_memberCarList = "appdata/memberCarList.dpk";
	private final String filename_parkingCarList = "appdata/parkingCarList.dpk";
	private final String filename_barcodeList = "appdata/barcodeList.dpk";
	private final String filename_transactionList = "appdata/transactions_";
	private final String filename_requestList = "appdata/requests_";
	
	private final String filename_transactionId = "appdata/transactionId.dpk";
	
	private final String filename_report_barcodeList = "info/barcodeList.xls";
	private final String filename_report_memberCarList = "info/memberList.xls";
	private final String filename_report_parkingCarList = "info/parkingList.xls";
	private final String filename_report_monthly = "info/monthly";
	private final String filename_report_daily = "info/daily";
	private final String filename_report_processed_monthly = "reports/report";
	
	public final int processedReportModNum = 9;

	public MainServer(int port) {
		this.port = port;
		clientListeners = new ArrayList<ClientListener>();
		loadMemberCarList();
		loadParkingCarList();
		loadBarcodeList();
		loadTransactionId();
		//initializeTransactionArray();
		//loadRequestArray();

		/*
		 * try { File dir = new File("appdata/text.xls").getParentFile();
		 * dir.mkdir(); PrintWriter out = new PrintWriter("appdata/text.xls");
		 * out.println("d\te\tf"); out.println("4\t5\t6");
		 * out.println("7\t8\t9"); out.close(); } catch (Exception e) {
		 * e.printStackTrace(); }
		 */
		
//		buildBarcodeReport();
//		buildMemberReport();
//		buildParkingReport();
//		buildDailyTransactionReport(Calendar.getInstance());
//		buildMonthlyTransactionReport(11);

	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setServerGUI(MainServerGui serverGui) {
		this.mainServerGui = serverGui;
	}

	public void setCurrentMemberCarEntry(MemberCarEntry mce) {
		currentMemberCarEntry = mce;
	}
	
	public void setCurrentParkingEntry(ParkingEntry pe) {
		currentParkingEntry = pe;
	}
	
	public void setCurrentBarcodeIndex(int bci) {
		currentBarcodeIndex = bci;
	}
	
	public MemberCarEntry getCurrentMemberCarEntry() {
		return currentMemberCarEntry;
	}
	
	public ParkingEntry getCurrentParkingEntry() {
		return currentParkingEntry;
	}
	
	public int getCurrentBarcodeIndex() {
		return currentBarcodeIndex;
	}

	synchronized public void loadMemberCarList() {
		try {
			FileInputStream fis = new FileInputStream(filename_memberCarList);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object ro = ois.readObject();
			if (ro instanceof ArrayList<?>) {
				memberCarList = (ArrayList<MemberCarEntry>) ro;
			} else {
				memberCarList = new ArrayList<MemberCarEntry>();
				saveMemberCarList();
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
			memberCarList = new ArrayList<MemberCarEntry>();
			saveMemberCarList();
		}
	}

	synchronized public void saveMemberCarList() {
		if (memberCarList == null) {
			loadMemberCarList();
			return;
		}
		try {
			File dir = new File(filename_memberCarList).getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(filename_memberCarList);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(memberCarList);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	synchronized public void loadParkingCarList() {
		try {
			FileInputStream fis = new FileInputStream(filename_parkingCarList);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object ro = ois.readObject();
			if (ro instanceof ArrayList<?>) {
				parkingCarList = (ArrayList<ParkingEntry>) ro;
			} else {
				parkingCarList = new ArrayList<ParkingEntry>();
				saveParkingCarList();
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
			parkingCarList = new ArrayList<ParkingEntry>();
			saveParkingCarList();
		}
	}

	synchronized public void saveParkingCarList() {
		if (parkingCarList == null) {
			loadParkingCarList();
			return;
		}
		try {
			File dir = new File(filename_parkingCarList).getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(filename_parkingCarList);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(parkingCarList);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	synchronized public void loadBarcodeList() {
		try {
			FileInputStream fis = new FileInputStream(filename_barcodeList);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object ro = ois.readObject();
			if (ro instanceof ArrayList<?>) {
				barcodeList = (ArrayList<String>) ro;
			} else {
				barcodeList = new ArrayList<String>();
				saveBarcodeList();
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
			barcodeList = new ArrayList<String>();
			saveBarcodeList();
		}
		
		/*
		if (barcodeList != null) {
			for (int i = 0; i<barcodeList.size(); i++) {
				System.out.println(barcodeList.get(i));
			}
		}
		*/
		
	}

	synchronized public void saveBarcodeList() {
		if (barcodeList == null) {
			loadBarcodeList();
			return;
		}
		try {
			File dir = new File(filename_barcodeList).getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(filename_barcodeList);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(barcodeList);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	synchronized private void saveTransactionId() {
		
		Integer idObject = new Integer(currentTransactionId);
		
		try {
			File dir = new File(filename_transactionId).getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(filename_transactionId);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(idObject);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	synchronized private int loadTransactionId() {
		
		try {
			FileInputStream fis = new FileInputStream(filename_transactionId);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object ro = ois.readObject();
			if (ro instanceof Integer) {
				currentTransactionId = ((Integer) ro).intValue();
			} else {
				currentTransactionId = 0;
				saveTransactionId();
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			//e.printStackTrace();
			currentTransactionId = 0;
			saveTransactionId();
		}
		
		if (currentTransactionId < 0) {
			currentTransactionId = 0;
			saveTransactionId();
		}
		
		return currentTransactionId;
	}
	
	synchronized public String getTransactionId() {
		
		int id = currentTransactionId;
		if (id < 0) {
			id = loadTransactionId();
		}
		
		currentTransactionId = (id+1)%100000000;
		saveTransactionId();
		
		return ""+id;
		
	}
	
	/*
	public void initializeTransactionArray() {
		
		if (transactionArray == null) {
			transactionArray = new ArrayList<ArrayList<ParkingEntry>>();
			for (int i = 0; i < 12; i++) {
				transactionArray.add(new ArrayList<ParkingEntry>());
			}
		}
		
	}
	*/
	
	/*
	public void loadTransactionArray() {

		transactionArray = new ArrayList<ArrayList<ParkingEntry>>();

		for (int i = 0; i < 12; i++) {

			try {
				FileInputStream fis = new FileInputStream(
						filename_transactionList + i);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Object ro = ois.readObject();
				if (ro instanceof ArrayList<?>) {
					transactionArray.add((ArrayList<ParkingEntry>) ro);
				} else {
					transactionArray.add(new ArrayList<ParkingEntry>());
					saveTransactionList(i);
				}
				ois.close();
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
//				ArrayList<ParkingEntry> aList = new ArrayList<ParkingEntry>();
//				for (int j = 0; j<30000; j++) {
//					aList.add(new ParkingEntry(100000l,999990l,EntryType.NORMAL));
//				}
//				transactionArray.add(aList);
				transactionArray.add(new ArrayList<ParkingEntry>());
				saveTransactionList(i);
			}

		}

	}
	*/
	
	/*
	public void loadTransactionList(int month) {
		
		if (transactionArray == null) {
			initializeTransactionArray();
		}
		
		try {
			FileInputStream fis = new FileInputStream(
					filename_transactionList + month + ".dpk");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object ro = ois.readObject();
			if (ro instanceof ArrayList<?>) {
				transactionArray.set(month, (ArrayList<ParkingEntry>) ro);
			} else {
				saveTransactionList(month);
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
			saveTransactionList(month);
		}
	}
	*/

	// month 0-11
	/*
	public void saveTransactionList(int month) {

		if (month >= 12) {
			System.err.println("invalid month index");
			return;
		}

		if (transactionArray == null) {
			initializeTransactionArray();
			return;
		}

		try {
			File dir = new File(filename_transactionList + month + ".dpk")
					.getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(
					filename_transactionList + month + ".dpk");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(transactionArray.get(month));
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/
	
	synchronized public void saveTransactionList(int month, ArrayList<ParkingEntry> aTransactionList) {
		
		if (month >= 12) {
			System.err.println("invalid month index");
			return;
		}

		try {
			File dir = new File(filename_transactionList + month + ".dpk")
					.getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(
					filename_transactionList + month + ".dpk");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(aTransactionList);
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	synchronized public ArrayList<ParkingEntry> getTransactionList(int month) {
		
		ArrayList<ParkingEntry> retval = null;
		
		if (month >= 12) {
			System.err.println("invalid month index");
			return null;
		}
		
		if (currentTransactionMonth == month && currentTransactionList != null) {
			return currentTransactionList;
		}
		
		try {
			FileInputStream fis = new FileInputStream(
					filename_transactionList + month + ".dpk");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Object ro = ois.readObject();
			if (ro instanceof ArrayList<?>) {
				retval = (ArrayList<ParkingEntry>) ro;
			} else {
				retval = new ArrayList<ParkingEntry>();
				saveTransactionList(month, retval);
			}
			ois.close();
			fis.close();
		} catch (Exception e) {
			retval = new ArrayList<ParkingEntry>();
			e.printStackTrace();
			saveTransactionList(month, retval);
		}
		
		if (retval != null) {
			currentTransactionList = retval;
			currentTransactionMonth = month;
		}
		return retval;
		
//		if (transactionArray == null) {
//			initializeTransactionArray();
//		}
		
//		retval = transactionArray.get(month);
		
		/*
		if (retval != null && retval.size() > 0) {
			return retval;
		} else {
			
			try {
				FileInputStream fis = new FileInputStream(
						filename_transactionList + month + ".dpk");
				ObjectInputStream ois = new ObjectInputStream(fis);
				Object ro = ois.readObject();
				if (ro instanceof ArrayList<?>) {
					retval = (ArrayList<ParkingEntry>) ro;
				} else {
					saveTransactionList(month, new ArrayList<ParkingEntry>());
				}
				ois.close();
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
				saveTransactionList(month);
			}
			
			return retval;
			
		}
		*/
		
	}
	
	synchronized public boolean addTransaction(int month, ParkingEntry aTransaction) {
		
		if (month >= 12) {
			System.err.println("invalid month index");
			return false;
		}
		
		if (currentTransactionMonth == month && currentTransactionList != null) {
			
		} else {
			getTransactionList(month);
		}
		
		if (currentTransactionList == null) {
			currentTransactionList = new ArrayList<ParkingEntry>();
		}
		
		if (currentTransactionList.size() > 0) {
			int listYear = currentTransactionList.get(0).getYear();
			int entryYear = aTransaction.getYear();
			
			if (entryYear > listYear) {
				currentTransactionList = new ArrayList<ParkingEntry>();
			} else if (entryYear == listYear) {
				
			} else {
				System.err.println("outdated entry");
				return false;
			}
			
		}
		
		currentTransactionList.add(aTransaction);
		saveTransactionList(month, currentTransactionList);
		return true;
		
	}
	
	synchronized public boolean addTransaction(ParkingEntry aTransaction) {
		return addTransaction(aTransaction.getMonth(), aTransaction);
	}
	
	synchronized public boolean removeTransaction(String id) {
		
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		
		ArrayList<ParkingEntry> aTransactionList;
		
		for (int i = 0; i<12 ;i++) {
			
			int monthIndex = (month+12-i)%12;
			
			aTransactionList = getTransactionList(monthIndex);
			
			if (aTransactionList != null) {
				for (int j=0; j<aTransactionList.size(); j++) {
					
					if (aTransactionList.get(j).uniqueId.equals(id)) {
						aTransactionList.remove(j);
						this.saveTransactionList(monthIndex, aTransactionList);
						return true;
					}
				}
			}
			
		}
		
		return false;
		
	}
	
	synchronized public boolean changeTransactionInfo(ParkingEntry oldEntry, ParkingEntry infoEntry) {
		
		if (!removeTransaction(oldEntry.uniqueId)) {
			return false;
		}
		return addTransaction(infoEntry);
		
	}

	/*
	public void loadRequestArray() {

		requestArray = new ArrayList<ArrayList<ParkingEntry>>();

		for (int i = 0; i < 12; i++) {

			try {
				FileInputStream fis = new FileInputStream(filename_requestList
						+ i);
				ObjectInputStream ois = new ObjectInputStream(fis);
				Object ro = ois.readObject();
				if (ro instanceof ArrayList<?>) {
					requestArray.add((ArrayList<ParkingEntry>) ro);
				} else {
					requestArray.add(new ArrayList<ParkingEntry>());
					saveRequestList(i);
				}
				ois.close();
				fis.close();
			} catch (Exception e) {
				e.printStackTrace();
//				ArrayList<ParkingEntry> aList = new ArrayList<ParkingEntry>();
//				for (int j = 0; j<30000; j++) {
//					aList.add(new ParkingEntry(100000l,999990l,EntryType.NORMAL));
//				}
//				requestArray.add(aList);
				requestArray.add(new ArrayList<ParkingEntry>());
				saveRequestList(i);
			}

		}

	}
	*/

	/*
	// month 0-11
	public void saveRequestList(int month) {

		if (month >= 12) {
			System.err.println("invalid month index");
			return;
		}

		if (requestArray == null) {
			loadRequestArray();
			return;
		} else if (requestArray.get(month) == null) {
			loadRequestArray();
			return;
		}

		try {
			File dir = new File(filename_requestList + month)
					.getParentFile();
			dir.mkdir();
			FileOutputStream fos = new FileOutputStream(
					filename_requestList + month);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(requestArray.get(month));
			oos.flush();
			oos.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/

	private void displayLog(int type, String message) {
		switch (type) {
		case ConfigValues.LOG_NORMAL:
			System.out.println(message);
			break;
		case ConfigValues.LOG_ERROR:
			System.err.println(message);
			break;
		}
	}

	synchronized void remove(int id) {
		for (int i = 0; i < clientListeners.size(); i++) {
			ClientListener cl = clientListeners.get(i);
			if (cl.id == id) {
				clientListeners.remove(i);
				return;
			}
		}
	}

	public boolean startServerLobby() {

		if (isStarted) {
			displayLog(ConfigValues.LOG_ERROR, "Server is already running");
			return false;
		}

		isActive = true;

		try {

			serverSocket = new ServerSocket(port);

			new Thread() {
				public void run() {
					while (isActive) {
						displayLog(ConfigValues.LOG_NORMAL,
								"Server is waiting for Clients on port: "
										+ port);

						try {
							Socket socket = serverSocket.accept();
							if (!isActive) {
								break;
							}

							connectClientLobby(socket);

						} catch (IOException eIO) {
							displayLog(ConfigValues.LOG_ERROR,
									"Error accepting connections to server socket: "
											+ eIO);
						}

					}

					try {

						for (int i = 0; i < clientListeners.size(); i++) {
							ClientListener cl = clientListeners.get(i);
							try {
								/*
								 * cl.sInput.close(); cl.sOutput.close();
								 * cl.socket.close();
								 */
								cl.writeMsg(new PR_ControlMessage(
										PR_ControlMessage.LOGOUT, ""
												+ cl.getClientId(),
										PR_ControlMessage.SERVER_ID, cl
												.getClientId()));
								// cl.keepGoing = false;
								// cl.close();
							} catch (Exception e) {

							}
						}

						serverSocket.close();

					} catch (Exception e) {
						displayLog(ConfigValues.LOG_ERROR,
								"Error closing the Server/Clients: " + e);
					}
				}
			}.start();

		} catch (Exception e) {
			displayLog(ConfigValues.LOG_ERROR, "Error creating Server Socket: "
					+ e);
			return false;
		}

		return true;

	}

	protected void stop() {
		isActive = false;
		try {
			new Socket("localhost", port);
		} catch (Exception e) {

		}
	}

	private synchronized void connectClientLobby(Socket socket) {

		ClientListener cl = new ClientListener(socket);

		clientListeners.add(cl);

		cl.start();

		cl.writeMsg(new PR_ControlMessage(
				PR_ControlMessage.LOGIN_SUCCESSFUL_LOBBY,
				"" + cl.getClientId(), PR_ControlMessage.SERVER_ID, cl
						.getClientId()));
	}
	
	synchronized public boolean isInBarcodeList(String bc) {
		
		for (int i = 0; i<barcodeList.size(); i++) {
			if (barcodeList.get(i).equals(bc)) {
				return true;
			}
		}
		
		return false;
		
	}
	
	synchronized public int getBarcodeIndex(String bc) {
		
		for (int i = 0; i<barcodeList.size(); i++) {
			if (barcodeList.get(i).equals(bc)) {
				return i;
			}
		}
		
		return -1;
		
	}
	
	/*
	synchronized public byte[] getParkingPlateNumBytesFromBarcode(String bc) {
		
		for (int i = 0; i<parkingCarList.size(); i++) {
			ParkingEntry pe = parkingCarList.get(i);
			if (pe.barcodeId.equals(bc)) {
				return pe.plateNumberBytes;
			}
		}
		
		return null;
		
	}
	*/
	
	synchronized public String getParkingPlateNumStringFromBarcode(String bc) {
		
		for (int i = 0; i<parkingCarList.size(); i++) {
			ParkingEntry pe = parkingCarList.get(i);
			if (pe.barcodeId.equals(bc)) {
//				String retVal;
//				try {
//					retVal = new String(pe.plateNumberBytes,"UTF-8");
//				} catch(Exception e) {
//					retVal = new String(pe.plateNumberBytes);
//				}
//				return retVal;
				return pe.plateNumString;
			}
		}
		
		return null;
		
	}
	
	synchronized public ParkingEntry getParkingEntryFromBarcode(String bc) {
		
		for (int i = 0; i<parkingCarList.size(); i++) {
			ParkingEntry pe = parkingCarList.get(i);
			if (pe.barcodeId.equals(bc)) {
				return pe;
			}
		}
		
		return null;
		
	}
	
	synchronized public ParkingEntry getParkingEntryFromPlateNumStr(String pnStr) {
		
		for (int i = 0; i<parkingCarList.size(); i++) {
			ParkingEntry pe = parkingCarList.get(i);
			String plateNumStrToCompare;
//			try {
//				plateNumStrToCompare = new String(pe.plateNumberBytes,"UTF-8");
//			} catch(Exception e) {
//				plateNumStrToCompare = new String(pe.plateNumberBytes);
//			}
			plateNumStrToCompare = pe.plateNumString;
			if (plateNumStrToCompare.equals(pnStr)) {
				return pe;
			}
		}
		
		return null;
		
	}
	
	synchronized public ParkingEntry getParkingEntryFromTransactionId(String id) {
		
		for (int i = 0; i<parkingCarList.size(); i++) {
			ParkingEntry pe = parkingCarList.get(i);
			if (pe.uniqueId.equals(id)) {
				return pe;
			}
		}
		
		return null;
		
	}
	
	synchronized public String getParkingBarcodeFromPlateNumStr(String pnStr) {
		
		for (int i = 0; i<parkingCarList.size(); i++) {
			ParkingEntry pe = parkingCarList.get(i);
			String plateNumStrToCompare;
//			try {
//				plateNumStrToCompare = new String(pe.plateNumberBytes,"UTF-8");
//			} catch(Exception e) {
//				plateNumStrToCompare = new String(pe.plateNumberBytes);
//			}
			plateNumStrToCompare = pe.plateNumString;
			if (plateNumStrToCompare.equals(pnStr)) {
				return pe.barcodeId;
			}
		}
		
		return null;
		
	}
	
	synchronized public MemberCarEntry getMemberEntryFromBarcode(String bc) {
		
		for (int i = 0; i<memberCarList.size(); i++) {
			MemberCarEntry mce = memberCarList.get(i);
			if (mce.barcodeId.equals(bc)) {
				return mce;
			}
		}
		
		return null;
		
	}
	
	synchronized public MemberCarEntry getMemberEntryFromPlateNumStr(String pnStr) {
		
		for (int i = 0; i<memberCarList.size(); i++) {
			MemberCarEntry mce = memberCarList.get(i);
			String plateNumStrToCompare;
//			try {
//				plateNumStrToCompare = new String(mce.plateNumberBytes,"UTF-8");
//			} catch(Exception e) {
//				plateNumStrToCompare = new String(mce.plateNumberBytes);
//			}
			plateNumStrToCompare = mce.plateNumString;
			if (plateNumStrToCompare.equals(pnStr)) {
				return mce;
			}
		}
		
		return null;
		
	}
	
	synchronized public ParkingEntry getTransaction(String id) {
		
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		
		ArrayList<ParkingEntry> aTransactionList;
		ParkingEntry retval;
		
		for (int i = 0; i<12 ;i++) {
			
			aTransactionList = getTransactionList((month+12-i)%12);
			
			if (aTransactionList != null) {
				for (int j=0; j<aTransactionList.size(); j++) {
					
					retval = aTransactionList.get(j);
					
					if (retval.uniqueId.equals(id)) {
						return retval;
					}
				}
			}
			
		}
		
		return null;
	}
	
	synchronized public boolean addBarcode(String bc) {
		
		int bcIndex = getBarcodeIndex(bc);
		ParkingEntry pe = getParkingEntryFromBarcode(bc);
		MemberCarEntry mce = getMemberEntryFromBarcode(bc);
		
		if (bcIndex < 0 && pe==null && mce==null) {
			barcodeList.add(bc);
			saveBarcodeList();
			return true;
		} else {
			return false;
		}
	}
	
	synchronized public boolean removeBarcode(String bc) {
		int bcIndex = getBarcodeIndex(bc);
		ParkingEntry pe = getParkingEntryFromBarcode(bc);
		MemberCarEntry mce = getMemberEntryFromBarcode(bc);
		if (bcIndex >= 0 && pe==null && mce==null) {
			barcodeList.remove(bcIndex);
			saveBarcodeList();
			return true;
		} else {
			return false;
		}
	}
	
	synchronized public boolean addMember(MemberCarEntry mce) {
		
		if ( (getBarcodeIndex(mce.barcodeId) >= 0) || getMemberEntryFromBarcode(mce.barcodeId) != null) {
			return false;
		} else {
			memberCarList.add(mce);
			saveMemberCarList();
			return true;
		}
		
	}
	
	synchronized public boolean removeMember(MemberCarEntry mce) {
		if (mce != null) {
			
//			ParkingEntry pe = getParkingEntryFromBarcode(mce.barcodeId);
//			
//			if (pe == null) {
//				boolean retval = memberCarList.remove(mce);
//				saveMemberCarList();
//				return retval;
//			} else {
//				return false;
//			}
			
			boolean retval = memberCarList.remove(mce);
			saveMemberCarList();
			return retval;

		} else {
			return false;
		}
	}
	
	synchronized public boolean changeMemberInfo(MemberCarEntry memberEntry, MemberCarEntry infoEntry) {
		if (memberEntry != null && infoEntry != null) {
			if (isInBarcodeList(infoEntry.barcodeId)) {
				return false;
			}
			MemberCarEntry aMemberFromNewBarcode = getMemberEntryFromBarcode(infoEntry.barcodeId);
			if (aMemberFromNewBarcode != null && aMemberFromNewBarcode != memberEntry) {
				return false;
			}
//			MemberCarEntry aMemberFromNewPlateNum = getMemberEntryFromPlateNumStr(infoEntry.getPlateNumString());
//			if (aMemberFromNewPlateNum != null && aMemberFromNewPlateNum != memberEntry) {
//				return false;
//			}
			
			memberEntry.copyInfo(infoEntry);
			saveMemberCarList();
			return true;
			
		} else {
			return false;
		}
	}
	
	synchronized public boolean changeCurrentMemberInfo(MemberCarEntry infoEntry) {
		return changeMemberInfo(currentMemberCarEntry, infoEntry);
	}
	
	synchronized public boolean removeCurrentMember() {
		return removeMember(currentMemberCarEntry);
	}
	
	synchronized public ParkingEntry addParkingEntry(ParkingEntry anEntry) {
		
		if ( (getBarcodeIndex(anEntry.barcodeId)<0 && getMemberEntryFromBarcode(anEntry.barcodeId)==null)) {
			return null;
		} else {
			
			anEntry.uniqueId = getTransactionId();
			
			parkingCarList.add(anEntry);
			saveParkingCarList();
			return anEntry;
		}
		
	}
	
	synchronized public boolean removeParkingEntry(ParkingEntry anEntry) {
		if (anEntry != null) {
			boolean retval = parkingCarList.remove(anEntry);
			saveParkingCarList();
			return retval;
		} else {
			return false;
		}
	}
	
	synchronized public boolean removeCurrentParkingEntry() {
		return removeParkingEntry(currentParkingEntry);
	}
	
	public boolean changeParkingEntryInfo(ParkingEntry anEntry, ParkingEntry infoEntry) {
		
		if (anEntry != null && infoEntry != null) {
			if (!isInBarcodeList(infoEntry.barcodeId) && (!anEntry.barcodeId.equals(infoEntry.barcodeId))) {
				return false;
			}
			ParkingEntry aParkingEntryFromNewBarcode = getParkingEntryFromBarcode(infoEntry.barcodeId);
			if (aParkingEntryFromNewBarcode != null && aParkingEntryFromNewBarcode != anEntry) {
				return false;
			}
//			ParkingEntry aParkingEntryFromNewPlateNum = getParkingEntryFromPlateNumStr(infoEntry.getPlateNumString());
//			if (aParkingEntryFromNewPlateNum != null && aParkingEntryFromNewPlateNum != anEntry) {
//				return false;
//			}
			
			anEntry.copyInfo(infoEntry);
			saveParkingCarList();
			return true;
			
		} else {
			return false;
		}
		
	}
	
	public boolean changeCurrentParkingEntryInfo(ParkingEntry infoEntry) {
		return changeParkingEntryInfo(currentParkingEntry, infoEntry);
	}
	
	private String getBarcodeStringFromString(String str) {
		
//		int barcodeInt = -1;
//		try {
//			barcodeInt = Integer.parseInt(str);
//		} catch (NumberFormatException nfe) {
//			barcodeInt = -1;
//		}
//		if (barcodeInt < 0) {
//			return null;
//		} else {
//			return ""+barcodeInt;
//		}
		return str;
		
	}
	
	class ClientListener extends Thread {

		Socket socket;
		ObjectInputStream sInput;
		ObjectOutputStream sOutput;

		int id;

		String username;

		PR_ControlMessage cm;

		boolean keepGoing = true;

		ClientListener(Socket socket) {
			id = ++uniqueId;
			this.socket = socket;

			System.out
					.println("Thread trying to create Object Input/Output Streams");

			try {
				sOutput = new ObjectOutputStream(socket.getOutputStream());
				sInput = new ObjectInputStream(socket.getInputStream());

				username = (String) sInput.readObject();
				displayLog(ConfigValues.LOG_NORMAL, username + " connected.");

			} catch (IOException eIO) {
				displayLog(ConfigValues.LOG_ERROR,
						"Error creating new I/O Streams: " + eIO);
			} catch (ClassNotFoundException eCNF) {

			}

		}

		private boolean writeMsg(PR_ControlMessage cm) {
			if (!socket.isConnected()) {
				close();
				return false;
			}
			try {
				sOutput.writeObject(cm);
			} catch (IOException eIO) {
				displayLog(ConfigValues.LOG_ERROR, "Error sending message to "
						+ username);
				displayLog(ConfigValues.LOG_ERROR, eIO.toString());
				return false;
			}
			return true;
		}

		public void run() {

			keepGoing = true;

			while (keepGoing) {

				try {
					cm = (PR_ControlMessage) sInput.readObject();

					switch (cm.getType()) {

					case PR_ControlMessage.LOGOUT:
						keepGoing = false;
						break;

					case PR_ControlMessage.ID_SEARCH:
						getRequestResult(cm);
						break;
						
					case PR_ControlMessage.CHECK_IN:
						checkIn(cm);
						break;
						
					case PR_ControlMessage.CHECK_OUT:
						checkOut(cm);
						break;
						
					case PR_ControlMessage.PING_EMPTY:
						respondPingEmpty(cm);
						break;
						
					}

				} catch (IOException eIO) {
					displayLog(ConfigValues.LOG_ERROR,
							"Error reading Streams: " + eIO);
					break;
				} catch (ClassNotFoundException eCNF) {
					break;
				}

			}

			remove(id);
			close();

		}

		private void close() {

			try {
				if (sInput != null) {
					sInput.close();
					sInput = null;
				}
			} catch (Exception e) {

			}

			try {
				if (sOutput != null) {
					sOutput.close();
					sOutput = null;
				}
			} catch (Exception e) {

			}

			try {
				if (socket != null) {
					socket.close();
					socket = null;
				}
			} catch (Exception e) {

			}

		}

		public int getClientId() {
			return id;
		}
		
		private synchronized void getRequestResult(PR_ControlMessage cm) {
			String bc = getBarcodeStringFromString(cm.getMessage());
			boolean barcodeInSystem = false;
			MemberCarEntry mce = null;
			ParkingEntry pe = null;
			
			MemberCarEntry retMce = null;
			ParkingEntry retPe = null;
			
			if (bc==null) {
				barcodeInSystem = false;
			} else {
				barcodeInSystem = isInBarcodeList(bc);
				pe = getParkingEntryFromBarcode(bc);
				mce = getMemberEntryFromBarcode(bc);
			}
			
			currentInfoEntry = cm.getParkingEntry();
			if (currentInfoEntry == null) {
				currentInfoEntry = new ParkingEntry();
			}
			
			Calendar cNow = Calendar.getInstance();
			
			currentInfoEntry.entryUTC = cNow.getTimeInMillis();
			
			if (mce != null) {
				retMce = new MemberCarEntry();
				retMce.copyInfo(mce);
			}
			
			if (pe != null) {
				
				retPe = new ParkingEntry();
				retPe.copyInfo(pe);
				retPe.uniqueId = pe.uniqueId;
				
				if (currentInfoEntry != null) {
//					if (retPe.entryType == EntryType.NORMAL && currentInfoEntry.entryType == EntryType.CK) {
//						retPe.entryType = EntryType.CK;
//					}
					retPe.isCK = currentInfoEntry.isCK;
				}
				
//				Calendar cExit = Calendar.getInstance();
				retPe.exitUTC = cNow.getTimeInMillis();
				currentInfoEntry.exitUTC = retPe.exitUTC;
				
				if (retMce != null && retMce.getPlateNumString().equals(retPe.getPlateNumString())) {
					retPe.expirationDate = retMce.expirationDate;
//					retPe.entryType = retMce.entryType;
				}
				
				retPe.price = retPe.CalculatePrice();
				
			}
			
			PR_ControlMessage retMsg;
			
			if (barcodeInSystem || (retPe!=null) || (retMce!=null)) {
				retMsg = new PR_ControlMessage(PR_ControlMessage.ID_RESULT_FOUND,bc,PR_ControlMessage.SERVER_ID,cm.getSenderId(),retPe,retMce);
				retMsg.setTimestamp(currentInfoEntry.entryUTC);
			} else {
				retMsg = new PR_ControlMessage(PR_ControlMessage.ID_RESULT_NOTFOUND,bc,PR_ControlMessage.SERVER_ID,cm.getSenderId());
			}
			
			retMsg.setWaitingId(cm.getWaitingId());
			
			writeMsg(retMsg);
			
		}
		
		private synchronized void checkIn(PR_ControlMessage cm) {
			
			ParkingEntry pe = cm.getParkingEntry();
			ParkingEntry entryToAdd = new ParkingEntry();
			
			PR_ControlMessage retMsg;
			
			String err = "";
			boolean isError = false;
			
			if (pe == null) {
				err = "Null parking entry.";
				isError = true;
			} else {
				
				entryToAdd.copyInfo(pe);
				Calendar cNow = Calendar.getInstance();
				entryToAdd.entryUTC = cNow.getTimeInMillis();
				
			}
			
			if (isError) {
				retMsg = new PR_ControlMessage(PR_ControlMessage.CHECK_IN_ERROR,err,PR_ControlMessage.SERVER_ID,id);
				retMsg.setWaitingId(cm.getWaitingId());
				writeMsg(retMsg);
			} else {
				
				entryToAdd = addParkingEntry(entryToAdd);
				if (entryToAdd == null) {
					isError = true;
					err = "Invalid check-in info";
					retMsg = new PR_ControlMessage(PR_ControlMessage.CHECK_IN_ERROR,err,PR_ControlMessage.SERVER_ID,id);
					retMsg.setWaitingId(cm.getWaitingId());
					writeMsg(retMsg);
				} else {
					retMsg = new PR_ControlMessage(PR_ControlMessage.CHECK_IN_CONFIRMED,err,PR_ControlMessage.SERVER_ID,id,entryToAdd);
					retMsg.setWaitingId(cm.getWaitingId());
					writeMsg(retMsg);
				}
			}
			
		}
		
		private synchronized void checkOut(PR_ControlMessage cm) {
			
			ParkingEntry pe = cm.getParkingEntry();
			
			PR_ControlMessage retMsg;
			
			String err = "";
			boolean isError = false;
			
			ParkingEntry parkingEntryToRemove = null;
			
			if (pe == null) {
				err = "Null parking entry.";
				isError = true;
			} else {
				
				parkingEntryToRemove = getParkingEntryFromBarcode(pe.barcodeId);
				
			}
			
			if (isError) {
				retMsg = new PR_ControlMessage(PR_ControlMessage.CHECK_OUT_ERROR,err,PR_ControlMessage.SERVER_ID,id);
				retMsg.setWaitingId(cm.getWaitingId());
				writeMsg(retMsg);
			} else {
				
				ParkingEntry transaction = new ParkingEntry();
				transaction.copyInfo(pe);
				transaction.uniqueId = parkingEntryToRemove.uniqueId;
				
				if (removeParkingEntry(parkingEntryToRemove)) {
					addTransaction(transaction);
					retMsg = new PR_ControlMessage(PR_ControlMessage.CHECK_OUT_CONFIRMED,err,PR_ControlMessage.SERVER_ID,id,transaction);
					retMsg.setWaitingId(cm.getWaitingId());
					writeMsg(retMsg);
				} else {
					retMsg = new PR_ControlMessage(PR_ControlMessage.CHECK_OUT_ERROR,"Invalid entry not found",PR_ControlMessage.SERVER_ID,id);
					retMsg.setWaitingId(cm.getWaitingId());
					writeMsg(retMsg);
				}
				
			}
			
		}
		
		private void respondPingEmpty(PR_ControlMessage cm) {
			PR_ControlMessage retMsg = new PR_ControlMessage(PR_ControlMessage.PING_EMPTY_RESPONSE, "PONG", PR_ControlMessage.SERVER_ID, id);
			writeMsg(retMsg);
		}

	}
	
	
	
	
	public void buildBarcodeReport() {
		
		ArrayList<BarcodeReportEntry> bcList = new ArrayList<BarcodeReportEntry>();
		
		for (int i = 0; i<barcodeList.size(); i++) {
			
			BarcodeReportEntry bre = new BarcodeReportEntry(barcodeList.get(i), false);
			if (getParkingEntryFromBarcode(barcodeList.get(i))!=null) {
				bre.isParking = true;
			} else {
				bre.isParking = false;
			}
			bcList.add(bre);
			
		}
		
		ReportBuilder.buildBarcodeReport(filename_report_barcodeList, bcList);
		
	}
	
	public void buildMemberReport() {
		
		ReportBuilder.buildMemberReport(filename_report_memberCarList, memberCarList);
		
	}
	
	public void buildParkingReport() {
		
		ReportBuilder.buildParkingReport(filename_report_parkingCarList, parkingCarList);
		
	}
	
	public void buildDailyTransactionReport(Calendar cDate) {
		
		ArrayList<ParkingEntry> dailyTransactionList = new ArrayList<ParkingEntry>();
		
		int date = cDate.get(Calendar.DATE);
		int month = cDate.get(Calendar.MONTH);
		
		ArrayList<ParkingEntry> thisMonthTransactionList = this.getTransactionList(month);
		
		ParkingEntry sampleTransaction = thisMonthTransactionList.get(0);
		int year = sampleTransaction.getYear();
		cDate.set(Calendar.YEAR, year);
		
		for (int i = 0; i<thisMonthTransactionList.size(); i++) {
			
			ParkingEntry aTransaction = thisMonthTransactionList.get(i);
			
			Calendar cTransactionDate = Calendar.getInstance();
			cTransactionDate.setTimeInMillis(aTransaction.exitUTC);
			
			if (cTransactionDate.get(Calendar.DATE) == date && cTransactionDate.get(Calendar.MONTH) == month && cTransactionDate.get(Calendar.YEAR) == year) {
				dailyTransactionList.add(aTransaction);
			}
			
		}
		
		ReportBuilder.buildDailyReport(filename_report_daily, cDate, dailyTransactionList);
		
	}
	
	public void buildMonthlyTransactionReport(int month) {
		
		int year = -1;
		
		ArrayList<ParkingEntry> thisMonthTransactionList = this.getTransactionList(month);
		if (thisMonthTransactionList != null) {
			ParkingEntry transactionSample = thisMonthTransactionList.get(0);
			if (transactionSample != null) {
				year = transactionSample.getYear();
			}
		}
		
		if (year < 0) {
			Calendar cNow = Calendar.getInstance();
			year = cNow.get(Calendar.YEAR);
		}
		
		Calendar cMonth = Calendar.getInstance();
		cMonth.set(Calendar.MONTH, month);
		cMonth.set(Calendar.YEAR, year);
		cMonth.set(Calendar.DATE, 1);
		
		int numDayInMonth = cMonth.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		ArrayList<DayTransactionRecord> dtrList = new ArrayList<DayTransactionRecord>();
		for (int i = 1; i<=numDayInMonth; i++) {
			
			DayTransactionRecord dtr = new DayTransactionRecord(i,month,year);
			dtrList.add(dtr);
		}
		
		for (int i = 0; i<thisMonthTransactionList.size(); i++) {
			ParkingEntry aTransaction = thisMonthTransactionList.get(i);
			int date = aTransaction.getDate();
			if (date <= dtrList.size()) {
				dtrList.get(date-1).addValuesFromTransaction(aTransaction);
			}
		}
		
		ReportBuilder.buildMonthlyReport(filename_report_monthly, cMonth, dtrList);
		
	}
	
	public void buildProcessedMonthlyTransactionReport(int month, int modNum) {
		
		int year = -1;
		
		ArrayList<ParkingEntry> thisMonthTransactionList = this.getTransactionList(month);
		if (thisMonthTransactionList != null) {
			ParkingEntry transactionSample = thisMonthTransactionList.get(0);
			if (transactionSample != null) {
				year = transactionSample.getYear();
			}
		}
		
		if (year < 0) {
			Calendar cNow = Calendar.getInstance();
			year = cNow.get(Calendar.YEAR);
		}
		
		Calendar cMonth = Calendar.getInstance();
		cMonth.set(Calendar.MONTH, month);
		cMonth.set(Calendar.YEAR, year);
		cMonth.set(Calendar.DATE, 1);
		
		int numDayInMonth = cMonth.getActualMaximum(Calendar.DAY_OF_MONTH);
		
		ArrayList<DayTransactionRecord> dtrList = new ArrayList<DayTransactionRecord>();
		for (int i = 1; i<=numDayInMonth; i++) {
			
			DayTransactionRecord dtr = new DayTransactionRecord(i,month,year);
			dtrList.add(dtr);
		}
		
		for (int i = 0; i<thisMonthTransactionList.size(); i++) {
			ParkingEntry aTransaction = thisMonthTransactionList.get(i);
			int idInt = -1;
			try {
				idInt = Integer.parseInt(aTransaction.uniqueId);
			} catch (Exception e) {
				idInt = -1;
			}
			if ((idInt%modNum!=0) && (idInt-4)%modNum!=0) {
				continue;
			}
			int date = aTransaction.getDate();
			if (date <= dtrList.size()) {
				dtrList.get(date-1).addValuesFromTransaction(aTransaction);
			}
		}
		
		ReportBuilder.buildMonthlyReport(filename_report_processed_monthly, cMonth, dtrList);
		
	}

}
