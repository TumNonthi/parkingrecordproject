import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Test;

public class PricingTest {

	static MemberType m_none = MemberType.NONE;
	static MemberType m_day = MemberType.MONTHLY_DAY;
	static MemberType m_night = MemberType.MONTHLY_NIGHT;
	static MemberType m_all = MemberType.MONTHLY_ALL;

	@Test
	public void test1() {

		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();

		en.set(Calendar.YEAR, 2015);
		en.set(Calendar.MONTH, 9 - 1);
		en.set(Calendar.DATE, 10);
		en.set(Calendar.HOUR_OF_DAY, 14);
		en.set(Calendar.MINUTE, 58);
		en.set(Calendar.SECOND, 0);

		ex.set(Calendar.YEAR, 2015);
		ex.set(Calendar.MONTH, 9 - 1);
		ex.set(Calendar.DATE, 10);
		ex.set(Calendar.HOUR_OF_DAY, 17);
		ex.set(Calendar.MINUTE, 54);
		ex.set(Calendar.SECOND, 0);

		exp.set(Calendar.YEAR, 2016);
		exp.set(Calendar.MONTH, 4 - 1);
		exp.set(Calendar.DATE, 30);
		exp.set(Calendar.HOUR_OF_DAY, 0);
		exp.set(Calendar.MINUTE, 0);
		exp.set(Calendar.SECOND, 0);

		boolean ck = false;
		EntryType entryType = EntryType.CAR;

		assertEquals(60,
				ParkingEntry.CalculatePrice(en, ex, entryType, m_none, ck, exp));

	}
	
	@Test
	public void test2() {

		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();

		en.set(Calendar.YEAR, 2015);
		en.set(Calendar.MONTH, 9 - 1);
		en.set(Calendar.DATE, 10);
		en.set(Calendar.HOUR_OF_DAY, 22);
		en.set(Calendar.MINUTE, 58);
		en.set(Calendar.SECOND, 0);

		ex.set(Calendar.YEAR, 2015);
		ex.set(Calendar.MONTH, 9 - 1);
		ex.set(Calendar.DATE, 11);
		ex.set(Calendar.HOUR_OF_DAY, 4);
		ex.set(Calendar.MINUTE, 54);
		ex.set(Calendar.SECOND, 0);

		exp.set(Calendar.YEAR, 2016);
		exp.set(Calendar.MONTH, 4 - 1);
		exp.set(Calendar.DATE, 30);
		exp.set(Calendar.HOUR_OF_DAY, 0);
		exp.set(Calendar.MINUTE, 0);
		exp.set(Calendar.SECOND, 0);

		boolean ck = false;
		EntryType entryType = EntryType.CAR;

		assertEquals(60,
				ParkingEntry.CalculatePrice(en, ex, entryType, m_none, ck, exp));

	}
	
	@Test
	public void test3() {

		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();

		en.set(Calendar.YEAR, 2015);
		en.set(Calendar.MONTH, 9 - 1);
		en.set(Calendar.DATE, 10);
		en.set(Calendar.HOUR_OF_DAY, 17);
		en.set(Calendar.MINUTE, 58);
		en.set(Calendar.SECOND, 0);

		ex.set(Calendar.YEAR, 2015);
		ex.set(Calendar.MONTH, 9 - 1);
		ex.set(Calendar.DATE, 11);
		ex.set(Calendar.HOUR_OF_DAY, 4);
		ex.set(Calendar.MINUTE, 54);
		ex.set(Calendar.SECOND, 0);

		exp.set(Calendar.YEAR, 2016);
		exp.set(Calendar.MONTH, 4 - 1);
		exp.set(Calendar.DATE, 30);
		exp.set(Calendar.HOUR_OF_DAY, 0);
		exp.set(Calendar.MINUTE, 0);
		exp.set(Calendar.SECOND, 0);

		boolean ck = false;
		EntryType entryType = EntryType.CAR;

		assertEquals(120,
				ParkingEntry.CalculatePrice(en, ex, entryType, m_none, ck, exp));

	}
	
	@Test
	public void test4() {

		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();

		en.set(Calendar.YEAR, 2015);
		en.set(Calendar.MONTH, 9 - 1);
		en.set(Calendar.DATE, 10);
		en.set(Calendar.HOUR_OF_DAY, 17);
		en.set(Calendar.MINUTE, 58);
		en.set(Calendar.SECOND, 0);

		ex.set(Calendar.YEAR, 2015);
		ex.set(Calendar.MONTH, 9 - 1);
		ex.set(Calendar.DATE, 22);
		ex.set(Calendar.HOUR_OF_DAY, 4);
		ex.set(Calendar.MINUTE, 54);
		ex.set(Calendar.SECOND, 0);

		exp.set(Calendar.YEAR, 2016);
		exp.set(Calendar.MONTH, 4 - 1);
		exp.set(Calendar.DATE, 30);
		exp.set(Calendar.HOUR_OF_DAY, 0);
		exp.set(Calendar.MINUTE, 0);
		exp.set(Calendar.SECOND, 0);

		boolean ck = false;
		EntryType entryType = EntryType.CAR;

		assertEquals(3420,
				ParkingEntry.CalculatePrice(en, ex, entryType, m_none, ck, exp));

	}
	
	@Test
	public void test5() {

		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();

		en.set(Calendar.YEAR, 2015);
		en.set(Calendar.MONTH, 9 - 1);
		en.set(Calendar.DATE, 14);
		en.set(Calendar.HOUR_OF_DAY, 16);
		en.set(Calendar.MINUTE, 41);
		en.set(Calendar.SECOND, 0);

		ex.set(Calendar.YEAR, 2015);
		ex.set(Calendar.MONTH, 9 - 1);
		ex.set(Calendar.DATE, 14);
		ex.set(Calendar.HOUR_OF_DAY, 17);
		ex.set(Calendar.MINUTE, 43);
		ex.set(Calendar.SECOND, 0);

		exp.set(Calendar.YEAR, 2016);
		exp.set(Calendar.MONTH, 4 - 1);
		exp.set(Calendar.DATE, 30);
		exp.set(Calendar.HOUR_OF_DAY, 0);
		exp.set(Calendar.MINUTE, 0);
		exp.set(Calendar.SECOND, 0);

		boolean ck = false;
		EntryType entryType = EntryType.MOTORCYCLE;

		assertEquals(20,
				ParkingEntry.CalculatePrice(en, ex, entryType, m_none, ck, exp));

	}
	
	@Test
	public void test6() {

		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();

		en.set(Calendar.YEAR, 2015);
		en.set(Calendar.MONTH, 9 - 1);
		en.set(Calendar.DATE, 14);
		en.set(Calendar.HOUR_OF_DAY, 16);
		en.set(Calendar.MINUTE, 41);
		en.set(Calendar.SECOND, 0);

		ex.set(Calendar.YEAR, 2015);
		ex.set(Calendar.MONTH, 9 - 1);
		ex.set(Calendar.DATE, 14);
		ex.set(Calendar.HOUR_OF_DAY, 20);
		ex.set(Calendar.MINUTE, 43);
		ex.set(Calendar.SECOND, 0);

		exp.set(Calendar.YEAR, 2016);
		exp.set(Calendar.MONTH, 4 - 1);
		exp.set(Calendar.DATE, 30);
		exp.set(Calendar.HOUR_OF_DAY, 0);
		exp.set(Calendar.MINUTE, 0);
		exp.set(Calendar.SECOND, 0);

		boolean ck = false;
		EntryType entryType = EntryType.MOTORCYCLE;

		assertEquals(35,
				ParkingEntry.CalculatePrice(en, ex, entryType, m_none, ck, exp));

	}
	
	@Test
	public void test7() {

		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();

		en.set(Calendar.YEAR, 2015);
		en.set(Calendar.MONTH, 9 - 1);
		en.set(Calendar.DATE, 14);
		en.set(Calendar.HOUR_OF_DAY, 16);
		en.set(Calendar.MINUTE, 41);
		en.set(Calendar.SECOND, 0);

		ex.set(Calendar.YEAR, 2015);
		ex.set(Calendar.MONTH, 9 - 1);
		ex.set(Calendar.DATE, 14);
		ex.set(Calendar.HOUR_OF_DAY, 17);
		ex.set(Calendar.MINUTE, 43);
		ex.set(Calendar.SECOND, 0);

		exp.set(Calendar.YEAR, 2015);
		exp.set(Calendar.MONTH, 9 - 1);
		exp.set(Calendar.DATE, 18);
		exp.set(Calendar.HOUR_OF_DAY, 0);
		exp.set(Calendar.MINUTE, 0);
		exp.set(Calendar.SECOND, 0);

		boolean ck = false;
		EntryType entryType = EntryType.MOTORCYCLE;

		assertEquals(0,
				ParkingEntry.CalculatePrice(en, ex, entryType, m_all, ck, exp));

	}
	
	@Test
	public void test8() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "16:41-14/9/15");
		setCalendarWithString(ex , "17:43-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.MOTORCYCLE;
		MemberType m = m_all;

		assertEquals(100,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test9() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "16:41-14/9/15");
		setCalendarWithString(ex , "00:43-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.MOTORCYCLE;
		MemberType m = m_all;

		assertEquals(20,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test10() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-14/9/15");
		setCalendarWithString(ex , "00:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_all;

		assertEquals(0,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test11() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-14/9/15");
		setCalendarWithString(ex , "00:05-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_all;

		assertEquals(10,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test12() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-14/9/15");
		setCalendarWithString(ex , "08:05-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_all;

		assertEquals(120,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test13() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-14/9/15");
		setCalendarWithString(ex , "08:05-20/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_all;

		assertEquals(420,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test14() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-17/9/15");
		setCalendarWithString(ex , "19:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(0,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test15() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "07:51-17/9/15");
		setCalendarWithString(ex , "10:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(20,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test16() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-17/9/15");
		setCalendarWithString(ex , "20:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(10,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test17() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-16/9/15");
		setCalendarWithString(ex , "19:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(140,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test18() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-14/9/15");
		setCalendarWithString(ex , "20:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(430,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test19() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-17/9/15");
		setCalendarWithString(ex , "00:05-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(190,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test20() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-17/9/15");
		setCalendarWithString(ex , "06:05-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(260,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test21() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "14:51-17/9/15");
		setCalendarWithString(ex , "00:05-20/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_day;

		assertEquals(510,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test22() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "19:51-17/9/15");
		setCalendarWithString(ex , "03:05-18/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(0,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test23() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "17:51-16/9/15");
		setCalendarWithString(ex , "07:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(20,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test24() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "21:51-16/9/15");
		setCalendarWithString(ex , "08:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(20,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test25() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "19:51-16/9/15");
		setCalendarWithString(ex , "09:05-18/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(240,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test26() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "19:51-14/9/15");
		setCalendarWithString(ex , "09:05-17/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(440,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test27() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "21:51-17/9/15");
		setCalendarWithString(ex , "00:05-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(210,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test28() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "21:51-17/9/15");
		setCalendarWithString(ex , "21:05-19/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(500,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	@Test
	public void test29() {
		
		Calendar en = Calendar.getInstance();
		Calendar ex = Calendar.getInstance();
		Calendar exp = Calendar.getInstance();
		
		setCalendarWithString(en , "22:51-17/9/15");
		setCalendarWithString(ex , "00:05-20/9/15");
		setCalendarWithString(exp, "00:00-18/9/15");
		
		boolean ck = false;
		EntryType entryType = EntryType.CAR;
		MemberType m = m_night;

		assertEquals(530,
				ParkingEntry.CalculatePrice(en, ex, entryType, m, ck, exp));
		
	}
	
	
	// 16:41-14/9/15
	void setCalendarWithString(Calendar c,String str) {
		
		int colonIndex = str.indexOf(':');
		int dashIndex = str.indexOf('-');
		int firstSlashIndex = str.indexOf('/');
		int lastSlashIndex = str.lastIndexOf('/');
		
		int hour = Integer.parseInt(str.substring(0, colonIndex));
		int min = Integer.parseInt(str.substring(colonIndex+1, dashIndex));
		int day = Integer.parseInt(str.substring(dashIndex+1, firstSlashIndex));
		int month = Integer.parseInt(str.substring(firstSlashIndex+1, lastSlashIndex))-1;
		int year = Integer.parseInt(str.substring(lastSlashIndex+1));
		
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, month);
		c.set(Calendar.DATE, day);
		c.set(Calendar.HOUR_OF_DAY, hour);
		c.set(Calendar.MINUTE, min);
		c.set(Calendar.SECOND, 0);
		
	}

}
