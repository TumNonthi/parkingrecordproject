import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.awt.print.PageFormat;

public class EntryPrinter {

	public static boolean printCard(final String txt) {
		final PrinterJob job = PrinterJob.getPrinterJob();
		if (job.getPrintService() == null) {
			return false;
		}

		Printable contentToPrint = new Printable() {
			@Override
			public int print(Graphics graphics, PageFormat pageFormat,
					int pageIndex) throws PrinterException {

				Graphics2D g2d = (Graphics2D) graphics;

				g2d.translate(pageFormat.getImageableX(),
						pageFormat.getImageableY());
				g2d.setFont(new Font("Monospaced", Font.BOLD, 10));

				if (pageIndex > 0) {
					return NO_SUCH_PAGE;
				} // Only one page

				String txtArray[] = txt.split(";");

				int y = 20;
				for (int i = 0; i < txtArray.length; i++) {

					g2d.drawString(txtArray[i], 0, y);
					y = y + 20;
				}

				return PAGE_EXISTS;

			}

		};

		PageFormat pageFormat = new PageFormat();
		pageFormat.setOrientation(PageFormat.PORTRAIT);
		Paper pPaper = pageFormat.getPaper();

		pPaper.setImageableArea(0, 0, pPaper.getWidth(), pPaper.getHeight() - 2);
		pageFormat.setPaper(pPaper);

		job.setPrintable(contentToPrint, pageFormat);

		try {
			job.print();
			return true;
		} catch (PrinterException e) {
			System.err.println(e.getMessage());
			return false;
		}

	}
	
	public static void main(String[] args) {
		printCard("This is the first line.;This is the second line.;*************************");
	}

}



