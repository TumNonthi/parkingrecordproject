import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;


public class CustomGuiUtilities {

	public static void useCustomLookAndFeel () {
		
		Color transparent = new Color(0,0,0,0);
    	UIManager.put("nimbusFocus",transparent);
    	UIManager.put("nimbusBorder",transparent);
    	UIManager.put("ComboBox:\"ComboBox.listRenderer\".background",Color.green);
    	UIManager.put("textBackground",transparent);
    	UIManager.put("nimbusSelectionBackground", Color.white);
    	UIManager.put("text",Color.white);
    	UIManager.put("nimbusLightBackground", new Color(0,0,0,0));
    	UIManager.put("nimbusSelectedText", new Color(252,166,182));
    	
    	UIManager.put("JXMonthView.boxPaddingX", 1);
        UIManager.put("JXMonthView.boxPaddingY", 1);
        UIManager.put("JXMonthView.background", Color.BLACK);
        UIManager.put("JXMonthView.monthStringBackground", Color.BLACK);
    	UIManager.put("JXMonthView.monthStringForeground", Color.WHITE);
        UIManager.put("JXMonthView.daysOfTheWeekForeground", Color.BLACK);
        UIManager.put("JXMonthView.weekOfTheYearForeground", Color.BLACK);
        UIManager.put("JXMonthView.unselectableDayForeground", Color.BLACK);
        //UIManager.put("JXMonthView.selectedBackground", new ColorUIResource(197, 220, 240));
        UIManager.put("JXMonthView.flaggedDayForeground", Color.BLACK);
        UIManager.put("JXMonthView.leadingDayForeground", Color.BLACK);
        UIManager.put("JXMonthView.trailingDayForeground", Color.BLACK);
        
        UIManager.put("OptionPane.messageForeground", Color.BLACK);
		
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                try {
					UIManager.setLookAndFeel(info.getClassName());
				} catch (Exception e) {
					e.printStackTrace();
				} 
                break;
            }
        }
	}
	
	public static void makeTransparent(Component[] comp){//method for change looking of JCombobox
		for(int x = 0; x < comp.length; x++){
			
			if(comp[x] instanceof javax.swing.plaf.metal.MetalComboBoxButton){
				((javax.swing.plaf.metal.MetalComboBoxButton)comp[x]).setOpaque(false);
				((javax.swing.plaf.metal.MetalComboBoxButton)comp[x]).setBorder(null);
			}else if(comp[x] instanceof JTextField){
				//((JTextField)comp[x]).setBorder(null);
				((JTextField)comp[x]).setBackground(new Color(0,0,0,0));
				((JTextField)comp[x]).setForeground(new Color(255,255,255));
			}else if(comp[x] instanceof JButton){
				
				((JButton)comp[x]).setRolloverEnabled(false);
				((JButton)comp[x]).setFocusPainted(false);
				((JButton)comp[x]).setBorderPainted(false);
				((JButton)comp[x]).setForeground(new Color(0,0,0,255));
				
			}
		}
	}
	
}
