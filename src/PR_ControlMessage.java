import java.io.Serializable;
import java.util.ArrayList;


public class PR_ControlMessage implements Serializable {

	/**
	 * 
	 */
	protected static final long serialVersionUID = 1L;
	
	static final int TEST_CONNECTION = 0, TEST_CONNECTION_SUCCESSFUL = 1, 
			ID_SEARCH = 2, ID_RESULT_FOUND = 20, ID_RESULT_NOTFOUND = 21,
			CHECK_IN = 3, CHECK_IN_CONFIRMED = 30, CHECK_IN_ERROR = 31,
			CHECK_OUT = 4, CHECK_OUT_CONFIRMED = 40, CHECK_OUT_ERROR = 41,
			FEEDBACK_ERROR = 9, LOGIN = 50, LOGIN_SUCCESSFUL_LOBBY = 51, LOGOUT = 99,
			PING_EMPTY = 1000, PING_EMPTY_RESPONSE = 1001;
	
	static final int SERVER_ID = -1;
	static final int BROADCAST_RID = -2;
	
	private int type;
	private String message;
	private ParkingEntry parkingEntry;
	private MemberCarEntry memberCarEntry;
	private ArrayList<String> attachedList;
	
	private int senderId;
	private int receiverId;
	
	private int waitingId = -1;
	
	private long timestamp;
	
	PR_ControlMessage(int type, String message, int sId, int rId) {
		this.type = type;
		this.message = message;
		this.senderId = sId;
		this.receiverId = rId;
	}
	
	PR_ControlMessage(int type, String message, int sId, int rId, ParkingEntry parkingEntry) {
		this.type = type;
		this.message = message;
		this.senderId = sId;
		this.receiverId = rId;
		this.parkingEntry = parkingEntry;
	}
	
	PR_ControlMessage(int type, String message, int sId, int rId, MemberCarEntry memberCarEntry) {
		this.type = type;
		this.message = message;
		this.senderId = sId;
		this.receiverId = rId;
		this.memberCarEntry = memberCarEntry;
	}
	
	PR_ControlMessage(int type, String message, int sId, int rId, ParkingEntry parkingEntry, MemberCarEntry memberCarEntry) {
		this.type = type;
		this.message = message;
		this.senderId = sId;
		this.receiverId = rId;
		this.parkingEntry = parkingEntry;
		this.memberCarEntry = memberCarEntry;
	}
	
	PR_ControlMessage(int type, String message, int sId, int rId, ArrayList<String> al) {
		this.type = type;
		this.message = message;
		this.senderId = sId;
		this.receiverId = rId;
		this.attachedList = new ArrayList<String>(al);
	}
	
	PR_ControlMessage(int type, String message, int sId, int rId, ParkingEntry parkingEntry, ArrayList<String> al) {
		this.type = type;
		this.message = message;
		this.senderId = sId;
		this.receiverId = rId;
		this.parkingEntry = parkingEntry;
		this.attachedList = new ArrayList<String>(al);
	}
	
	ParkingEntry getParkingEntry() {
		return parkingEntry;
	}
	
	MemberCarEntry getMemberCarEntry() {
		return memberCarEntry;
	}
	
	ArrayList<String> getAttachedList() {
		return attachedList;
	}
	
	int getType() {
		return type;
	}
	
	String getMessage() {
		return message;
	}
	
	int getWaitingId() {
		return waitingId;
	}
	
	void setWaitingId(int anId) {
		waitingId = anId;
	}
	
	int getReceiverId() {
		return receiverId;
	}
	
	int getSenderId() {
		return senderId;
	}

	long getTimestamp() {
		return timestamp;
	}
	
	void setTimestamp(long aTimestamp) {
		timestamp = aTimestamp;
	}
	
}
