import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.JPanel;


public class MyPanel extends JPanel {
		
	protected void paintComponent(Graphics g) {
		g.setColor(getBackground());
        Rectangle r = g.getClipBounds();
        g.fillRect(r.x, r.y, r.width, r.height);
        super.paintComponent(g);
	}
		
}
