import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

enum EntryType {
	CAR, MOTORCYCLE
}

public class ParkingEntry implements Serializable, Comparable<ParkingEntry> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String uniqueId;

	public String barcodeId;
	public String plateNumString;
	public long entryUTC;
	public long exitUTC;
	public EntryType entryType;
	public MemberType memberType;
	public boolean isCK;
	public long expirationDate;
	public int price;

	public ParkingEntry() {
		memberType = MemberType.NONE;
	}

	public ParkingEntry(long anEntryUTC, long anExitUTC, EntryType anEntryType) {
		entryUTC = anEntryUTC;
		exitUTC = anExitUTC;
		entryType = anEntryType;
		memberType = MemberType.NONE;
		// isCK = ck;
	}

	public ParkingEntry(Calendar anEntryTime, Calendar anExitTime,
			EntryType anEntryType) {
		entryUTC = anEntryTime.getTimeInMillis();
		exitUTC = anExitTime.getTimeInMillis();
		entryType = anEntryType;
		memberType = MemberType.NONE;
		// isCK = ck;
	}

	public int CalculatePrice() {

		Calendar cEntry = Calendar.getInstance();
		cEntry.setTimeInMillis(entryUTC);
		Calendar cExit = Calendar.getInstance();
		cExit.setTimeInMillis(exitUTC);
		Calendar cExp = Calendar.getInstance();
		cExp.setTimeInMillis(expirationDate);

		return CalculatePrice(cEntry, cExit, entryType, memberType, isCK, cExp);

	}

	static int _dayBeginForMemberFreeDay = 8;
	static int _dayEndForMemberFreeDay = 20;

	static int _dayBeginForMemberFreeNight = 8;
	static int _dayEndForMemberFreeNight = 18;

	public static int CalculatePrice(Calendar _entryTime, Calendar _exitTime,
			EntryType entryType, MemberType memberType, boolean isCK,
			Calendar _expDate) {

		Calendar entryTime = (Calendar) _entryTime.clone();
		Calendar exitTime = (Calendar) _exitTime.clone();
		Calendar expDate = (Calendar) _expDate.clone();

		entryTime.set(Calendar.SECOND, 0);
		entryTime.set(Calendar.MILLISECOND, 0);

		exitTime.set(Calendar.SECOND, 0);
		exitTime.set(Calendar.MILLISECOND, 0);

		expDate.set(Calendar.HOUR_OF_DAY, 23);
		expDate.set(Calendar.MINUTE, 59);
		expDate.set(Calendar.SECOND, 59);
		expDate.set(Calendar.MILLISECOND, 0);

		int dayFee = 20;
		int nightFee = 10;

		int retVal = 0;
		int sessionLength = 0;
		boolean wasDaySession = false;
		boolean isDaySession = false;

		if (exitTime.getTimeInMillis() - entryTime.getTimeInMillis() < 60000) {

			if (entryTime.get(Calendar.HOUR_OF_DAY) >= 6
					&& entryTime.get(Calendar.HOUR_OF_DAY) < 18) {
				if ((memberType == MemberType.MONTHLY_DAY || memberType == MemberType.MONTHLY_ALL)
						&& exitTime.getTimeInMillis() <= expDate
								.getTimeInMillis()) {
					return 0;
				} else if (entryTime.get(Calendar.HOUR_OF_DAY) < 8
						&& (memberType == MemberType.MONTHLY_NIGHT || memberType == MemberType.MONTHLY_ALL)
						&& exitTime.getTimeInMillis() <= expDate
								.getTimeInMillis()) {
					return 0;
				} else {
					if (entryType == EntryType.MOTORCYCLE) {
						return 20;
					} else {
						return dayFee;
					}
				}
			} else {
				if ((memberType == MemberType.MONTHLY_NIGHT || memberType == MemberType.MONTHLY_ALL)
						&& exitTime.getTimeInMillis() <= expDate
								.getTimeInMillis()) {
					return 0;
				} else {
					if (entryType == EntryType.MOTORCYCLE) {
						return 20;
					} else {
						return nightFee;
					}
				}
			}

		}

		if (isCK && memberType == MemberType.NONE) {
			if (exitTime.getTimeInMillis() - entryTime.getTimeInMillis() <= 7200000) {

				if (entryType == EntryType.MOTORCYCLE) {
					return 10;
				}

				if (entryTime.get(Calendar.HOUR_OF_DAY) >= 6
						&& entryTime.get(Calendar.HOUR_OF_DAY) < 18) {
					return dayFee;
				} else {
					return nightFee;
				}
			}
		}

		int dayBegin = 6;
		int dayEnd = 18;

		Calendar timeItr = (Calendar) entryTime.clone();
		while (timeItr.compareTo(exitTime) < 0) {

			boolean isFreeHour = false;

			int freeDayBegin = 0;
			int freeDayEnd = 0;

			MemberType currentMemberType = memberType;
			if (timeItr.compareTo(expDate) > 0) {
				currentMemberType = MemberType.NONE;
			}

			if (currentMemberType == MemberType.MONTHLY_DAY) {
				freeDayBegin = _dayBeginForMemberFreeDay;
				freeDayEnd = _dayEndForMemberFreeDay;
			}
			if (currentMemberType == MemberType.MONTHLY_NIGHT) {
				freeDayBegin = _dayBeginForMemberFreeNight;
				freeDayEnd = _dayEndForMemberFreeNight;
			}

			boolean isFreeDay = timeItr.get(Calendar.HOUR_OF_DAY) >= freeDayBegin
					&& timeItr.get(Calendar.HOUR_OF_DAY) < freeDayEnd;
			switch (currentMemberType) {
			case MONTHLY_DAY:
				isFreeHour = isFreeDay;
				break;
			case MONTHLY_NIGHT:
				isFreeHour = !isFreeDay;
				break;
			case MONTHLY_ALL:
				isFreeHour = true;
				break;
			default:
				isFreeHour = false;
			}

			isDaySession = (timeItr.get(Calendar.HOUR_OF_DAY) >= dayBegin && timeItr
					.get(Calendar.HOUR_OF_DAY) < dayEnd);

			if (wasDaySession != isDaySession) {
				sessionLength = 0;
			}

			long anHourFromNow = timeItr.getTimeInMillis() + 3600000;

			long nextMark;
			if (anHourFromNow > exitTime.getTimeInMillis()) {
				nextMark = exitTime.getTimeInMillis();
			} else {
				nextMark = anHourFromNow;
			}

			if (isDaySession) {

				if (isFreeHour) {

					if (timeItr.get(Calendar.HOUR_OF_DAY) < _dayBeginForMemberFreeNight) {

						// Reset time itr at free night end
						if (memberType == MemberType.MONTHLY_NIGHT) {
							Calendar freeNightEndDateTime = (Calendar) timeItr
									.clone();

							freeNightEndDateTime.set(Calendar.HOUR_OF_DAY,
									_dayBeginForMemberFreeNight - 1);
							freeNightEndDateTime.set(Calendar.MINUTE, 59);
							freeNightEndDateTime.set(Calendar.SECOND, 59);
							freeNightEndDateTime.set(Calendar.MILLISECOND, 0);

							if (timeItr.compareTo(freeNightEndDateTime) <= 0
									&& freeNightEndDateTime.getTimeInMillis() < nextMark) {
								timeItr.setTime(freeNightEndDateTime.getTime());
								timeItr.add(Calendar.SECOND, 1);

								sessionLength = 1;
								if (entryType == EntryType.MOTORCYCLE) {
									retVal += 5;
									System.out.println("MOTOR+1-"
											+ timeItr.get(Calendar.HOUR_OF_DAY)
											+ ":"
											+ timeItr.get(Calendar.MINUTE));
								} else {
									retVal += dayFee;
									System.out.println("DAY");
								}

							} else {

							}
						}

					}

				} else {

					if (sessionLength < 10 || entryType != EntryType.CAR) {
						if (entryType == EntryType.MOTORCYCLE) {
							retVal += 5;
							System.out.println("MOTOR+1-"
									+ timeItr.get(Calendar.HOUR_OF_DAY) + ":"
									+ timeItr.get(Calendar.MINUTE));
						} else {
							retVal += dayFee;
							System.out.println("DAY");
						}
						sessionLength++;
					}

				}

			} else {
				if (isFreeHour) {

					// Reset time itr at free day end
					if (memberType == MemberType.MONTHLY_DAY) {

						Calendar freeDayEndDateTime = (Calendar) timeItr
								.clone();

						freeDayEndDateTime.set(Calendar.HOUR_OF_DAY, freeDayEnd - 1);
						freeDayEndDateTime.set(Calendar.MINUTE, 59);
						freeDayEndDateTime.set(Calendar.SECOND, 59);
						freeDayEndDateTime.set(Calendar.MILLISECOND, 0);

						if (timeItr.compareTo(freeDayEndDateTime) <= 0
								&& freeDayEndDateTime.getTimeInMillis() < nextMark) {
							
							timeItr.setTime(freeDayEndDateTime.getTime());
							timeItr.add(Calendar.SECOND, 1);

							sessionLength = 1;
							if (entryType == EntryType.MOTORCYCLE) {
								retVal += 5;
								System.out.println("MOTOR+1-"
										+ timeItr.get(Calendar.HOUR_OF_DAY)
										+ ":" + timeItr.get(Calendar.MINUTE));
							} else {
								retVal += nightFee;
								System.out.println("NIGHT");
							}

						} else {

						}

					}

					// Reset time itr at exp date time
					if (timeItr.compareTo(expDate) <= 0
							&& expDate.getTimeInMillis() < nextMark) {
						timeItr.setTime(expDate.getTime());
						timeItr.add(Calendar.SECOND, 1);

						sessionLength = 1;
						if (entryType == EntryType.MOTORCYCLE) {
							retVal += 5;
							System.out.println("MOTOR+1-"
									+ timeItr.get(Calendar.HOUR_OF_DAY) + ":"
									+ timeItr.get(Calendar.MINUTE));
						} else {
							retVal += nightFee;
							System.out.println("NIGHT");
						}

					} else {

					}

				} else {

					if (sessionLength < 10 || entryType != EntryType.CAR) {
						if (entryType == EntryType.MOTORCYCLE) {
							retVal += 5;
							System.out.println("MOTOR+1-"
									+ timeItr.get(Calendar.HOUR_OF_DAY) + ":"
									+ timeItr.get(Calendar.MINUTE));
						} else {
							retVal += nightFee;
							System.out.println("NIGHT");
						}
						sessionLength++;
					}

				}

			}

			wasDaySession = isDaySession;
			timeItr.add(Calendar.HOUR_OF_DAY, 1);

		}

		if (entryType == EntryType.MOTORCYCLE && retVal > 0) {
			retVal = Math.max(20, retVal + 10);
		}

		return retVal;
	}

	boolean isTimeBeforeHour(Calendar time, int hour) {

		time.set(Calendar.SECOND, 0);
		time.set(Calendar.MILLISECOND, 0);

		Calendar cHour = (Calendar) time.clone();

		if (hour == 0) {
			hour = 24;
		}

		cHour.set(Calendar.HOUR_OF_DAY, hour - 1);
		cHour.set(Calendar.MINUTE, 59);
		cHour.set(Calendar.SECOND, 59);
		cHour.set(Calendar.MILLISECOND, 59);

		return time.compareTo(cHour) < 0;

	}

	boolean isTimeAfterOrEqualToHour(Calendar time, int hour) {

		time.set(Calendar.SECOND, 0);
		time.set(Calendar.MILLISECOND, 0);

		Calendar cHour = (Calendar) time.clone();

		cHour.set(Calendar.HOUR_OF_DAY, hour);
		cHour.set(Calendar.MINUTE, 0);

		return time.compareTo(cHour) >= 0;

	}

	public String getPlateNumString() {

		// String retVal;
		//
		// try {
		// retVal = new String(plateNumberBytes);
		// } catch(Exception e) {
		// retVal = new String(plateNumberBytes);
		// }
		// return retVal;

		return plateNumString;

	}

	public int getYear() {

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(exitUTC);
		return c.get(Calendar.YEAR);

	}

	public int getMonth() {

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(exitUTC);
		return c.get(Calendar.MONTH);

	}

	public int getDate() {

		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(exitUTC);
		return c.get(Calendar.DATE);

	}

	public Date getDateTime() {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(exitUTC);
		return c.getTime();
	}

	@Override
	public int compareTo(ParkingEntry p) {
		return getDateTime().compareTo(p.getDateTime());
	}

	public void copyInfo(ParkingEntry infoEntry) {
		this.barcodeId = infoEntry.barcodeId;
		// this.plateNumberBytes = infoEntry.plateNumberBytes;
		this.plateNumString = infoEntry.plateNumString;
		this.entryType = infoEntry.entryType;
		this.memberType = infoEntry.memberType;
		this.isCK = infoEntry.isCK;
		this.entryUTC = infoEntry.entryUTC;
		this.exitUTC = infoEntry.exitUTC;
		this.expirationDate = infoEntry.expirationDate;
		this.price = infoEntry.price;
	}

	public static void main(String[] args) {
		Calendar en = Calendar.getInstance();
		en.set(2015, 9-1, 10, 14, 58, 0);
		Calendar ex = Calendar.getInstance();
		ex.set(2015, 9-1, 10, 17, 54, 0);
		Calendar exp = Calendar.getInstance();
		exp.set(2016, 3, 30, 0, 0, 0);

		System.out.println(CalculatePrice(en, ex, EntryType.CAR,
				MemberType.NONE, false, exp));

	}

}
